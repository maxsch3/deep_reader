import utils
import json


def bbox_size(bbox):
    if bbox is None:
        return 0, 0
    w = bbox['w'] if 'w' in bbox else 0
    h = bbox['h'] if 'h' in bbox else 0
    return w, h


def bbox_offset(bbox):
    if bbox is None:
        return 0, 0
    x = bbox['x'] if 'x' in bbox else 0
    y = bbox['y'] if 'y' in bbox else 0
    return x, y


def bbox_rectangle(bbox):
    size = bbox_size(bbox)
    offs = bbox_offset(bbox)
    far = tuple(x+y for x, y in zip(offs, size))
    return offs, far


def bbox_union(bboxes, bb_type=None):
    assert type(bboxes) == list
    assert len(bboxes) > 0
    x = min([b['x'] for b in bboxes])
    y = min([b['y'] for b in bboxes])
    w = max([b['x'] + b['w'] for b in bboxes]) - x
    h = max([b['y'] + b['h'] for b in bboxes]) - y
    if bb_type is None:
        bb_type = bboxes[0]['type']
    return bbox_new(bb_type, None, (x, y), (w, h))


def bbox_new(typ, content, corner=(0, 0), dim=None):
    if dim is None:
        dim = bbox_size(content)
    if not bbox_is_content(content):
        content = None
    elif type(content) is dict:
        # envelope single bbox in a list to keep structure consistent.
        # this will not affect atomic values like str at the bottom of the structure
        content = [content]
    return {"type": typ, "content": content, "x": corner[0], "y": corner[1], "w": dim[0], "h": dim[1]}


def bbox_adjust_single(bbox, key, val, mode):
    assert type(key) == str
    if key in bbox:
        if mode == 'add':
            bbox[key] += val
        else:
            bbox[key] = val
    return bbox


def bbox_adjust(bbox, key, val, mode='replace'):
    if not utils.is_sequence(key) and not utils.is_sequence(val):
        # both key and val are single not iterable items
        bbox = bbox_adjust_single(bbox, key, val, mode)
    else:
        assert len(key) == len(val)
        for k, v in zip(key, val):
            bbox = bbox_adjust_single(bbox, k, v, mode)
    return bbox


def bbox_is_content(content):
    if content is None:
        return False
    if utils.is_sequence(content):
        return len(content) > 0
    else:
        if 'content' in content:
            if content['content'] is None:
                return False
    return True


def bbox_cascade_offset(bbox, offset=(0, 0)):
    if type(bbox) not in [dict]:
        return bbox
    if 'x' in bbox and 'y' in bbox:
        bbox = bbox_adjust(bbox, ['x', 'y'], offset, mode='add')
        loc_offset = (bbox['x'], bbox['y'])
        if 'content' in bbox:
            if type(bbox['content']) is list:
                bbox['content'] = [bbox_cascade_offset(b, loc_offset) for b in bbox['content']]
    return bbox


def bbox_cascade_transform(bbox, transform):
    if type(bbox) not in [dict]:
        return bbox
    if 'x' in bbox and 'y' in bbox:
        bbox = bbox_transform(bbox, transform)
        if 'content' in bbox:
            if type(bbox['content']) is list:
                bbox['content'] = [bbox_cascade_transform(b, transform) for b in bbox['content']]
    return bbox


def bbox_transform(bbox, transform):
    return transform.transform_bbox(bbox)


def bbox_move(bbox, offset=(0, 0)):
    assert type(bbox) in [dict, list]
    if type(bbox) == dict:
        if 'x' in bbox and 'y' in bbox:
            bbox = bbox_adjust(bbox, ['x', 'y'], offset, mode='add')
            if 'content' in bbox:
                if type(bbox['content']) == list:
                    bbox['content'] = [bbox_move(b, offset) for b in bbox['content']]
    if type(bbox) == list:
        bbox = [bbox_move(b, offset) for b in bbox]
    return bbox


def bbox_select_entities(bboxes, entity_type, tags=None):
    # this is a recursive function that scans through the bbox tree looking for entities of particular type
    # and returns them as a plain list
    entities = []
    if type(bboxes) is list:
        en = [bbox_select_entities(bbox, entity_type, tags) for bbox in bboxes]
        en = [e for e in en if len(e) > 0]
        # flatten lists of lists if needed
        en = [e for e in en if type(e) == dict] + sum([e for e in en if type(e) == list], [])
        if len(en) > 0:
            entities = en
    elif type(bboxes) is dict:
        if 'type' in bboxes:
            if bboxes['type'] == entity_type and bbox_tags_match(bboxes, tags) is True:
                entities = bboxes
            elif 'content' in bboxes:
                entities = bbox_select_entities(bboxes['content'], entity_type, tags)
    return entities


def bbox_get_outerbox(bboxes, entity_type, tags=None):
    assert type(bboxes) in [dict, list]
    entities = bbox_select_entities(bboxes, entity_type, tags)
    if len(entities) == 0:
        return bbox_new(entity_type, None, (0, 0), (0, 0))
    return bbox_union(entities)


def bbox_make_envelope(bboxes, look_for, new_type):
    assert type(new_type) in [list, str]
    outer_bb = bbox_get_outerbox(bboxes, look_for)
    if type(new_type) is str:
        new_type = [new_type]
    for t in new_type:
        bboxes = bbox_new(t, bboxes, bbox_offset(outer_bb), bbox_size(outer_bb))
    return bboxes


def bbox_add_tags(bbox, tags):
    bbox['tags'] = tags
    return bbox


def bbox_tags_match(bbox, tags):
    if tags is None:
        return True
    if 'tags' not in bbox:
        return False
    rv = True
    assert type(tags) == dict
    for k, v in tags.items():
        if k not in bbox['tags']:
            rv = False
        elif v is not None:
            if bbox['tags']['k'] != v:
                rv = False
    return rv


def bboxes_from_tags(bboxes, tag, new_type, entity_type):
    # this function scans bboxes for boxes with tags set, and builds a list of bboxes of type new_type
    # for each encountered value of a tag
    new_bboxes = []
    entities = bbox_select_entities(bboxes, entity_type, {tag:None})
    values = list(set([e['tags'][tag] for e in entities]))
    for v in values:
        e = [e for e in entities if e['tags'][tag] == v]
        bbox = bbox_union(e, new_type)
        new_bboxes.append(bbox)
    return new_bboxes


def bboxes_savejson(bboxes, filename):
    js = json.dumps(bboxes, indent="\t")
    f = open(filename, "w")
    f.write(js)
    f.close()
