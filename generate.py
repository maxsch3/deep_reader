import json
import utils
import pillow_draw as draw
import bbox as bb
import content_generator as cg
import template_generator as tg
import transform as tr

def load_json(file):
    if not utils.check_file(file):
        exit(1)
    f = open(file, "r", encoding='utf-8')
    j = f.read()
    f.close()
    jdata = json.loads(j)
    return jdata

def load_template(file):
    return load_json(file)

def process_block(values, canvas, meta, block, context):
    # dispatch call to proper handler based on what type of block is being processed
    # fun = locals()['pillow_draw.draw_'+block['type']]
    fun = getattr(draw, 'draw_'+block['type'])
    try:
        canvas, bboxes = fun(values, canvas, meta, block, context)
    except RuntimeError as err:
        print(err)
        exit(1)
    return canvas, bboxes


def process_template(values, template):
    meta = template['document']['metadata']
    canvas = draw.new_patch(meta, 50)
    bboxes = []
    # process blocks in template
    context = draw.new_context()
    for block in template['document']['blocks']:
        canvas, bbox = process_block(values, canvas, meta, block, context)
        context = draw.update_context(context, canvas.size)
        bboxes.append(bbox)
    # finalize bboxes
    bboxes = bb.bbox_new('document', bboxes, (0, 0), canvas.size)
    # bboxes = bb.bbox_cascade_offset(bboxes)
    # At this point, both image and bboxes are not transformed and are ready to be saved.
    # canvas = draw.draw_bboxes(canvas, bboxes, 'word', 'blue')
    #canvas.save('out.png', 'PNG')
    #bb.bboxes_savejson(bboxes, 'boxes.json')

    # We will apply transformation to both before saving though
    #transf = tr.Transform([None, None, tr.FixedDistribution(5*2*3.14/180), None])
    #transf.adjust_origin_hw(canvas.size)
    #bb.bbox_cascade_transform(bboxes, transf)
    #canvas = transf.transform_image(canvas)
    #canvas = draw.draw_bboxes(canvas, bboxes, 'word', 'blue')
    # save image
    # canvas.save('out.png', 'PNG')
    # process bboxes
    # bb.bboxes_savejson(bboxes, 'boxes.json')
    return canvas, bboxes


def generate_document(parameters, name='out'):
    #parameters = default_parameters(parameters)
    gen = cg.ConGen(parameters['words_dir'])
    doc = gen.random_document()
    tgen = tg.TemGen(parameters['templates_dir'] + '/' + parameters['language'] + '/')
    if 'doc_template' not in parameters:
        parameters['doc_template'] = None
    template = tgen.gen_template(template=parameters['doc_template'])
    image, bboxes = process_template(doc, template)
    image, bboxes = transform_document(image, bboxes, parameters)
    save_document(image, bboxes, parameters, name)
    return 1


def transform_document(image, bboxes, parameters):
    #transformation = tr.Transform([None, None, {'type': 'fixed', 'parameters': 5}, None])
    transformation = tr.Transform([parameters['move'], parameters['scale'],
                                   parameters['rotation'], parameters['shear'],
                                   parameters['compress']])
    transformation.adjust_origin_hw(image.size)
    bboxes = bb.bbox_cascade_transform(bboxes, transformation)
    image = transformation.transform_image(image)
    return image, bboxes


def default_parameters(parameters):
    defaults = load_json('defaults.json')['defaults']
    pars = {}
    [pars.update(default_parameter(d, parameters)) for d in defaults]
    return pars


def save_document(image, bboxes, parameters, name):
    image_dir = parameters['img_out_dir']
    box_dir = parameters['box_out_dir']
    image.save(image_dir + '/' + name + '.png', 'PNG')
    bb.bboxes_savejson(bboxes, box_dir + '/' + name + '.json')


def default_parameter(setting, parameters):
    assert type(setting) is dict
    key, value = list(setting.items())[0]
    if key not in parameters:
        return setting
    else:
        return parameters[key]


defaults = default_parameters({'doc_template': 'doc1'})
if 'batch_size' not in defaults:
    defaults['batch_size'] = 1
for i in range(0, defaults['batch_size']):
    generate_document(defaults, str(i+1))

