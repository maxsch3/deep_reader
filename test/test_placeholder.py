from data_generator import placeholder as pc, text_entity as te
import os
from PIL import ImageDraw


class TestDocument:

    @classmethod
    def setup_class(cls):
        defaults = {'font': 'DejaVuSansMono',
                    'font_size': 12,
                    'font_color': 'black',
                    'color_mode': 'RGB',
                    'background_color': 'white'}
        te.TextEntity.set_defaults(defaults)
        pc.Placeholder.set_defaults(defaults)
        cls.text_block = {'type': 'text', 'text': 'the_test'}
        #cls.text_obj = te.TextEntity(text_block)
        # cls.block = {'type': 'table', 'columns': 2, 'table': [
        #     {'type': 'text', 'text': 'cell 1'},
        #     {'type': 'text', 'text': 'Wide-wide-wide column g'},
        #     {'type': 'text', 'text': 'cell 2_1'},
        #     None
        # ]}
        # cls.tab = table.Table(cls.block)
        cls.dirname = os.path.dirname(__file__)

    @classmethod
    def teardown_class(cls):
        pass

    def test_placeholder_size(self):
        block = {'align': 'cb', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.get_size() == (100, 100)
        block = {'align': 'cm', **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.get_size() == (56, 15)
        block = {'align': 'cm', 'padding': 10, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.get_size() == (76, 35)

    def test_placeholder_def_align(self):
        block = {'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'l'
        assert ph.valign == 't'
        assert ph.offset == (0, 0)

    def test_placeholder_def_align_padded(self):
        block = {'padding': 10, 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'l'
        assert ph.valign == 't'
        assert ph.offset == (10, 10)

    def test_placeholder_r_align(self):
        block = {'align': 'r', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'r'
        assert ph.valign == 't'
        assert ph.offset == (44, 0)

    def test_placeholder_r_align_padded(self):
        block = {'padding': [10, 5, 10, 5], 'align': 'r', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'r'
        assert ph.valign == 't'
        assert ph.offset == (39, 10)

    def test_placeholder_c_align(self):
        block = {'align': 'c', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'c'
        assert ph.valign == 't'
        assert ph.offset == (22, 0)

    def test_placeholder_c_align_padded(self):
        block = {'align': 'c', 'padding': 10, 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'c'
        assert ph.valign == 't'
        assert ph.offset == (22, 10)

    def test_placeholder_rb_align(self):
        block = {'align': 'rb', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'r'
        assert ph.valign == 'b'
        assert ph.offset == (44, 85)

    def test_placeholder_rb_align_padded(self):
        block = {'align': 'rb', 'padding': [5, 7, 9, 11], 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'r'
        assert ph.valign == 'b'
        assert ph.offset == (37, 76)

    def test_placeholder_rm_align(self):
        block = {'align': 'rm', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'r'
        assert ph.valign == 'm'
        assert ph.offset == (44, 42)

    def test_placeholder_rm_align_padded_irregular(self):
        block = {'align': 'rm', 'padding': [5, 7, 9, 11], 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'r'
        assert ph.valign == 'm'
        assert ph.offset == (37, 40)

    def test_placeholder_cm_align(self):
        block = {'align': 'cm', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.offset == (22, 42)

    def test_placeholder_b_align(self):
        block = {'align': 'b', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'l'
        assert ph.valign == 'b'
        assert ph.offset == (0, 85)

    def test_placeholder_bbox(self):
        block = {'align': 'cm', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        box = ph.bbox()
        assert 'type' in box
        assert box['type'] == 'text'
        assert 'x' in box
        assert box['x'] == [(22, 42), (22, 57), (78, 57), (78, 42)]

    def test_placeholder_bbox_padded(self):
        block = {'align': 'rb', 'padding': 12, 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        box = ph.bbox()
        assert 'type' in box
        assert box['type'] == 'text'
        assert 'x' in box
        assert box['x'] == [(32, 73), (32, 88), (88, 88), (88, 73)]

    def test_placeholder_set_height(self):
        block = {'align': 'cm', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.offset == (22, 42)
        ph.set_height(120)
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.offset == (22, 52)
        box = ph.bbox()
        assert box['x'] == [(22, 52), (22, 67), (78, 67), (78, 52)]

    def test_placeholder_set_width(self):
        block = {'align': 'cm', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.offset == (22, 42)
        ph.set_width(120)
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.offset == (32, 42)
        box = ph.bbox()
        assert box['x'] == [(32, 42), (32, 57), (88, 57), (88, 42)]

    def test_placeholder_set_size(self):
        block = {'align': 'cm', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.offset == (22, 42)
        ph.set_size((110, 120))
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.offset == (27, 52)
        box = ph.bbox()
        assert box['x'] == [(27, 52), (27, 67), (83, 67), (83, 52)]

    def test_placeholder_set_padding(self):
        block = {'align': 'rm', 'width_max': 100, 'height_max': 100, **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.offset == (44, 42)
        ph.set_padding(10)
        assert ph.offset == (34, 42)
        ph.set_padding([5, 7, 9, 11])
        assert ph.offset == (37, 40)

    def test_placeholder_create_def_size_and_change(self):
        block = {'align': 'cm', **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.halign == 'c'
        assert ph.valign == 'm'
        assert ph.get_size() == (56, 15)
        assert ph.offset == (0, 0)
        ph.set_size((100, 100))
        assert ph.get_size() == (100, 100)
        assert ph.offset == (22, 42)
        box = ph.bbox()
        assert 'x' in box
        assert box['x'] == [(22, 42), (22, 57), (78, 57), (78, 42)]

    def test_placeholder_set_parameters(self):
        block = {'align': 'ct', **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.get_size() == (56, 15)
        assert ph.offset == (0, 0)
        assert ph.halign == 'c'
        assert ph.valign == 't'
        ph.set_parameters({'padding': 10, 'align': 'lm', 'size': (110, 120)})
        assert ph.get_size() == (110, 120)
        assert ph.offset == (10, 52)
        assert ph.halign == 'l'
        assert ph.valign == 'm'

    def test_placeholder_reset_transformation(self):
        block = {'align': 'ct'}

    def test_push_down_parameters(self):
        block = {'align': 'ct', 'width_max': 100, 'padding': [10, 11, 12, 13], **self.text_block}
        ph = pc.Placeholder(block)
        assert ph.obj.width == 100-11-13
        p = ph.pushdown_parameters({'width': 100, 'padding': 10, 'type': 'text', 'text': 'test test test',
                                    'width_max': 50})
        # check that block definition (second argument) takes precedence over placeholder (first argument)
        # when same parameter (width) availble in both
        assert p['width_max'] == 50

    def test_register_entity(self):
        pc.Placeholder._primitives = None
        pc.Placeholder.register_entity()
        assert pc.Placeholder._primitives is not None
        assert len(pc.Placeholder._primitives) == 4
        assert 'text' in pc.Placeholder._primitives
        assert 'image' in pc.Placeholder._primitives
        assert 'barcode' in pc.Placeholder._primitives
        assert 'table' in pc.Placeholder._primitives
        pc.Placeholder.register_entity(pc.Placeholder)
        assert len(pc.Placeholder._primitives) == 5
        assert 'placeholder' in pc.Placeholder._primitives
        assert pc.Placeholder._primitives['placeholder'].entity_type() == 'placeholder'





#
# defaults = {'font': 'DejaVuSansMono',
#             'font_size': 12,
#             'font_color': 'black',
#             'color_mode': 'RGB',
#             'background_color': 'white'}
# te.TextEntity.set_defaults(defaults)
#
# block = {'type': 'text', 'text': 'the_test'}
#
# text_obj = te.TextEntity(block)
# ph = pc.Placeholder(text_obj, 'align': 'lt', 'padding': 10, 'width_max': 100, 'height_max': 100}
# img = ph.get_img()
# box = ph.bbox()
# draw = ImageDraw.Draw(img)
# draw.polygon(box['x'], outline='red')
# img.show()
