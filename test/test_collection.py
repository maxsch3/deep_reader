from data_generator import collection
from data_generator import text_entity, geom_transform
from data_generator import placeholder
from PIL import ImageDraw, Image
import os
import numpy as np


class TestCollection:

    epsilon = 10**(-5)

    @classmethod
    def setup_class(cls):
        defaults = {'font': 'DejaVuSansMono',
                    'font_size': 24,
                    'font_color': 'black',
                    'color_mode': 'RGB',
                    'background_color': 'white'}
        collection.Collection.set_defaults(defaults)
        text_entity.TextEntity.set_defaults(defaults)
        block = {'type': 'collection', 'collection': [
                  {'type': 'text', 'text': 'line 1'},
                  {'type': 'text', 'text': 'line 2'}
                ]}
        cls.coll = collection.Collection(block)
        cls.dirname = os.path.dirname(__file__)

    @classmethod
    def teardown_class(cls):
        pass

    def test_get_size(self):
        # self.coll.add_rotation(30)
        self.coll.apply_transformations({'rotation': 30})
        assert self.coll.get_size() == (174, 134)

    def test_creating_with_transform(self):
        block = {'type': 'collection', 'collection': [
            {'type': 'text', 'text': 'line 1'},
            {'type': 'text', 'text': 'line 2'}
        ]}
        coll1 = collection.Collection(block)
        assert coll1.get_size() == (168, 58)
        block = {'type': 'collection', 'collection': [
            {'type': 'text', 'text': 'line 1'},
            {'type': 'text', 'text': 'line 2'}
            ], 'transform': {'rotation': 30}}
        coll1 = collection.Collection(block)
        assert coll1.get_size() == (174, 134)

    def test_bbox_structure(self):
        bbox = self.coll.bbox()
        assert type(bbox) == dict
        assert 'type' in bbox
        assert bbox['type'] == 'collection'
        assert 'x' in bbox
        assert all(type(p) == tuple for p in bbox['x'])
        assert all(len(p) == 2 for p in bbox['x'])
        assert 'content' in bbox
        assert type(bbox['content']) == list
        content = bbox['content']
        assert len(content) == 2
        assert 'type' in content[0]
        assert content[0]['type'] == 'text'
        assert 'x' in content[0]
        assert 'content' in content[0]
        assert content[0]['text'] == 'line 1'
        assert 'type' in content[1]
        assert content[1]['type'] == 'text'
        assert 'x' in content[1]
        assert 'content' in content[1]
        assert content[1]['text'] == 'line 2'

    def test_bbox_x(self):
        self.coll.reset_transformation()
        bbox = self.coll.bbox()
        # min of each dimension in x must be 0
        assert abs(min(p[0] for p in bbox['x'])) < self.epsilon
        assert abs(min(p[1] for p in bbox['x'])) < self.epsilon
        self.coll.apply_transformations({'rotation': 30})
        bbox = self.coll.bbox()
        # min of each dimension in x must be 0
        assert abs(min(p[0] for p in bbox['x'])) < self.epsilon
        assert abs(min(p[1] for p in bbox['x'])) < self.epsilon
        assert bbox['x'] == [(0, 84), (145, 0), (174, 50), (29, 134)]
        # img = self.coll.get_img()
        # draw = ImageDraw.ImageDraw(img)
        # for c in bbox['content']:
        #     draw.polygon(c['x'], outline='green')
        # img.show()
        assert bbox['content'][0]['x'] == [(0, 84), (11, 104), (84, 62), (73, 42)]
        assert bbox['content'][1]['x'] == [(87, 67), (99, 87), (171, 45), (160, 25)]

    def test_independent_transform(self):
        self.coll.reset_transformation()
        self.coll.transform_child(0, [{'rotation': -10}, {'shear': (30, 0)}])
        self.coll.apply_transformations({'rotation': 30})
        assert self.coll.get_size() == (198, 156)
        bbox = self.coll.bbox()
        assert bbox['x'] == [(0, 93), (162, 0), (198, 62), (36, 156)]
        assert bbox['content'][0]['x'] == [(0, 93), (19, 109), (105, 76), (86, 61)]
        assert bbox['content'][1]['x'] == [(111, 79), (122, 99), (195, 57), (183, 37)]
        new_img = self.coll.get_img().convert("RGB")
        # test boxes by drawing them
        draw = ImageDraw.ImageDraw(new_img)
        draw.polygon(bbox['x'], outline='red')
        draw.polygon(bbox['content'][0]['x'], outline='blue')
        draw.polygon(bbox['content'][1]['x'], outline='blue')
        # save image and then comment out
        # new_img.save('data/test_coll_ind_transform.png')
        file = os.path.join(self.dirname, 'data/test_coll_ind_transform.png')
        ref_img = Image.open(file).convert("RGB")
        assert new_img.size == ref_img.size
        new_array = np.array(new_img)
        ref_array = np.array(ref_img)
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0

    def test_external_transform(self):
        self.coll.reset_transformation()
        self.coll.transform_child(0, [{'rotation': -10}, {'shear': (30, 0)}])
        self.coll.apply_transformations({'rotation': 30})
        # Note: below numbers for offset are rounded integers that are supposed to compensate
        # float numbers and bring displacement to 0. In case of rounding issues, put floats
        ext_transform = geom_transform.GeomTransform([{'rotation': 15}, {'offset': (-24, 42)}])
        bbox = self.coll.bbox(affine_ext=ext_transform)
        assert bbox['x'] == [(0, 132), (132, 0), (183, 51), (51, 183)]
        assert bbox['content'][0]['x'] == [(0, 132), (23, 142), (97, 88), (75, 78)]
        assert bbox['content'][1]['x'] == [(103, 90), (120, 106), (179, 47), (163, 30)]

    def test_reset_transformation(self):
        self.coll.reset_transformation()
        self.coll.transform_child(0, [{'rotation': -10}, {'shear': (30, 0)}])
        self.coll.apply_transformations({'rotation': 30})
        self.coll.reset_transformation()
        assert self.coll.get_img().size == (168, 58)
        bbox = self.coll.bbox()
        print(bbox['content'][0]['x'])
        print(bbox['content'][1]['x'])
        assert bbox['x'] == [(0, 0), (168, 0), (168, 58), (0, 58)]
        assert bbox['content'][0]['x'] == [(0, 0), (0, 23), (84, 23), (84, 0)]
        assert bbox['content'][1]['x'] == [(84, 29), (84, 52), (168, 52), (168, 29)]

    def test_pushdown_align(self):
        block = {'type': 'collection', 'align': 'c', 'collection': [
            {'type': 'text', 'text': 'line 1', 'align': 'r'},
            {'type': 'text', 'text': 'line 2'}
            ]}
        coll = collection.Collection(block)
        assert coll.objects[0].align == 'r'
        assert coll.objects[1].align == 'c'

    def test_pushdown_padding(self):
        block = {'type': 'collection', 'padding': 10, 'collection': [
            {'type': 'text', 'text': 'line 1', 'padding': [11, 12, 13, 14]},
            {'type': 'text', 'text': 'line 2'}
        ]}
        coll = collection.Collection(block)
        assert coll.objects[0].padding == [11, 12, 13, 14]
        # padding is not pushed down from collection to placeholders
        assert coll.objects[1].padding == [0, 0, 0, 0]

    def test_pushdown_width(self):
        block = {'type': 'collection', 'width': 100, 'collection': [
            {'type': 'text', 'text': 'line 1', 'width': 50, 'align': 'r'},
            {'type': 'text', 'text': 'line 2'}
        ]}
        coll = collection.Collection(block)
        # width is not intercepted by placeholders and passed to objects themselves
        assert coll.objects[0].width == 100
        assert coll.objects[0].obj.width == 50
        assert coll.objects[1].width == 100

    def test_pushdown_height(self):
        block = {'type': 'collection', 'height': 100, 'collection': [
            {'type': 'text', 'text': 'line 1', 'height': 50, 'align': 'b'},
            {'type': 'text', 'text': 'line 2'}
        ]}
        coll = collection.Collection(block)
        assert coll.objects[0].height == 100
        # height parameter is not supported by text, so no testing of height at text object level
        # assert coll.objects[0].obj.height == 50
        assert coll.objects[1].height == 100


# defaults = {'font': 'DejaVuSansMono',
#                 'font_size': 24,
#                 'font_color': 'black',
#                 'color_mode': 'RGB',
#                 'background_color': 'white',
#                 'barcode_height': 100,
#                 'barcode_type': 'code128'}
#
# block = {'type': 'collection', 'collection': [
#     {'type': 'text', 'text': 'line 1'},
#     {'type': 'text', 'text': 'line 2'}
#     ]}
#
#
# collection.Collection.set_defaults(defaults)
# text_entity.TextEntity.set_defaults(defaults)
# coll = collection.Collection(block)
# print(coll.objects[0].get_size())
# coll.transform_child(0, [{'rotation': -10}, {'shear': (30, 0)}])
# print(coll.objects[0].get_size())
# coll.apply_transformations({'rotation': 30})
# #ext_transform = affine_transform.GeomTransform([{'rotation': 15}, {'offset': (-24, 41)}])
#
# print(coll.get_size())
# print(coll.bbox())
# #print(coll.bbox(affine_ext=ext_transform))
# bbox = coll.bbox()
# img = coll.get_img()
# img.show()
#
# # test boxes by drawing them
# draw = ImageDraw.ImageDraw(img)
# draw.polygon(bbox['x'], outline='red')
# draw.polygon(bbox['content'][0]['x'], outline='blue')
# draw.polygon(bbox['content'][1]['x'], outline='blue')
# img.show()
# # img.save('data/test_coll_ind_transform.png', 'PNG')
