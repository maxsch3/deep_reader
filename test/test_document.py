from data_generator import document as dc, text_entity as te
import os


class TestDocument():

    @classmethod
    def setup_class(cls):
        defaults = {'font': 'DejaVuSansMono',
                    'font_size': 12,
                    'font_color': 'black',
                    'color_mode': 'RGB',
                    'background_color': 'white'}
        te.TextEntity.set_defaults(defaults)
        dc.Document.set_defaults(defaults)
        cls.block = {'type': 'document', 'width': 400, 'align': 'r', 'padding': [30, 30, 0, 30], 'document': [
            {'type': 'text', 'text': '1 Clarks st,\n New Town,\n BG14 7RT', 'align': 'c'},
            {'type': 'table', 'columns': 2, 'col_align': ['lt', 'rt'], 'widths': [250, 90], 'table':
                [{'type': 'text', 'text': 'Item'},
                 {'type': 'text', 'text': 'Price'},
                 {'type': 'text', 'text': 'Yellow penguins'},
                 {'type': 'text', 'text': '£5.00'}]},
            {'type': 'text', 'text': 'second line'}]}

        cls.dirname = os.path.dirname(__file__)

    @classmethod
    def teardown_class(cls):
        pass

    def test_size(self):
        d = dc.Document(self.block)
        assert d.get_size() == (400, 122)

    def test_offsets(self):
        d = dc.Document(self.block)
        assert d.obj_layout[0]['offset'] == (30, 30)
        assert d.obj_layout[1]['offset'] == (30, 77)
        assert d.obj_layout[2]['offset'] == (30, 107)

    def test_alignment(self):
        d = dc.Document(self.block)
        assert d.objects[0].align == 'c'
        assert d.objects[1].align == 'r'
        assert d.objects[2].align == 'r'

#
# defaults = {'font': 'DejaVuSansMono',
#             'font_size': 12,
#             'font_color': 'black',
#             'color_mode': 'RGB',
#             'background_color': 'white'}
#
# te.TextEntity.set_defaults(defaults)
#
# block = {'type': 'document', 'width': 400, 'align': 'r', 'padding': [30, 30, 0, 30], 'document': [
#     {'type': 'text', 'text': '1 Clarks st,\n New Town,\n BG14 7RT', 'align': 'c'},
#     {'type': 'table', 'columns': 2, 'col_align': ['lt', 'rt'], 'widths': [250, 90], 'table':
#         [{'type': 'text', 'text': 'Item'},
#          {'type': 'text', 'text': 'Price'},
#          {'type': 'text', 'text': 'Yellow penguins'},
#          {'type': 'text', 'text': '£5.00'}]},
#     {'type': 'text', 'text': 'second line'}]}
#
# doc = dc.Document(block)
# print(doc.get_size())
# doc.get_img().show()
