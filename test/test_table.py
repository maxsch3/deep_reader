from data_generator import table, text_entity as te
from PIL import ImageDraw, Image
import numpy as np
import os


class TestTable:

    epsilon = 10**(-5)

    @classmethod
    def setup_class(cls):
        defaults = {'font': 'DejaVuSansMono',
                    'font_size': 12,
                    'font_color': 'black',
                    'color_mode': 'RGB',
                    'background_color': 'white'}
        table.Table.set_defaults(defaults)
        te.TextEntity.set_defaults(defaults)
        cls.block = {'type': 'table', 'table': [
            {'type': 'text', 'text': 'cell 1'},
            {'type': 'text', 'text': 'Wide-wide-wide column g'},
            {'type': 'text', 'text': 'cell 2_1'},
            None
        ]}
        cls.dirname = os.path.dirname(__file__)

    @classmethod
    def teardown_class(cls):
        pass

    def test_padding_single(self):
        addon = {'col_align': ['br', 'br'], 'column_padding': [20, 15], 'columns': 2}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        # t.get_img().show()
        assert t.obj_layout[0]['padding'] == [20, 20, 20, 20]
        assert t.obj_layout[1]['padding'] == [15, 15, 15, 15]
        assert t.obj_layout[2]['padding'] == [20, 20, 20, 20]
        assert t.get_size() == (287, 110)

    def test_padding_multiple(self):
        addon = {'col_align': ['br', 'br'], 'column_padding': [[20, 10, 0, 15], 5], 'columns': 2}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        # t.get_img().show()
        assert t.obj_layout[0]['padding'] == [20, 10, 0, 15]
        assert t.obj_layout[1]['padding'] == [5, 5, 5, 5]
        assert t.obj_layout[2]['padding'] == [20, 10, 0, 15]
        assert t.get_size() == (252, 70)

    def test_bbox(self):
        addon = {'col_align': ['br', 'br'], 'column_padding': [[20, 10, 0, 15], 10], 'columns': 2}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        bbox = t.bbox()
        # img = t.get_img()
        # draw = ImageDraw.Draw(img)
        # for c in bbox['columns']:
        #     draw.polygon(c, outline='red')
        # for r in bbox['rows']:
        #     draw.polygon(r, outline='blue')
        # img.show()
        assert bbox['type'] == 'table'
        assert 'x' in bbox
        print(bbox['columns'][0])
        print(bbox['columns'][1])
        assert bbox['x'] == [(0, 0), (262, 0), (262, 70), (0, 70)]
        assert 'rows' in bbox
        assert len(bbox['rows']) == 2
        assert bbox['rows'][0] == [(29, 10), (29, 32), (252, 32), (252, 10)]
        assert bbox['rows'][1] == [(15, 55), (15, 70), (71, 70), (71, 55)]
        assert 'columns' in bbox
        assert len(bbox['columns']) == 2
        assert bbox['columns'][0] == [(15, 20), (15, 70), (71, 70), (71, 20)]
        assert bbox['columns'][1] == [(91, 10), (91, 25), (252, 25), (252, 10)]

    def test_fixed_widths(self):
        addon = {'col_align': ['c', 'lb'], 'column_padding': [[10, 10, 0, 10], 0], 'widths': [120, 180], 'columns': 2}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        assert t.get_size()[0] == 300

    def test_rendered_pixels(self):
        addon = {'col_align': ['c', 'lb'], 'column_padding': [[30, 10, 0, 10], 0], 'widths': [120, 180], 'columns': 2}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        box = t.bbox()
        new_img = t.get_img()
        draw = ImageDraw.Draw(new_img)
        for c in box['columns']:
            draw.polygon(c, outline='red')
        for r in box['rows']:
            draw.polygon(r, outline='blue')
        # new_img.show()
        file = os.path.join(self.dirname, 'data/test_table_basic.png')
        ref_img = Image.open(file).convert("RGB")
        assert new_img.size == ref_img.size
        new_array = np.array(new_img)
        ref_array = np.array(ref_img)
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0

    def test_infer_columns_align(self):
        addon = {'col_align': ['c', 'lb']}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        assert t.columns == 2

    def test_infer_columns_widths(self):
        addon = {'widths': [120, 180]}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        assert t.columns == 2

    def test_infer_columns_padding(self):
        addon = {'column_padding': [[30, 10, 0, 10], 0]}
        local_block = dict(self.block, **addon)
        t = table.Table(local_block)
        assert t.columns == 2

    def test_sync_object_parameters(self):
        # This test checks that height and width parameters are synced with cell sizes correctly
        local_block = {'type': 'table', 'col_align': ['lb', 'rb', 'rb'],
                       'widths': [250, 70, 30], 'columns': 3, 'table': [
                          {'type': 'text', 'text': 'Article 9456\nmagnetic field\n_____2 * 5.7'},
                          {'type': 'text', 'text': '11.34'}, {'type': 'text', 'text': 'N'},
                          {'type': 'text', 'text': 'Total:'}, {'type': 'text', 'text': '15.3'}
                       ]}
        t = table.Table(local_block)
        assert t.objects[0].get_size()[0] == 250
        assert t.objects[0].get_size()[1] > t.objects[3].get_size()[1]
        # TODO: once text block widths are fixed, uncomment below line (value might change)
        # assert t.objects[0].get_size()[1] == 45


# defaults = {'font': 'DejaVuSansMono',
#             'font_size': 12,
#             'font_color': 'black',
#             'color_mode': 'RGB',
#             'background_color': 'white'}
#
# table.Table.set_defaults(defaults)
# te.TextEntity.set_defaults(defaults)
#
# block = {'type': 'table', 'columns': 2, 'column_padding': [[30, 10, 0, 10], 0],
#          'col_align': ['c', 'lb'],
#          'widths': [120, 180], 'table': [
#     {'type': 'text', 'text': 'cell 1'},
#     {'type': 'text', 'text': 'Wide-wide-wide column g'},
#     {'type': 'text', 'text': 'cell 2_1'},
#     None
# ]}
#
# block = {'type': 'table',
#          'align': ['c', 'lb'],
#          'widths': [120, 180], 'table': [
#         {'type': 'text', 'text': 'cell 1'},
#         {'type': 'text', 'text': 'Wide-wide-wide column g'},
#         {'type': 'text', 'text': 'cell 2_1'},
#         None
#     ]}
#
# block = {'type': 'table', 'col_align': ['lb', 'rb', 'rb'], 'widths': [250, 70, 30], 'columns': 3, 'table': [{'type': 'text', 'text': 'Article 9456\nmagnetic field\n_____2 * 5.7'}, {'type': 'text', 'text': '11.34'}, {'type': 'text', 'text': 'N'}, {'type': 'text', 'text': 'Total:'}, {'type': 'text', 'text': '15.3'}]}
#
# t = table.Table(block)
# # t.apply_transformations({'rotation': 10})
# box = t.bbox()
# img = t.get_img()
# # draw = ImageDraw.Draw(img)
# # for c in box['columns']:
# #     draw.polygon(c, outline='red')
# # for r in box['rows']:
# #     draw.polygon(r, outline='blue')
# img.show()
# # print(draw.multiline_textsize(''))
