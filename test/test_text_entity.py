from data_generator import text_entity as te
import os
from PIL import Image
import numpy as np


class TestTextEntity:

    epsilon = 10**(-5)

    @classmethod
    def setup_class(cls):
        defaults = {'font': 'DejaVuSansMono',
                    'font_size': 12,
                    'font_color': 'black',
                    'color_mode': 'RGB',
                    'background_color': 'white'}
        te.TextEntity.set_defaults(defaults)
        cls.dirname = os.path.dirname(__file__)

    @classmethod
    def teardown_class(cls):
        pass

    def test_text_img_size(self):
        text_block = {'text': 'Test_of_size', 'type': 'text'}
        tx = te.TextEntity(text_block)
        assert tx.get_size() == (84, 15)

    def test_text_bbox(self):
        test_text = 'Test_of_size'
        text_block = {'text': test_text, 'type': 'text'}
        tx = te.TextEntity(text_block)
        box = tx.bbox()
        assert 'type' in box
        assert 'content' in box
        assert box['type'] == 'text'
        assert box['text'] == test_text
        lines = box['content']
        assert type(lines) == list
        assert len(lines) == 1
        line = lines[0]
        assert type(line) == dict
        assert 'type' in line
        assert 'content' in line
        assert 'text' in line
        assert line['type'] == 'line'
        assert line['text'] == test_text
        words = line['content']
        assert type(words) == list
        assert len(words) == 1
        word = words[0]
        assert type(word) == dict
        assert 'type' in word
        assert 'content' in word
        assert 'text' in word
        assert word['type'] == 'word'
        assert word['text'] == test_text
        chars = word['content']
        assert type(chars) == list
        assert len(chars) == 12
        char = chars[0]
        assert type(char) == dict
        assert 'type' in char
        assert 'content' in char
        assert char['type'] == 'char'
        assert char['content'] == 'T'
        textc = "".join([t['content'] for t in chars])
        assert textc == test_text

    def test_bbox_spaces(self):
        test_text = "  test 1g "
        text_block = {'text': test_text, 'type': 'text'}
        tx = te.TextEntity(text_block)
        box = tx.bbox()
        assert box['text'] == test_text
        lines = box['content']
        assert len(lines) == 1
        line = lines[0]
        assert line['x'] == [(14, 0), (14, 15), (63, 15), (63, 0)]
        assert line['text'] == test_text.strip()
        words = line['content']
        assert len(words) == 2
        assert words[0]['text'] == 'test'
        assert words[0]['x'] == [(14, 0), (14, 12), (42, 12), (42, 0)]
        assert len(words[0]['content']) == 4
        chars0 = words[0]['content']
        assert chars0[0]['x'] == [(14, 0), (14, 12), (21, 12), (21, 0)]
        assert chars0[1]['x'] == [(21, 0), (21, 12), (28, 12), (28, 0)]
        assert chars0[2]['x'] == [(28, 0), (28, 12), (35, 12), (35, 0)]
        assert chars0[3]['x'] == [(35, 0), (35, 12), (42, 12), (42, 0)]
        assert words[1]['text'] == '1g'
        assert words[1]['x'] == [(49, 0), (49, 15), (63, 15), (63, 0)]
        assert len(words[1]['content']) == 2
        chars1 = words[1]['content']
        assert chars1[0]['x'] == [(49, 0), (49, 12), (56, 12), (56, 0)]
        assert chars1[1]['x'] == [(56, 0), (56, 15), (63, 15), (63, 0)]

    def test_bbox_rotated(self):
        test_text = "  test 1g "
        text_block = {'text': test_text, 'type': 'text', 'transform': {'rotation': 30}}
        tx = te.TextEntity(text_block)
        box = tx.bbox()
        assert box['text'] == test_text
        lines = box['content']
        assert len(lines) == 1
        line = lines[0]
        assert line['x'] == [(12, 28), (20, 41), (62, 16), (55, 3)]
        assert line['text'] == test_text.strip()
        words = line['content']
        assert len(words) == 2
        assert words[0]['text'] == 'test'
        assert words[0]['x'] == [(12, 28), (18, 38), (42, 24), (36, 14)]
        assert len(words[0]['content']) == 4
        chars0 = words[0]['content']
        assert chars0[0]['x'] == [(12, 28), (18, 38), (24, 35), (18, 24)]
        assert chars0[1]['x'] == [(18, 24), (24, 35), (30, 31), (24, 21)]
        assert chars0[2]['x'] == [(24, 21), (30, 31), (36, 28), (30, 17)]
        assert chars0[3]['x'] == [(30, 17), (36, 28), (42, 24), (36, 14)]
        assert words[1]['text'] == '1g'
        assert words[1]['x'] == [(42, 10), (50, 23), (62, 16), (55, 3)]
        assert len(words[1]['content']) == 2
        chars1 = words[1]['content']
        assert chars1[0]['x'] == [(42, 10), (48, 21), (54, 17), (48, 7)]
        assert chars1[1]['x'] == [(48, 7), (56, 20), (62, 16), (55, 3)]


    def test_transform_sequence(self):
        text_block = {'type': 'text', 'text': 'Test_of_size', 'transform': [{'rotation': 45},
                                                                            {'scale': (1.8, 1)}]}
        tx = te.TextEntity(text_block)
        assert tx.get_size() == (126, 70)
        text_block = {'type': 'text', 'text': 'Test_of_size', 'transform': [{'scale': (1.8, 1)},
                                                                            {'rotation': 45}]}
        tx = te.TextEntity(text_block)
        assert tx.get_size() == (118, 118)

    def test_multiline(self):
        text_block = {'text': 'Test of size of 50. Hopefully it will work',
                      'type': 'text', 'align': 'left', 'width': 50, 'spacing': 4}
        tx = te.TextEntity(text_block)
        new_img = tx.get_img().convert("RGB")
        file_rot30 = os.path.join(self.dirname, 'data/test_multiline_left.png')
        ref_img = Image.open(file_rot30).convert("RGB")
        assert new_img.size == ref_img.size
        new_array = np.array(new_img)
        ref_array = np.array(ref_img)
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0
        text_block = {'text': 'Test of size of 50. Hopefully it will work',
                      'type': 'text', 'align': 'right', 'width': 50, 'spacing': 4}
        tx = te.TextEntity(text_block)
        new_img = tx.get_img().convert("RGB")
        file_rot30 = os.path.join(self.dirname, 'data/test_multiline_right.png')
        ref_img = Image.open(file_rot30).convert("RGB")
        assert new_img.size == ref_img.size
        new_array = np.array(new_img)
        ref_array = np.array(ref_img)
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0
        text_block = {'text': 'Test of size of 50. Hopefully it will work',
                      'type': 'text', 'align': 'center', 'width': 50, 'spacing': 4}
        tx = te.TextEntity(text_block)
        new_img = tx.get_img().convert("RGB")
        file_rot30 = os.path.join(self.dirname, 'data/test_multiline_center.png')
        ref_img = Image.open(file_rot30).convert("RGB")
        assert new_img.size == ref_img.size
        new_array = np.array(new_img)
        ref_array = np.array(ref_img)
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0

    def test_align_c(self):
        text_block = {'type': 'text', 'text': 'Test_of_size', 'align': 'cb'}
        tx = te.TextEntity(text_block)
        assert tx.align == 'center'

    def test_align_r(self):
        text_block = {'type': 'text', 'text': 'Test_of_size', 'align': 'r'}
        tx = te.TextEntity(text_block)
        assert tx.align == 'right'

    def test_align_l(self):
        text_block = {'type': 'text', 'text': 'Test_of_size', 'align': 'lt'}
        tx = te.TextEntity(text_block)
        assert tx.align == 'left'

    def test_multiline_width(self):
        text_block = {'type': 'text', 'width': 250, 'text': 'Article 9456\nmagnetic field\n_____2 * 5.7'}
        tx = te.TextEntity(text_block)
        # there must be approximately 3 lines: 3 x 15 = 45. This value might change a bit due to line spacing and white
        # space so it should be between 40 and 50
        assert 40 < tx.get_size()[1] < 50

    def test_leading_spaces(self):
        text_block = {'type': 'text', 'width': 250, 'text': 'Article 9456\nmagnetic field\n2 * 5.7'}
        tx = te.TextEntity(text_block)
        width1 = tx.get_size()[0]
        text_block = {'type': 'text', 'width': 250, 'text': 'Article 9456\nmagnetic field\n        2 * 5.7'}
        tx = te.TextEntity(text_block)
        width2 = tx.get_size()[0]
        # there must be approximately 3 lines: 3 x 15 = 45. This value might change a bit due to line spacing and white
        # space so it should be between 40 and 50
        assert width1 < width2

#
# defaults = {'font': 'DejaVuSansMono', 'font_size': 12, 'font_color': 'black',
#             'color_mode': 'RGB', 'background_color': 'white'}
#
# te.TextEntity.set_defaults(defaults)
#
# text_block = {'text': 'Test of size of 50. Hopefully it will work', 'type': 'text', 'align': 'center', 'width': 50}
# text_block = {'type': 'text', 'text': 'Test_of_size', 'transform': [{'rotation': 45},
#                                                                     {'scale': (1.8, 1)}]}
# text_block = {'type': 'text', 'width': 250, 'text': 'Article 9456\nmagnetic field\n 2 * 5.7g'}
#
# t = te.TextEntity(text_block)
# i = t.get_img()
# i.save('data/test_multiline_center.png', 'PNG')
# i.show()
# print(t.get_size())
# print(t.bbox())
# text_block = {'type': 'text', 'text': 'Test_of_size', 'transform': [{'scale': (1.8, 1)},
#                                                                     {'rotation': 45}]}
#
# t = te.TextEntity(text_block)
# t.get_img().show()
# print(t.get_size())

# from PIL import ImageFont, ImageDraw
#
# font_file = os.path.join('fonts', 'DejaVuSansMono')
# font = ImageFont.truetype(font_file, 24)
#
# img = Image.new('RGB', (100, 100))
# draw = ImageDraw.Draw(img)
# print(draw.textsize('bbc', font))
# print(draw.textsize('bbg', font))
# print(draw.textsize('ccc', font))
# line = 'bbc\nbbg\nbbc'
# line1 = 'ccc\nccc\nccc'
# print(draw.multiline_textsize(line, font))
# print(draw.multiline_textsize(line1, font))
# print(draw.multiline_textsize('ccc', font))
# draw.multiline_text((10,10), line, font=font)
# #img.show()
