from data_generator.combinator import SlotCombinator, SliceFormatter
from data_generator.abstract_value import RandomValue
from data_generator.random_table import RandomTable, ItemTable
import string
import pandas as pd
import numpy as np
import re

class TestSlotCombinator:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_combinator_names_from_files(self):
        weights = [10, 5, 1, 1]
        b = SlotCombinator({"slots": [{"distribution": "choice",
                                       "file": 'data_generator/data/words/names.csv'},
                                      {"distribution": "choice",
                                       "file": 'data_generator/data/words/surnames.csv'}],
                           "format": {'distribution': 'choice', 'choice_from': ['{0} {1}', '{0}', 'Mr {1}', 'Mrs {1}'],
                                      'weights': weights}})
        s = b()
        assert type(s) is str
        assert len(s) > 1
        s = b(1000)
        assert type(s) is list
        assert len(s) == 1000
        # below test of actual format distribution is very expensive because of multiple regexp check,
        # so it will be done once for slot combinator
        patterns = ['(?!Mr )(?!Mrs )\w* \w*', '\w*', 'Mr \w*', 'Mrs \w*']
        patterns = [re.compile("^" + p + "$") for p in patterns]
        matches = [[1 if p.search(st) is not None else 0 for p in patterns] for st in s]
        stats = (np.sum(np.array(matches), axis=0)*sum(weights)/1000).tolist()
        assert all([abs((t - a)/t) < .2 for t, a in zip(weights, stats)])
        print(matches)

    # def test_phone_number(self):
    #     b = PhoneGenerator({'digits': 11,
    #                         'prefix': {"distribution": "choice",
    #                                    "choice_from": ['01', '07', '020'],
    #                                    "weights": [5, 3, 2]},
    #                         'format': {"distribution":"choice",
    #                                    "choice_from": ["{0|0,5} {0|5,8} {0|8,11}",
    #                                                    "{0|0,5} {0|5,11}",
    #                                                    "{0|0,3} {0|3,7} {0|7,11}"],
    #                                    "weights": [5, 2, 1]}})
    #     s = b()
    #     assert type(s) is str
    #     assert len(s) > 1
    #     s = b(100)
    #     assert type(s) is list
    #     assert len(s) == 100

    def test_phone_stacked_combinators(self):
        a = SlotCombinator({'slots': [{"distribution": "choice",
                                       "choice_from": ['01', '07', '020'],
                                       "weights": [5, 3, 2]},
                                      {"distribution": "randstr", "chars": "1234567890", "len": 11}],
                            'format': "{0}{1}"})
        b = SlotCombinator({'slots': [a],
                            'format': {"distribution": "choice",
                                       "choice_from": ["{0|0,5} {0|5,8} {0|8,11}",
                                                       "{0|0,5} {0|5,11}",
                                                       "{0|0,3} {0|3,7} {0|7,11}"],
                                       "weights": [5, 2, 1]}})
        s = b()
        assert type(s) is str
        assert len(s) > 1
        s = b(100)
        assert type(s) is list
        assert len(s) == 100
        re_formats = ["^\d{5} \d{3} \d{3}$", "^\d{5} \d{6}$", "^\d{3} \d{4} \d{4}$"]
        assert all([any([re.search(f, t) is not None for f in re_formats]) for t in s])

    def test_postcodes(self):
        d = SlotCombinator({'slots': [{'distribution': 'randstr', 'case': 'u',
                                       'len': {'distribution': 'choice', 'choice_from': [1, 2], 'weights': [1, 10]}},
                                      {'distribution': 'randint', 'low': 0, 'width': 99},
                                      {'distribution': 'randint', 'low': 0, 'width': 9},
                                      {'distribution': 'randstr', 'case': 'u', 'len': 2}],
                            'format': '{0}{1} {2}{3}'})
        s = d()
        assert type(s) is str
        assert len(s) > 1
        # quickly check if it matches above UK postcode format
        assert re.search('^[A-Z]{1,2}\d{1,2} \d[A-Z]{2}$', s) is not None
        s = d(100)
        assert type(s) is list
        assert len(s) == 100

    def test_item_names(self):
        d = SlotCombinator({"slots": [{"distribution": "choice",
                                       "file": 'data_generator/data/words/adjectives.csv'},
                                      {"distribution": "choice",
                                       "file": 'data_generator/data/words/nouns.csv'},
                                      {"distribution": "choice",
                                       "file": 'data_generator/data/words/nouns.csv'}],
                            "format": {"distribution": "choice", "choice_from": ["{0} {1}", "{0} {1} {2}"],
                                       "weights": [1, 1]}})
        s = d(100)
        s = [len(t.split()) for t in s]
        s = pd.Series(s).value_counts()/len(s)
        assert abs(s[2] - 0.5) < 0.1
        assert abs(s[3] - 0.5) < 0.1

    def test_slot_dictionary(self):
        def capitalize(x):
            return x.capitalize()
        street = SlotCombinator({"slots": [{"distribution": "choice", "mod_func": capitalize,
                                            "file": 'data_generator/data/words/nouns.csv'},
                                           {"distribution": "choice",
                                            "choice_from": ["Road", "Drive", "Close", "Mews", "Crescent", "Lane",
                                                            "Ride", "St.", "Cl.", "Cr.", "Ln."]}],
                                 "format": {"distribution": "choice",
                                            "choice_from": ["{0} {1}"]}})
        street_address1 = SlotCombinator({"slots": {"number": {"distribution": "lognormint", "mean": 10, "scale": 5},
                                                    "street": street,
                                                    "town": {"distribution": "choice",
                                                             "file": 'data_generator/data/words/towns.csv'}},
                                          "format": {"number": "{number}", "street": "{street}",
                                                     "town": "{town}"}})
        a = street_address1()
        assert type(a) == dict
        assert 'number' in a
        assert 'street' in a
        assert 'town' in a
        assert len(a) == 3
        a = street_address1(10)
        assert type(a) == list
        assert all(['number' in l for l in a])
        assert all(['street' in l for l in a])
        assert all(['town' in l for l in a])
        assert all([len(l) == 3 for l in a])

    def test_unnamed_slots(self):
        a = SlotCombinator({"slots": [{"distribution": "choice",
                                       "file": 'data_generator/data/words/adjectives.csv'},
                                      {"distribution": "choice",
                                       "file": 'data_generator/data/words/nouns.csv'}],
                            "format": {"distribution": "choice",
                                       "choice_from": [{'name': "{0} {1}", 'email': "info@{0}-{1}.com"},
                                                       {'name': "{0}", 'email': "info@{0}.com"},
                                                       {'name': "{1}", 'email': "info@{1}.com"}],
                                       "weights": [1, 1, 1]}})
        s = a()
        assert type(s) == dict
        assert 'name' in s
        assert 'email' in s
        assert len(s) == 2

    def test_list_output(self):
        a = SlotCombinator({"slots": [{"distribution": "choice",
                                       "file": 'data_generator/data/words/adjectives.csv'},
                                      {"distribution": "choice",
                                       "file": 'data_generator/data/words/nouns.csv'}],
                            "format": {"distribution": "choice",
                                       "choice_from": [["{0} {1}", "info@{0}-{1}.com"],
                                                       ["{0}", "info@{0}.com"],
                                                       ["{1}", "info@{1}.com"]],
                                       "weights": [1, 1, 1]}})
        s = a()
        assert type(s) == list
        assert len(s) == 2


choice_from = [['1', '2'], ['3', '4'], ['3']]
choice = np.random.choice(len(choice_from), 1).tolist()

print([choice_from[c] for c in choice])

#
# shop_name = SlotCombinator({"slots": [{"distribution": "choice",
#                                        "file": ['..', 'data_generator', 'data', 'words', 'adjectives.csv']},
#                                       {"distribution": "choice",
#                                        "file": ['..', 'data_generator', 'data', 'words', 'nouns.csv']}],
#                             "format": {"distribution": "choice",
#                                        "choice_from": [{'name': "{0} {1}", 'email': "info@{0}-{1}.com"},
#                                                        {'name': "{0}", 'email': "info@{0}.com"},
#                                                        {'name': "{1}", 'email': "info@{1}.com"}],
#                                        "weights": [1, 1, 1]}})
#
# print(shop_name())
#
# shop_name = SlotCombinator({"slots": [{"distribution": "choice",
#                                        "file": ['..', 'data_generator', 'data', 'words', 'adjectives.csv']},
#                                       {"distribution": "choice",
#                                        "file": ['..', 'data_generator', 'data', 'words', 'nouns.csv']},
#                                       {"distribution": "choice",
#                                        "choice_from": ["info", "contact", "customer.service"]},
#                                       {"distribution": "choice",
#                                        "choice_from": ["com", "co.uk", "net", "org"],
#                                        "weights":[1, 1, .2, .1]}],
#                             "format": {"distribution": "choice",
#                                        "choice_from": [{'name': "{0} {1}", 'email': "{2}@{0}-{1}.{3}"},
#                                                        {'name': "{0}", 'email': "{2}@{0}.{3}"},
#                                                        {'name': "{1}", 'email': "{2}@{1}.{3}"}],
#                                        "weights": [1, 1, 1]}})
#
# print(shop_name())
#
# shop_name = SlotCombinator({"slots": {"name1":
#                                       {"distribution": "choice",
#                                        "file": ['..', 'data_generator', 'data', 'words', 'adjectives.csv']},
#                                       "name2":
#                                       {"distribution": "choice",
#                                        "file": ['..', 'data_generator', 'data', 'words', 'nouns.csv']},
#                                       "email_account":
#                                       {"distribution": "choice",
#                                        "choice_from": ["info", "contact", "customer.service"]},
#                                       "domain":
#                                       {"distribution": "choice",
#                                        "choice_from": ["com", "co.uk", "net", "org"],
#                                        "weights": [1, 1, .2, .1]}},
#                             "format": {"distribution": "choice",
#                                        "choice_from": [{'name': "{name1} {name2}", 'email': "{email_account}@{name1}-{name2}.{domain}"},
#                                                        {'name': "{name1}", 'email': "{email_account}@{name1}.{domain}"},
#                                                        {'name': "{name2}", 'email': "{email_account}@{name2}.{domain}"}],
#                                        "weights": [1, 1, 1]}})
#
# print(shop_name())
#
# shop_name = SlotCombinator({"slots": {"name1":
#                                           {"distribution": "choice",
#                                            "file": ['..', 'data_generator', 'data', 'words', 'adjectives.csv']},
#                                       "name2":
#                                           {"distribution": "choice",
#                                            "file": ['..', 'data_generator', 'data', 'words', 'nouns.csv']},
#                                       "email_account":
#                                           {"distribution": "choice",
#                                            "choice_from": ["info", "contact", "customer.service"]},
#                                       "domain":
#                                           {"distribution": "choice",
#                                            "choice_from": ["com", "co.uk", "net", "org"],
#                                            "weights": [1, 1, .2, .1]},
#                                       "web_prefix":
#                                           {"distribution": "choice",
#                                            "choice_from": ["www.", "http://", ""]}},
#                             "format": {"distribution": "choice",
#                                        "choice_from": [{'name': "{name1} {name2}",
#                                                         'email': "{email_account}@{name1}-{name2}.{domain}",
#                                                         "web": "{web_prefix}{name1}-{name2}.{domain}"},
#                                                        {'name': "{name1}", 'email': "{email_account}@{name1}.{domain}",
#                                                         "web": "{web_prefix}{name1}.{domain}"},
#                                                        {'name': "{name2}", 'email': "{email_account}@{name2}.{domain}",
#                                                         "web": "{web_prefix}{name2}.{domain}"}],
#                                        "weights": [1, 1, 1]}})
#
# print(shop_name())