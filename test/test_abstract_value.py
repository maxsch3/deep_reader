from data_generator.abstract_value import RandomValue
import numpy as np
import pandas as pd
import os
# from numbers import Number
# import seaborn as sns
# import matplotlib.pyplot as plt


class TestRandomGenerator:

    epsilon = 10**(-5)

    @classmethod
    def setup_class(cls):
        cls.fruits = pd.DataFrame({'fruit': ['orange', 'apple', 'pear'], 'weights': [0.1, 0.5, 0.4]})

    @classmethod
    def teardown_class(cls):
        pass

    def test_choice_pandas_single(self):
        gen = RandomValue({'distribution': 'choice', 'choice_from': self.fruits.fruit})
        s = gen()
        assert type(s) is str
        assert s in self.fruits.fruit.values

    def test_choice_pandas_multiple_no_weights(self):
        gen = RandomValue({'distribution': 'choice', 'choice_from': self.fruits.fruit})
        s = pd.Series(gen(1000))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.33) < 0.05

    def test_choice_pandas_multiple_weights(self):
        gen = RandomValue({'distribution': 'choice', 'choice_from': self.fruits.fruit,
                           'weights': self.fruits.weights})
        s = gen()
        assert type(s) is str
        s = gen(1000)
        assert type(s) is list
        s = pd.Series(s)
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - self.fruits.weights.loc[self.fruits.fruit == 'orange']).values[0] < 0.05
        assert abs(s['apple'] - self.fruits.weights.loc[self.fruits.fruit == 'apple']).values[0] < 0.05
        assert abs(s['pear'] - self.fruits.weights.loc[self.fruits.fruit == 'pear']).values[0] < 0.05

    def test_normal(self):
        gen = RandomValue({'distribution': 'normal', 'mean': 10, 'scale': 1})
        s = gen()
        assert type(s) in [float, np.float32, np.float64]
        s = gen(10000)
        assert type(s) is list
        assert abs(np.mean(s) - 10) < .1
        assert abs(np.std(s) - 1) < .1

    def test_lognormal(self):
        gen = RandomValue({'distribution': 'lognormal', 'mean': 10, 'scale': 1})
        s = gen()
        assert type(s) in [float, np.float32, np.float64]
        s = gen(10000)
        assert type(s) is list
        assert abs(np.mean(s) - 10) < .5
        assert abs(np.std(s) - 1) < .5

    def test_randint(self):
        gen = RandomValue({'distribution': 'randint', 'low': 10, 'width': 4})
        s = gen()
        assert type(s) in [int, np.int32, np.int64]
        s = gen(10000)
        assert type(s) is list
        s = pd.Series(s)
        s = s.value_counts() / len(s)
        assert abs(s[10] - .25) < 0.05
        assert abs(s[11] - .25) < 0.05
        assert abs(s[12] - .25) < 0.05
        assert abs(s[13] - .25) < 0.05

    def test_binomial(self):
        gen = RandomValue({'distribution': 'binomial', 'p': .1})
        s = gen()
        assert type(s) in [int, np.int32, np.int64]
        s = gen(1000)
        assert type(s) is list
        s = sum(s)/1000
        assert abs(s - .1) < .05

    def test_set_seed(self):
        gen = RandomValue({'distribution': 'randint', 'low': 0, 'width': 100000, 'seed': 123})
        s = gen()
        # with seed set above it will 'randomly' generate below number all the times
        assert s == 15725

    def test_choice_from_file_no_weights(self):
        filename = os.path.join('test', 'data', 'test_choice_no_weights.csv')
        gen = RandomValue({'distribution': 'choice', 'file': filename})
        s = gen()
        assert type(s) is str
        s = gen(10000)
        assert type(s) is list
        s = pd.Series(s)
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.33) < 0.01

    def test_choice_from_file_with_weights(self):
        filename = os.path.join('test', 'data', 'test_choice_with_weights.csv')
        gen = RandomValue({'distribution': 'choice', 'file': filename})
        s = pd.Series(list(gen(10000)))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.1) < 0.01

    def test_choice_from_file_with_weights_and_header(self):
        filename = os.path.join('test', 'data', 'test_choice_with_weights_and_header.csv')
        gen = RandomValue({'distribution': 'choice', 'file': filename})
        s = pd.Series(list(gen(10000)))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.1) < 0.01

    def test_randstr_def(self):
        d = RandomValue({'distribution': 'randstr', 'len': 5, 'case': 'upper'})
        s = d()
        assert type(s) is str
        assert len(s) == 5
        s = d(10)
        assert type(s) is list
        assert len(s) is 10
        assert all([type(p) is str for p in s])
        s = d(1000)
        s = list(''.join(s))
        s = pd.Series(s)
        uni = s.unique().tolist()
        assert all([u in list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') for u in uni])

    def test_randstr_def_upper(self):
        d = RandomValue({'distribution': 'randstr', 'len': 5, 'case': 'upper'})
        s = d()
        assert type(s) is str
        assert len(s) == 5
        s = d(10)
        assert type(s) is list
        assert len(s) is 10
        assert all([type(p) is str for p in s])
        s = d(1000)
        s = list(''.join(s))
        s = pd.Series(s)
        uni = s.unique().tolist()
        assert all([u in list('ABCDEFGHIJKLMNOPQRSTUVWXYZ') for u in uni])

    def test_randstr_def_lower(self):
        d = RandomValue({'distribution': 'randstr', 'len': 5, 'case': 'lower'})
        s = d()
        assert type(s) is str
        assert len(s) == 5
        s = d(10)
        assert type(s) is list
        assert len(s) is 10
        assert all([type(p) is str for p in s])
        s = d(1000)
        s = list(''.join(s))
        s = pd.Series(s)
        uni = s.unique().tolist()
        assert all([u in list('abcdefghijklmnopqrstuvwxyz') for u in uni])

    def test_randstr_def_custom_chars(self):
        d = RandomValue({'distribution': 'randstr', 'len': 5, 'chars': '0123456789ABCDEF'})
        s = d()
        assert type(s) is str
        assert len(s) == 5
        s = d(10)
        assert type(s) is list
        assert len(s) is 10
        assert all([type(p) is str for p in s])
        s = d(1000)
        s = list(''.join(s))
        s = pd.Series(s)
        uni = s.unique().tolist()
        assert all([u in list('0123456789ABCDEF') for u in uni])

    def test_modifier_function(self):
        def str2int(x):
            return int(x)
        d = RandomValue({"distribution": "randstr", "chars": "123456789", "len": 16, "mod_func": str2int})
        val = d()
        assert type(val) == int
        assert 1000000000000000 < val < 9999999999999999
        val = d(10)
        assert type(val) == list
        assert all([type(v) == int for v in val])
        assert all([1000000000000000 <= v < 9999999999999999 for v in val])


# fruits = pd.DataFrame({'fruit': ['orange', 'apple', 'pear'], 'weights': [0.1, 0.5, 0.4]})
# a = RandomValue({'distribution': 'choice', 'choice_from': fruits.fruit, 'weights': fruits.weights})
# #a = RandomValue({'distribution': 'normal', 'mean': {'distribution': 'randint', 'low': 1, 'width': 5}, 'scale': 3})
# sample = a(100)
# print(sample)
# #sns.distplot(sample, kde=False)
# plt.show()
#
# a = RandomValue({"distribution": "randstr", "case": 'u',
#                  'len': {'distribution': 'choice', 'choice_from': [1, 2], 'weights': [1, 10]}})
# print(a(10))
