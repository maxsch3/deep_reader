from data_generator import random_template
from data_generator import random_table
from jinja2 import Environment, FileSystemLoader, meta, Template
from data_generator import document, text_entity
import json
import os
#
# template_root = "../data_generator/templates"
# template_header = random_template.RandomTemplate(template_root, "header", "json")
# template_address = random_template.RandomTemplate(template_root, "address", "txt")
# template_item = random_template.RandomTemplate(template_root, "item", "json", weight_file="weights.csv")
# template_logo = random_template.RandomTemplate(template_root, "logo", "png")
# items = random_table.RandomTable()
# for _ in range(10):
#     print(template_address.value)
#
# address = {"number": 2, "street": "Oxford st.", "town": "Bristol", "postcode": "BS03 4FG"}
# loyalty = {"name": "IKEA FAMILY C", "id": "62734867547365800XXXXXXXXX4730"}
#
# env = Environment(loader=FileSystemLoader(template_root))
# template = env.get_template("document/document1.json")
# parameters = {"template_header": template_header.value,
#               "template_address": template_address.value,
#               "template_item": template_item.value,
#               "logo_image": os.path.join(template_root, template_logo.value),
#               "address": address,
#               "items": items,
#               "currency": "GBP",
#               "loyalty": loyalty}
# out = template.render(**parameters)
# print(out)
# d = json.loads(out)
# print(d)
#
# defaults = {'font': 'DejaVuSansMono',
#             'font_size': 12,
#             'font_color': 'black',
#             'color_mode': 'RGB',
#             'background_color': 'white'}
#
# text_entity.TextEntity.set_defaults(defaults)
#
# doc = document.Document(d)
# img = doc.get_img()
# img.show()
#
