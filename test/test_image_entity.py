from data_generator import image_entity as im
from PIL import Image
import numpy as np


class TestImageEntity:

    epsilon = 10**(-5)

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_from_path(self):
        image_block = {'type': 'image', 'image': 'test/data/entity_ref.png'}
        image = im.ImageEntity(image_block)
        assert image.get_size() == (200, 150)
        # image.add_rotation(30)
        image.apply_transformations({'rotation': 30})
        new_img = image.get_img().convert("RGB")
        file_rot30 = 'test/data/test_entity_ref_rot30.png'
        ref_img = Image.open(file_rot30).convert("RGB")
        assert new_img.size == ref_img.size
        new_array = np.array(new_img)
        ref_array = np.array(ref_img)
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0
        image.reset_transformation()
        # image.add_shear((30, -10))
        image.apply_transformations({'shear': (30, -10)})
        file_shear = 'test/data/test_entity_ref_shr30-10.png'
        new_array = np.array(image.get_img().convert("RGB"))
        ref_array = np.array(Image.open(file_shear).convert("RGB"))
        assert new_array.shape == ref_array.shape
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0

    def test_bbox(self):
        img_file = 'test/data/entity_ref.png'
        image_block = {'type': 'image', 'image': img_file}
        image = im.ImageEntity(image_block)
        box = image.bbox()
        assert 'type' in box
        assert 'content' in box
        assert box['type'] == 'image'
        assert box['content'] == img_file

    def test_width_parameter(self):
        img_file = 'test/data/entity_ref.png'
        image_block = {'type': 'image', 'image': img_file}
        image = im.ImageEntity(image_block)
        size = image.get_size()
        width = 100
        image_block = {'type': 'image', 'image': img_file, 'width': width}
        image = im.ImageEntity(image_block)
        assert image.get_size() == (width, int(size[1] * width / size[0]))

    def test_height_parameter(self):
        img_file = 'test/data/entity_ref.png'
        image_block = {'type': 'image', 'image': img_file}
        image = im.ImageEntity(image_block)
        size = image.get_size()
        height = 100
        image_block = {'type': 'image', 'image': img_file, 'height': height}
        image = im.ImageEntity(image_block)
        assert image.get_size() == (int(size[0] * height / size[1]), height)
