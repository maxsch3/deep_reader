from data_generator import barcode_entity as bar
from data_generator import text_entity as tex


class TestBarcodeEntity:

    @classmethod
    def setup_class(cls):
        cls.defaults = {'font': 'DejaVuSansMono',
                        'font_size': 12,
                        'font_color': 'black',
                        'color_mode': 'RGB',
                        'background_color': 'white',
                        'barcode_height': 100,
                        'barcode_type': 'code128'}
        bar.BarcodeEntity.set_defaults(cls.defaults)
        tex.TextEntity.set_defaults(cls.defaults)

    @classmethod
    def teardown_class(cls):
        pass

    def test_parameters_inheritance(self):
        """This is a very important test checking how default and instance specific attributes are set through
        base class methods"""
        defaults1 = {'barcode_height': 50, 'font_size': 32, 'font': 'DejaVuSansMono', 'font_color': 'black'}
        assert bar.BarcodeEntity.barcode_height == self.defaults['barcode_height']
        assert bar.BarcodeEntity.font_size == self.defaults['font_size']
        assert tex.TextEntity.font_size == self.defaults['font_size']
        # Now I will set default parameters of BarcodeEntity
        bar.BarcodeEntity.set_defaults(defaults1)
        assert bar.BarcodeEntity.barcode_height == defaults1['barcode_height']
        assert bar.BarcodeEntity.font_size == defaults1['font_size']
        # check that other classes were not affected
        assert tex.TextEntity.font_size == self.defaults['font_size']
        # check how instance and class attributes work together
        text_block = {'type': 'text', 'text': 'test_text', 'font_size': 16}
        text_block1 = {'type': 'text', 'text': 'test_text'}
        text = tex.TextEntity(text_block)
        text1 = tex.TextEntity(text_block1)
        assert text.font_size == text_block['font_size']
        assert text1.font_size == self.defaults['font_size']
        # Now that read-only attributes are not overwritten
        defaults1 = {'_block_name':'wrong_name','barcode_height': 50, 'font_size': 32,
                     'font': 'DejaVuSansMono', 'font_color': 'black'}
        bar.BarcodeEntity.set_defaults(defaults1)
        assert bar.BarcodeEntity._block_name != defaults1['_block_name']
        # Same check for class instance
        text_block = {'type': 'text', 'text': 'test_text', 'font_size': 16, '_block_name': 'wrong_name'}
        text = tex.TextEntity(text_block)
        assert text._block_name != text_block['_block_name']


# defaults = {'barcode_height': 50, 'font_size': 32, 'font': 'DejaVuSansMono', 'font_color': 'black'}
# defaults1 = {'barcode_height': 50, 'font_size': 33, 'font': 'DejaVuSansMono', 'font_color': 'black'}
# barcode_block = {'type': 'barcode', 'barcode': '12345678910', 'barcode_height': 50, 'font_size': 16}
# text_block = {'type': 'text', 'text': 'test_text', 'font_size': 16}
# text_block1 = {'type': 'text', 'text': 'test_text'}
#
# bar.BarcodeEntity.set_defaults(defaults)
# barcode = bar.BarcodeEntity(barcode_block)
# barcode.img.show()
