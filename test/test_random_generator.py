from data_generator.random_generator import RandomGenerator
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os


class TestRandomGenerator:

    epsilon = 10**(-5)

    @classmethod
    def setup_class(cls):
        cls.fruits = pd.DataFrame({'fruit': ['orange', 'apple', 'pear'], 'weights': [0.1, 0.5, 0.4]})

    @classmethod
    def teardown_class(cls):
        pass

    def test_choice_pandas_single(self):
        gen = RandomGenerator({'distribution': 'choice', 'choice_from': self.fruits.fruit})
        s = gen.sample()
        assert s in self.fruits.fruit.values

    def test_choice_pandas_multiple_no_weights(self):
        gen = RandomGenerator({'distribution': 'choice', 'choice_from': self.fruits.fruit})
        s = pd.Series(gen.sample(1000))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.33) < 0.05

    def test_choice_pandas_multiple_weights(self):
        gen = RandomGenerator({'distribution': 'choice', 'choice_from': self.fruits.fruit, 'p': self.fruits.weights})
        s = pd.Series(gen.sample(1000))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - self.fruits.weights.loc[self.fruits.fruit == 'orange']).values[0] < 0.05
        assert abs(s['apple'] - self.fruits.weights.loc[self.fruits.fruit == 'apple']).values[0] < 0.05
        assert abs(s['pear'] - self.fruits.weights.loc[self.fruits.fruit == 'pear']).values[0] < 0.05

    def test_iterate_choice(self):
        gen = RandomGenerator({'distribution': 'choice', 'choice_from': self.fruits.fruit, 'p': self.fruits.weights,
                               'n': 1000})
        assert len(gen) == 1000
        count = 0
        for g in gen:
            count += 1
        assert count == 1000

    def test_normal(self):
        gen = RandomGenerator({'distribution': 'normal', 'mean': 10, 'scale': 1, 'n': 1000})
        s = list(gen)
        assert abs(np.mean(s) - 10) < .1
        assert abs(np.std(s) - 1) < .1

    def test_lognormal(self):
        gen = RandomGenerator({'distribution': 'lognormal', 'mean': 10, 'scale': 1, 'n': 1000})
        s = np.log(list(gen))
        assert abs(np.mean(s) - 10) < 1
        assert abs(np.std(s) - 1) < .5

    def test_randint(self):
        gen = RandomGenerator({'distribution': 'randint', 'low': 10, 'high': 14, 'n': 1000})
        s = pd.Series(list(gen))
        s = s.value_counts() / len(s)
        assert abs(s[10] - .25) < 0.05
        assert abs(s[11] - .25) < 0.05
        assert abs(s[12] - .25) < 0.05
        assert abs(s[13] - .25) < 0.05

    def test_binomial(self):
        gen = RandomGenerator({'distribution': 'binomial', 'p': .1, 'n': 1000})
        s = sum(gen)/1000
        assert abs(s - .1) < .05

    def test_set_seed(self):
        gen = RandomGenerator({'distribution': 'randint', 'low': 0, 'high': 100000, 'n': 10, 'seed': 123})
        s = gen.sample()
        # with seed set above it will 'randomly' generate below number all the times
        assert s == 15725

    def test_choice_from_file_no_weights(self):
        filename = os.path.join('test', 'data', 'test_choice_no_weights.csv')
        gen = RandomGenerator({'distribution': 'choice', 'file': filename, 'n': 10000})
        s = pd.Series(list(gen))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.33) < 0.01

    def test_choice_from_file_with_weights(self):
        filename = os.path.join('test', 'data', 'test_choice_with_weights.csv')
        gen = RandomGenerator({'distribution': 'choice', 'file': filename, 'n': 10000})
        s = pd.Series(list(gen))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.1) < 0.01

    def test_choice_from_file_with_weights_and_header(self):
        filename = os.path.join('test', 'data', 'test_choice_with_weights_and_header.csv')
        gen = RandomGenerator({'distribution': 'choice', 'file': filename, 'n': 10000})
        s = pd.Series(list(gen))
        s = s.value_counts() / len(s)
        assert abs(s['orange'] - 0.1) < 0.01


#fruits = pd.DataFrame({'fruit': ['orange', 'apple', 'pear'], 'weights': [0.1, 0.5, 0.4]})
#gen = RandomGenerator({'distribution': 'choice', 'choice_from': fruits.fruit, 'p': fruits.weights, 'n': 1000})
#gen = RandomGenerator({'distribution': 'choice', 'file': 'test_choice.csv', 'n': 1000})
#s = pd.Series(list(gen)).value_counts()
#print(s)