from data_generator import entity as ent
from PIL import Image
import numpy as np
import os


class TestEntity:

    epsilon = 10**(-5)

    @classmethod
    def setup_class(cls):
        entity_block = {'type': 'entity', 'entity': 'dummy'}
        cls.e = ent.Entity(entity_block, render=True)
        cls.dirname = os.path.dirname(__file__)

    @classmethod
    def teardown_class(cls):
        pass

    def test_affine_apply(self):
        self.e.reset_transformation()
        self.e.apply_transformations([{'rotation': 30}, {'shear': (10, -20)}])
        newbox = self.e.get_box()
        assert newbox.shape == (4, 2)
        assert np.allclose(newbox, np.array([[0., 0.81520747],
                                             [0.77786191, 0.],
                                             [1.43056556, 0.68404029],
                                             [0.65270364, 1.49924776]]))

    def test_affine_align(self):
        self.e.reset_transformation()
        self.e.apply_transformations({'rotation': -45})
        assert np.allclose(self.e.transform.matrix, np.array([[0.70710678, -0.70710678, 0.70710678],
                                                              [0.70710678,  0.70710678,  0.],
                                                              [0.,          0.,          1.]]))
        self.e.apply_transformations({'scale': (1.2, .67)})
        assert np.allclose(self.e.transform.matrix, np.array([[0.84852814, -0.84852814, 0.84852814],
                                                              [0.47376154,  0.47376154, -0.],
                                                              [0.,          0.,          1.]]))

    def test_get_box(self):
        self.e.reset_transformation()
        newbox = self.e.get_box()
        assert newbox.shape == (4, 2)
        assert np.allclose(newbox, np.array([[0., 0.],
                                             [1., 0.],
                                             [1., 1.],
                                             [0., 1.]]))

    def test_get_img(self):
        self.e.reset_transformation()
        dir_name = os.path.dirname(__file__)
        file_base = os.path.join(dir_name, 'data/entity_ref.png')
        self.e.img = Image.open(file_base).convert("RGB")
        # test rotation
        self.e.apply_transformations({'rotation': 30})
        new_img = self.e.get_img()
        assert new_img.mode == self.e.color_mode
        new_img = new_img.convert('RGB')
        file_rot30 = os.path.join(self.dirname, 'data/test_entity_ref_rot30.png')
        ref_img = Image.open(file_rot30).convert("RGB")
        assert new_img.size == ref_img.size
        new_array = np.array(new_img)
        ref_array = np.array(ref_img)
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0
        # test shear
        self.e.reset_transformation()
        self.e.apply_transformations({'shear': (30, -10)})
        file_shear = os.path.join(self.dirname, 'data/test_entity_ref_shr30-10.png')
        new_array = np.array(self.e.get_img().convert("RGB"))
        ref_array = np.array(Image.open(file_shear).convert("RGB"))
        assert new_array.shape == ref_array.shape
        mse = np.sum(np.sqrt(new_array**2 - ref_array**2))
        assert mse == 0

    def test_bbox(self):
        self.e.reset_transformation()
        bbox = self.e.bbox()
        assert type(bbox) == dict
        assert 'type' in bbox
        assert 'content' in bbox
        assert 'x' in bbox
        assert bbox['type'] == 'entity'
        assert bbox['content'] == 'dummy'
        assert type(bbox['x']) == list
        assert len(bbox['x']) == 4
        assert all(type(p) == tuple for p in bbox['x'])
        assert all(len(p) == 2 for p in bbox['x'])
        # min of each dimension in x must be 0
        assert abs(min(p[0] for p in bbox['x'])) < self.epsilon
        assert abs(min(p[1] for p in bbox['x'])) < self.epsilon
        # test the same after some transformation applied
        self.e.apply_transformations({'rotation': -60})
        self.e.apply_transformations({'shear': (10, -7)})
        assert abs(min(p[0] for p in bbox['x'])) < self.epsilon
        assert abs(min(p[1] for p in bbox['x'])) < self.epsilon

    def test_get_size(self):
        self.e.reset_transformation()
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, 'data/entity_ref.png')
        self.e.img = Image.open(filename)
        self.e.apply_transformations([{'rotation': -45}, {'scale': (1.8, .67)}])
        assert self.e.get_size() == (445, 166)

    def test_accepts_pushed_down_parameters(self):
        assert ent.Entity.accepts_pushed_down_parameters() == []

    def test_block_type(self):
        assert ent.Entity.entity_type() == 'entity'

    def test_auto_naming(self):
        ent.Entity._instance_counter = 0
        entity_block = {'type': 'entity', 'entity': 'dummy'}
        e1 = ent.Entity(entity_block)
        e2 = ent.Entity(entity_block)
        assert e1.name == 'entity_0'
        assert e2.name == 'entity_1'

    def test_explicit_naming(self):
        ent.Entity._instance_counter = 0
        entity_block = {'type': 'entity', 'entity': 'dummy', 'name': 'my_entity'}
        e1 = ent.Entity(entity_block)
        assert e1.name == 'my_entity'
        entity_block = {'type': 'entity', 'entity': 'dummy'}
        e2 = ent.Entity(entity_block)
        assert e2.name == 'entity_0'

    def test_project_4point_identity(self):
        self.e.reset_transformation()
        file_base = os.path.join(self.dirname, 'data/entity_ref.png')
        self.e.img = Image.open(file_base).convert("RGB")
        self.e.apply_transformations({'project': [(-.1, -.1), (0, 1), (1, 1), (1, 0)]})
        ref = np.array([[1.2e+00, 1.0e-01, 0.0e+00],
                        [1.0e-01, 1.2e+00, 0.0e+00],
                        [5.0e-04, 5.0e-04, 1.0e+00]])
        print(self.e.transform.matrix)
        assert np.allclose(self.e.transform.matrix, ref)

    def test_project_8point(self):
        self.e.reset_transformation()
        file_base = os.path.join(self.dirname, 'data/entity_ref.png')
        self.e.img = Image.open(file_base).convert("RGB")
        print(self.e.img.size)
        self.e.apply_transformations({'project': [(0, 0), (0, 150), (200, 150), (200, 0),
                                                  (0, 0), (0, 150), (220, 165), (200, 0)]})
        ref = np.array([[9.16666667e-01,  0.00000000e+00,  0.00000000e+00],
                        [0.00000000e+00,  9.16666667e-01,  0.00000000e+00],
                        [-4.16666667e-04, -5.55555556e-04,  1.00000000e+00]])
        assert np.allclose(self.e.transform.matrix, ref)


# dirname = os.path.dirname(__file__)
# filename = os.path.join(dirname, 'data/entity_ref.png')
# entity_block = {'type': 'entity', 'entity': 'empty'}
#
# entity_block1 = {'type': 'entity', 'entity': 'empty', 'margin': 30}
# e = ent.Entity(entity_block)
# e1 = ent.Entity(entity_block1)
# e.img = Image.open(filename)
# e1.img = Image.open(filename)
# e.apply_transformations([{'rotation': -45}, {'scale': (1.8, .67)}])
# e1.apply_transformations([{'rotation': -45}, {'scale': (1.8, .67)}])
# e.get_img().show()
# e1.get_img().show()
# print(e.get_size())
# print(e1.get_size())
