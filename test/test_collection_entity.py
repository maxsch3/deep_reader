from data_generator import collection_entity as entity


class TestCollectionEntity():

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_padding_single(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'padding': 10}
        e = entity.CollectionEntity(entity_block)
        assert e.padding == [10, 10, 10, 10]

    def test_padding_multiple(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'padding': [10, 11, 12, 13]}
        e = entity.CollectionEntity(entity_block)
        assert e.padding == [10, 11, 12, 13]

    def test_align_default(self):
        entity_block = {'type': 'entity', 'entity': 'dummy'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'l'
        assert e.valign == 't'

    def test_align_horizontal_c(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'align': 'c'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'c'
        assert e.valign == 't'

    def test_align_horizontal_r(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'align': 'r'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'r'
        assert e.valign == 't'

    def test_align_vertical_m(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'align': 'm'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'l'
        assert e.valign == 'm'

    def test_align_vertical_b(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'align': 'b'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'l'
        assert e.valign == 'b'

    def test_align_vertical_t(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'align': 't'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'l'
        assert e.valign == 't'

    def test_align_cm(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'align': 'cm'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'c'
        assert e.valign == 'm'

    def test_align_br(self):
        entity_block = {'type': 'entity', 'entity': 'dummy', 'align': 'br'}
        e = entity.CollectionEntity(entity_block)
        assert e.halign == 'r'
        assert e.valign == 'b'
