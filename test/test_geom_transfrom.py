from data_generator import geom_transform as gt
import numpy as np


class TestGeomTransform:

    @classmethod
    def setup_class(cls):
        cls.trf = gt.GeomTransform()
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_expand_box(self):
        x = np.array([[0., 0.], [0., 1.], [1., 1.], [1., 0.]])
        ex = self.trf.expand_dim(x)
        assert ex.shape == (4, 3)

    def test_affine(self):
        self.trf.reset()
        assert np.allclose(self.trf.matrix, np.eye(3))
        # self.aff += af.GeomTransform(rotation=-45)
        self.trf.add_rotation(-45)
        # assert np.allclose(self.aff.affine, np.array([[0.70710678, -0.70710678,  0.70710678],
        #                                               [0.70710678,  0.70710678,  0.],
        #                                               [0.,          0.,          1.]]))
        # self.aff.add_scaling((1.2, .67))
        # assert np.allclose(self.aff.affine, np.array([[0.84852814, -0.84852814,  0.84852814],
        #                                               [0.47376154,  0.47376154, -0.],
        #                                               [0.,          0.,          1.]]))
        assert np.allclose(self.trf.matrix, np.array([[0.70710678, -0.70710678, 0.],
                                                      [0.70710678,  0.70710678,  0.],
                                                      [0.,          0.,          1.]]))
        # self.aff += af.GeomTransform(scale=(1.2, .67))
        self.trf.add_scale((1.2, .67))
        assert np.allclose(self.trf.matrix, np.array([[0.84852814, -0.84852814, 0.],
                                                      [0.47376154,  0.47376154,  0.],
                                                      [0.,          0.,          1.]]))

    def test_chain(self):
        # TODO
        pass

    def test_chain_init(self):
        # TODO
        pass

    def test_project_hourglass(self):
        # TODO: try non-convex rectangle of hourglass-like shape. Most likely there will be error
        pass
