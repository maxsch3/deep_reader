import glob
import os
import random
import json
import utils
import sys


class TemGen:
    special_templates = ['vspace']
    def __init__(self, path=""):
        self.template_catalog = self.load_catalog(path)

    def load_catalog(self, template_path):
        if template_path[-1] != '/':
            template_path += '/'
        doc_templates = glob.glob(template_path+'document/*json')
        catalog = {'document': doc_templates}
        block_templates = glob.glob(template_path+'blocks/*/*json', recursive=True)
        for t in block_templates:
            key = str(os.path.normpath(t).split(os.path.sep)[-2])
            if key not in catalog:
                catalog[key] = []
            catalog[key].append(t)
        return catalog

    def gen_template(self, template=None):
        doc = self.select_template('document', template)
        doc_template = self.load_template_file(doc)
        assert 'templates' in doc_template['document']
        block_templates = sum([self.pick_block_template(t) for t in doc_template['document']['templates']], [])
        doc_template['document']['blocks'] = block_templates
        return doc_template

    def pick_block_template(self, template):
        t_type, value = list(template.items())[0]
        if t_type not in self.special_templates:
            if t_type not in self.template_catalog:
                sys.exit('No templates of type ' + t_type + ' are defined!')
            if value == 'random':
                block = self.load_template_file(self.select_template(t_type))
            else:
                # should be a name of template
                value = self.find_matching_template(t_type, value)
                block = self.load_template_file(value)
        else:
            block = self.create_special_template(t_type, value)
        # each template file may deliver a list of different blocks that will be treated as separate blocks
        # so we will treat them as members of bigger list that will be aggregated into one list above this function
        # in order for list aggregation to work each member must be a list
        if type(block) is dict:
            block = [block]
        return block

    def select_template(self, t_type, name=None):
        assert t_type in self.template_catalog
        if name is None:
            return random.choice(self.template_catalog[t_type])
        else:
            return self.find_matching_template(t_type, name)

    def find_matching_template(self, t_type, name):
        assert t_type in self.template_catalog
        matching = [file for file in self.template_catalog[t_type] if name in file]
        if len(matching) == 0:
            sys.exit('Template ' + name + ' of type ' + t_type + ' not found!')
        return matching[0]

    def load_template_file(self, file):
        if not utils.check_file(file):
            exit(1)
        f = open(file, "r", encoding='utf-8')
        j = f.read()
        f.close()
        json_data = json.loads(j)
        return json_data

    def create_special_template(self, t_type, value):
        if t_type == 'vspace':
            return self.create_vspace_block(value)

    def create_vspace_block(self, value):
        return {'type': 'text', 'lines': [{'vspace': value}]}
