import numpy as np
import random
import math
from PIL import Image


class NormalDistribution:
    def __init__(self, parameters):
        self.mu = parameters[0]
        self.sigma = parameters[1]

    def value(self, n=None):
        if n is None:
            return random.normalvariate(self.mu, self.sigma)
        assert type(n) is int
        assert n > 0
        return [random.normalvariate(self.mu, self.sigma) for _ in range(0, n)]


class FixedDistribution:
    def __init__(self, value):
        self.v = value

    def value(self, n=None):
        if n is None:
            return self.v
        assert type(n) is int
        assert n > 0
        return [self.value]*n


class Rotation:
    def __init__(self, distribution):
        self.distribution = distribution

    def add(self, affine_matrix):
        assert type(affine_matrix) is np.ndarray
        assert affine_matrix.shape == (3, 3)
        angle = np.radians(self.distribution.value())
        delta = np.array([[math.cos(angle), math.sin(angle), 0.],
                          [-math.sin(angle), math.cos(angle), 0.],
                          [0., 0., 1.]])
        return np.matmul(affine_matrix, delta)


class Shear:
    def __init__(self, distribution):
        self.distribution = distribution

    def add(self, affine_matrix):
        assert type(affine_matrix) is np.ndarray
        assert affine_matrix.shape == (3, 3)
        angle = np.radians(self.distribution.value())
        delta = np.array([[1., math.tan(angle), 0.],
                          [0., 1., 0.],
                          [0., 0., 1.]])
        return np.matmul(affine_matrix, delta)


class Move:
    def __init__(self, distribution):
        self.distribution = distribution

    def add(self, affine_matrix):
        assert type(affine_matrix) is np.ndarray
        assert affine_matrix.shape == (3, 3)
        delta = np.array([[1., 0., self.distribution.value()],
                          [0., 1., self.distribution.value()],
                          [0., 0., 1.]])
        return np.matmul(affine_matrix, delta)


class Scale:
    def __init__(self, distribution):
        self.distribution = distribution

    def add(self, affine_matrix):
        assert type(affine_matrix) is np.ndarray
        assert affine_matrix.shape == (3, 3)
        factor = self.distribution.value()
        delta = np.array([[factor, 0., 0.],
                          [0., factor, 0.],
                          [0., 0.,     1.]])
        return np.matmul(affine_matrix, delta)


class Compress:
    def __init__(self, distribution):
        self.distribution = distribution

    def add(self, affine_matrix):
        assert type(affine_matrix) is np.ndarray
        assert affine_matrix.shape == (3, 3)
        factor = self.distribution.value()
        delta = np.array([[factor, 0., 0.],
                          [0., 1/factor, 0.],
                          [0., 0.,     1.]])
        return np.matmul(affine_matrix, delta)


class Transform:
    ttypes = [("move", Move), ("scale", Scale), ("rotation", Rotation),
              ("shear", Shear), ("compress", Compress)]
    dtypes = {'norm': NormalDistribution, 'fixed': FixedDistribution}
    d_defaults = {"move": {"class": NormalDistribution, "parameters": (0., 4)},
                  "scale": {"class": NormalDistribution, "parameters": (1., 0.1)},
                  "rotation": {"class": NormalDistribution, "parameters": (0., 5*math.pi/180)},
                  "shear": {"class": NormalDistribution, "parameters": (0., 2*math.pi/180)},
                  "compress": {"class": NormalDistribution, "parameters": (1., 0.1)}}
    nt = len(ttypes)

    def __init__(self, transformations=None):
        self.move, self.scale, self.rotation, \
        self.shear, self.compress = self.create_transformations(transformations)
        # calculate random transformation
        self.affine = self.generate_affine()

    def generate_affine(self):
        affine = np.identity(3)
        affine = self.rotation.add(affine)
        affine = self.shear.add(affine)
        affine = self.move.add(affine)
        affine = self.scale.add(affine)
        affine = self.compress.add(affine)
        return affine

    def dice(self):
        self.affine = self.generate_affine()

    def create_transformations(self, transformations):
        # if no transformations have been set up - make all random
        if transformations is None:
            transformations = ['random']*self.nt
        assert type(transformations) is list
        assert len(transformations) <= self.nt
        if len(transformations) < self.nt:
            # None for a transformation would mean no transformation or transformation with zero value
            transformations = transformations + [None]*(self.nt-len(transformations))  # Pad with Nones
        return [self.create_transformation(t, d) for t, d in zip(self.ttypes, transformations)]

    def create_transformation(self, ttype, distribution):
        if type(distribution) is dict:
            distribution = self.dtypes[distribution['type']](distribution['parameters'])
        elif distribution is None:
            distribution = self.zero_distribution()
        elif distribution == 'random':
            distribution = self.default_distribution(ttype[0])
        return ttype[1](distribution)

    def default_distribution(self, ttype):
        assert ttype in self.d_defaults
        return self.d_defaults[ttype]['class'](self.d_defaults[ttype]['parameters'])

    def zero_distribution(self):
        return FixedDistribution(0.)

    def __transform_bbox_xywh(self, bbox):
        # bboxes are usually described by x0,y0 and h,w
        # this definition cannot be transformed, so first it will ve converted to 4 points with x,y
        # each point will be padded with 1 to make 3 coordinates to enable matrix multiplication for affine transform
        box_points = self.__bbox_to_xy(bbox)
        new_box_points = self.__transform_polygon(box_points)
        xy, wh = self.__bbox_int(new_box_points)
        return xy, wh, new_box_points.round().astype(int)

    def transform_bbox(self, bbox):
        xy, wh, points = self.__transform_bbox_xywh(bbox)
        bbox['x'], bbox['y'] = xy[0], xy[1]
        bbox['w'], bbox['h'] = wh[0], wh[1]
        points = points.tolist()
        points = [tuple(t) for t in points]
        bbox['corners'] = points
        return bbox

    def transform_image(self, image):
        new_origin, size, points = self.__transform_xywh((0, 0), image.size)
        # image.transform applies transformation with origin located in upper left corner (0, 0),
        # so the image must be shifted so that corners are not cut
        matrix = self.__offset_affine(new_origin)
        # PIL.Image affine matrix maps output to pixels to source space which is inverse affine transform
        matrix = np.linalg.inv(matrix)
        matrix = matrix[:2, ...].flatten()
        image = image.convert('LA')
        new_image = image.transform(size, Image.AFFINE, data=matrix, resample=Image.BICUBIC)
        back_image = Image.new(new_image.mode, new_image.size, 'white')
        mask = new_image.split()[1]
        back_image.paste(new_image, (0, 0), mask)
        return back_image

    def transform_size(self, xy, wh):
        box_points = self.__xywh_to_xy(xy, wh)
        new_box_points = self.__transform_polygon(box_points)
        return self.__bbox_int(new_box_points)

    def adjust_origin_bbox(self, bbox):
        # this function calculates transformed bbox dimensions and adds translation component
        # to affine matrix to avoid clipping
        new_origin, size, points = self.__transform_bbox_xywh(bbox)
        self.affine = self.__offset_affine(new_origin)

    def adjust_origin_hw(self, hw, xy=(0, 0)):
        new_origin, size, points = self.__transform_xywh(xy, hw)
        self.affine = self.__offset_affine(new_origin)

    def __offset_affine(self, new_origin):
        return self.affine - np.array([[0, 0, new_origin[0]],
                                       [0, 0, new_origin[1]],
                                       [0, 0, 0]])

    def __transform_polygon(self, points):
        return np.matmul(points, np.transpose(self.affine))[..., :2]

    def __transform_xywh(self, xy, wh):
        box_points = self.__xywh_to_xy(xy, wh)
        new_box_points = self.__transform_polygon(box_points)
        xy, wh = self.__bbox_int(new_box_points)
        return xy, wh, new_box_points.round().astype(int)

    def __xywh_to_xy(self, xy, wh):
        return np.array([[xy[0], xy[1], 1.],
                         [xy[0] + wh[0], xy[1], 1.],
                         [xy[0] + wh[0], xy[1] + wh[1], 1.],
                         [xy[0], xy[1] + wh[1], 1.]])

    def __bbox_to_xy(self, bbox):
        return self.__xywh_to_xy((bbox['x'], bbox['y']), (bbox['w'], bbox['h']))

    def __bbox_to_xywh(self, bbox):
        # this should be a method of bbox class in the future when bbox is rewritten as class
        return (bbox['x'], bbox['y']), (bbox['w'], bbox['h'])

    def __bbox(self, points):
        origin = np.min(points, 0)
        return tuple(origin), tuple(np.max(points, 0) - origin)

    def __bbox_int(self, points):
        origin, size = self.__bbox(points)
        return (int(round(origin[0])), int(round(origin[1]))), (int(round(size[0])), int(round(size[1])))
