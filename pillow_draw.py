from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import numpy as np
import utils
import math
import sys
import code128
import bbox as bb


def load_font(fontname, size):
    fontfile = 'fonts/'+fontname
    if not utils.check_file(fontfile):
        exit(1)
    font = ImageFont.truetype(fontfile, size)
    return font


def vpaste(canvas, patch):
    w, h = canvas.size
    w1, h1 = patch.size
    new_image = Image.new(canvas.mode,(w,(h+h1)), 'white')
    new_image.paste(canvas, (0, 0))
    new_image.paste(patch, (0, h))
    return new_image


def new_patch(meta, height, width=None):
    w = meta['width']*meta['resolution'] if width is None else width
    return Image.new(meta['color_mode'], (w, height), 'white')


def eval_token(data, token, allow_missing=False):
    if token[0] == '$':
        token = token[1:]  # strip leading $ sign
        if token in data:
            return data[token]
        else:
            if allow_missing:
                return token
            else:
                return None
    else:
        return token


def eval_text(values, tokens, allow_missing=None):
    if allow_missing is None:
        allow_missing = [True] * len(tokens)
    if type(allow_missing) is not list and type(tokens) is list:
        allow_missing = [allow_missing] * len(tokens)
    if type(tokens) is str:
        tokens = [tokens]
    string = [eval_token(values, t, allow_missing) for t in tokens]
    if None in string:
        return None
    string = [str(s) for s in string]
    return ''.join(string)


def get_text_dim(str, font):
    #im = Image.new('L', (10, 10), 'white')
    #d = ImageDraw.Draw(im)
    #return d.textsize(str, font)
    return font.getsize(str)


def get_line_height(font, meta):
    base_height = font.getsize('Ap')[1]
    if 'line_space' in meta:
        return math.ceil(base_height * meta['line_space'])
    else:
        return base_height


def draw_text_word(word, font, meta):
    # this function draws separate word and returns a patch image with this word rendered
    # for now, we will not care about characters. Maybe in the future will do character bboxes
    size = get_text_dim(word, font)
    im = new_patch(meta, height=size[1], width=size[0])
    draw = ImageDraw.Draw(im)
    draw.text((0, 0), word, 'black', font)
    bbox = bb.bbox_new('word', word, (0, 0), size)
    return im, bbox


def draw_text_string(string, font, meta):
    # this function renders a string of one of few words into a single patch image taking care of spaces.
    # it returns an image patch with the string rendered and list of bounding boxes for each word.
    # this function is required to gather bounding boxes for separate words, otherwise we could just use
    # PIL.ImageDraw.Draw.text which will take care about spacing in a string
    words = filter(None, string.split(' '))
    patches, bboxes = zip(*[draw_text_word(w, font, meta) for w in words])
    bboxes = list(bboxes)
    line_height = get_line_height(font, meta)
    # determine space width
    sw, sh = get_text_dim(' ', font)
    width = sum(0 if patch is None else patch.size[0] for patch in patches) + sw * (len(patches) - 1)
    patch = Image.new(meta['color_mode'], (width, line_height), 'white')
    hpos = 0
    for i, p in enumerate(patches):
        patch.paste(p, (hpos, 0))
        bboxes[i] = bb.bbox_adjust(bboxes[i], 'x', hpos)
        hpos = hpos + p.size[0] + sw
    return patch, bboxes


def draw_text_block(values, block, meta, text_type='string'):
    # this function renders a piece of template text into an image patch. Its main function is
    # converting template code for a text into a string by evaluating all tokens from the template
    # against the data (values) and then passing this text to draw_text_string()
    fontname = block['font'] if 'font' in block else meta['font']
    font_size = block['font_size'] if 'font_size' in block else meta['font_size']
    font = load_font(fontname, font_size)
    string = ''
    if 'text' in block:
        allow_missing = block['allow_missing'] if 'allow_missing' in block else None
        string = eval_text(values, block['text'], allow_missing)
        if string is None:
            bbox = bb.bbox_new(text_type, None)
            return None, bbox, False  # last value is simply marking the whole line as invalid preventing its output
        if len(string) > 0:
            d = ImageDraw.Draw(new_patch(meta, 10))
            w, h = d.textsize(string, font)
        else:
            bbox = bb.bbox_new(text_type, None)
            return None, bbox, True    # no text have been decoded - no patch created
    elif 'vspace' in block:
        w, h = 1, block['vspace'] * font_size
    else:
        w, h = 1, font_size
    line_space = math.ceil(h * meta['line_space'])
    patch = new_patch(meta, line_space, width=w)
    if len(string) > 0:
        patch, bboxes = draw_text_string(string, font, meta)
        # bboxes here is just a list of word bboxes. We will now add envelope box around that
        bboxes = bb.bbox_new(text_type, bboxes, (0, 0), patch.size)
    else:
        bboxes = bb.bbox_new(text_type, None, dim=(0, line_space))
    return patch, bboxes, True


def get_text_columns(line):
    # there are two possible scenarios here:
    # 1. The line is a string represented by a list of tokens that need to be joined
    # 2. The line is tabulated consisting of multiple tokens at defined tabulated positions
    columns = [line]
    if 'text' in line:
        if len(line['text']) <= 1:
            return columns
        if 'align' in line:
            if type(line['align']) is list:
                if len(line['align']) != len(line['text']):
                    sys.exit("Alignment vector in text block does not match text tokens " + ' '.join(line['text']))
                if 'hpos' not in line:
                    sys.exit("Horizontal position not found for text tokens " + ' '.join(line['text']))
                if len(line['hpos']) != len(line['text']):
                    sys.exit("Alignment vector in text block does not match text tokens " + ' '.join(line['text']))
                columns = [{"text": tx, "hpos": hp, "align": al}
                           for tx, hp, al in zip(line['text'], line['hpos'], line['align'])]
                if 'allow_missing' in line:
                    if type(line['allow_missing']) is list and len(line['allow_missing']) == len(line['text']):
                        columns = [dict(c, **{'allow_missing': a}) for c, a in zip(columns, line['allow_missing'])]
    return columns


def apply_defaults(items, defaults):
    for i, item in enumerate(items):
        for k, v in defaults.items():
            if k not in items[i]:
                if is_applicable(items[i], k, v):
                    items[i][k] = v
    return items


def is_applicable(line, key, value):
    if 'text' in line:
        if type(line['text']) != type(value):
            raise RuntimeError('Template error: Dimensions of line defaults and the line do not match')
        return True
    return False

def draw_text_line(values, meta, line, context):
    # this function renders a line of text from template taking care of multi column layout and
    # alignment. It takes care of line height and line spacing
    # It returns a patch image of width of the document. All these patches stacked together will form a document
    if 'columns' in line:
        columns = line['columns']
        text_type = 'cell'
        # row can be multiline so it will be added in outer function
        line_type = 'line'
        tags = [{'column': i} for i, c in enumerate(columns)]
    else:
        columns = get_text_columns(line)
        if len(columns) > 1:
            text_type = 'cell'
            line_type = ['line', 'row'] # bboxes will be enveloped into line which will then be enveloped into row
            tags = [{'column': i} for i, c in enumerate(columns)]
        else:
            text_type = 'string'
            line_type = 'line'
            tags = []
    patches, bboxes, valid = zip(*[draw_text_block(values, c, meta, text_type) for c in columns])
    if False in valid:  # the whole line was invalidated and will not be plotted
        return None, None
    bboxes = list(bboxes)
    line_height = max(0 if patch is None else patch.size[1] for patch in patches)
    line_img = new_patch(meta, line_height)
    pos = [get_text_block_pos(column, meta, patch) for column, patch in zip(columns, patches)]
    for i, c in enumerate(columns):
        if patches[i] is not None:
            line_img.paste(patches[i], pos[i])
            bboxes[i] = bb.bbox_move(bboxes[i], pos[i])
            if len(tags) > 0:
                bboxes[i] = bb.bbox_add_tags(bboxes[i], tags[i])
    bboxes = bb.bbox_make_envelope(bboxes, 'word', line_type)
    bboxes = bb.bbox_move(bboxes, (context['x'], context['y']))
    return line_img, bboxes


def get_text_block_pos(column, meta, patch):
    if patch is None:
        return None
    align = column['align'] if 'align' in column else 'L'
    res = meta['resolution']
    right = (meta['width'] - meta['right_margin']) * res
    left = meta['left_margin'] * res
    if 'hpos' in column:
        if align == 'R':
            right = min(left + column['hpos'] * res, right)
        else:
            left = left + column['hpos'] * res
    w, h = patch.size
    if align == 'C':
        hpos = left + (right - left - w)/2
    elif align == 'R':
        hpos = right - w
    else:
        hpos = left
    return math.floor(hpos), math.floor(0)


def draw_text(values, canvas, meta, block, context):
    bboxes = []
    y0 = canvas.size[0]
    if 'line_defaults' in block:
        block['lines'] = apply_defaults(block['lines'], block['line_defaults'])
    if 'lines' in block:
        for line in block['lines']:
            patch, bbox = draw_text_line(values, meta, line, context)
            if patch is not None:
                bboxes.append(bbox)
                canvas = vpaste(canvas, patch)
                context['y'] = canvas.size[1]
    # envelope bboxes into a text block
    if 'visual_type' in block:
        visual_type = block['visual_type']
    else:
        visual_type = 'text_block'
    bboxes = bb.bbox_make_envelope(bboxes, 'word', visual_type)
    return canvas, bboxes


def draw_table(values, canvas, meta, block, context):
    if 'lines' not in block:
        print('there are no lines defined in table ' + block['name'])
    lines = block['lines']          # can be multiple lines per item
    bboxes = []
    y0 = canvas.size[1]
    if 'header' in block:
        canvas, bbox = draw_table_row(canvas, meta, lines, block['header'], context)
        bboxes.append(bbox)
    dict_name = block['name'] if 'name' in block else 'items'
    if dict_name in values:
        for item in values[dict_name]:
            canvas, bbox = draw_table_row(canvas, meta, lines, item, context)
            bboxes.append(bbox)
    if 'footer' in block:
        canvas, bbox = draw_table_row(canvas, meta, lines, decode_special_row(block['footer'], values), context)
        bboxes.append(bbox)
    columns = bb.bboxes_from_tags(bboxes, tag='column', new_type='column', entity_type='cell')
    bboxes.append(columns)
    bboxes = bb.bbox_make_envelope(bboxes, 'word', 'table')
    return canvas, bboxes


def decode_special_row(value_template, values):
    for k, v in value_template.items():
        value_template[k] = eval_token(values, value_template[k], allow_missing=True)
    return value_template


def draw_barcode(values, canvas, meta, block, context):
    bboxes = []
    voffset = canvas.size[1]
    h, w = (0, 0)
    if 'lines' in block:
        for line in block['lines']:
            patch, bbox = draw_bar_code(values, meta, line)
            bboxes.append(bb.bbox_adjust(bbox, 'y', h))
            h += patch.size[1]
            if w < patch.size[0]:
                w = patch.size[0]
            canvas = vpaste(canvas, patch)
    if len(bboxes) > 0:
        # envelope into barcode block bbox
        bboxes = bb.bbox_new('barcode_block', bboxes, (0, voffset), (w, h))
    return canvas, bboxes


def draw_bar_code(values, meta, line):
    fontname = line['font'] if 'font' in line else meta['font']
    font_size = line['font_size'] if 'font_size' in line else meta['font_size']
    font = load_font(fontname, font_size)
    code = eval_token(values, line['code'])
    barcode_image = code128.draw_code128(code, font)
    w, h = barcode_image.size
    patch = new_patch(meta, h)
    align = line['align'] if 'align' in line else 'C'
    x, y = get_text_block_pos({'align': align}, meta, barcode_image)
    patch.paste(barcode_image, (x, 0))
    bbox = bb.bbox_new('barcode', str(code), (x, 0), (w, h))
    return patch, bbox


def draw_table_row(canvas, meta, lines, values, context):
    bboxes = []
    for line in lines:
        patch, bbox = draw_text_line(values, meta, line, context)
        bboxes.append(bbox)
        canvas = vpaste(canvas, patch)
        context['y'] = canvas.size[1]
    bboxes = bb.bbox_make_envelope(bboxes, 'word', 'row')
    return canvas, bboxes


def new_context(corner=(0, 0)):
    return {'x': corner[0], 'y': corner[1]}


def get_offset(context):
    return context['x'], context['y']


def update_context(context, corner):
    context['y'] = corner[1]
    return context


def draw_bboxes(canvas, bboxes, entity_type, color):
    boxes_list = bb.bbox_select_entities(bboxes, entity_type)
    d = ImageDraw.Draw(canvas)
    for box in boxes_list:
        if 'corners' in box:
            d.polygon(box['corners'], outline=color)
        else:
            d.rectangle(bb.bbox_rectangle(box), outline=color)
    return canvas



