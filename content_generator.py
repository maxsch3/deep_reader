import random
import string
import math
import numpy as np
from datetime import date, time, datetime, timedelta
import pandas as pd


class ConGen:
    ph_prefix_choice = ['0844', '0800', '0845', '020']
    st_suffix_choice = ['Street', 'St.']
    pay_means = [{"pay_mean": "Cash", "pay_type": "Cash", "p": 0.1},
                 {"pay_mean": "AMEX", "pay_type": "Card", "card_num": "XXXX3534", "card_expiry": "08/19", "p": 0.6},
                 {"pay_mean": "Maestro", "pay_type": "Card", "card_num": "XXXX7356", "card_expiry": "03/20", "p": 0.3}]
    tax_bands = [{"name": "20 % VAT", "rate": 0.2, "p": 0.95},
                 {"name": "5 % VAT", "rate": 0.05, "p": 0.01},
                 {"name": "0 % VAT", "rate": 0, "p": 0.04}]
    price_factor = 5
    discount_prob = 0.1
    disc_header_choice = ['Voucher', 'Promotion', 'Brand Guarantee', 'Discount']

    def __init__(self, path=''):
        self.nouns = self.__load_file(path+'/nouns.txt')
        self.adjvs = self.__load_file(path+'/adjectives.txt')
        self.names = self.__load_file(path+'/names.txt')
        self.surnm = self.__load_file(path+'/surnames.txt')

    def __load_file(self, file):
        f = open(file, mode='r')
        return [line.strip() for line in f]

    def random_name(self, n=2):
        st = ' '.join([random.choice(self.adjvs)]*(n-1))
        return st + ' ' + random.choice(self.nouns)

    def random_address(self):
        town = random.choice(self.nouns).title()
        st_suffix = self.random_st_suffix()
        street = random.choice(self.adjvs).title() + ' ' + st_suffix
        postcode = self.random_postcode()
        number = random.randint(1, 20)
        return number, street, town, postcode

    def random_postcode(self):
        inner_letters = 1 if random.randint(0, 9) > 8 else 2
        inner = ''.join(random.sample(string.ascii_uppercase, inner_letters))+str(random.randint(1, 50))
        outer = str(random.randint(0, 9)) + ''.join(random.sample(string.ascii_uppercase, 2))
        return inner + ' ' + outer

    def random_st_suffix(self):
        return random.choice(self.st_suffix_choice)

    def random_phone(self):
        prefix = random.choice(self.ph_prefix_choice)
        rest = ''.join([str(random.randint(0, 9)) for _ in range(0,10-len(prefix))])
        return prefix + rest

    def random_price(self):
        return round(random.lognormvariate(0, 1) * self.price_factor, 2)

    def random_sku(self):
        return random.randint(0, 99999)

    def random_tax(self):
        return np.random.choice(self.tax_bands, p=[t['p'] for t in self.tax_bands])

    def random_staff_name(self):
        return random.choice(self.names)

    def random_item(self, flags):
        qty = int(round(random.lognormvariate(0, 0.25)))
        if qty == 0:
            qty = 1
        sku = self.random_sku()
        name1 = self.random_name()
        name2 = self.random_name()
        price = self.random_price()
        flag = random.choice(flags)
        tax = self.random_tax()
        return {'SKU': sku, 'name1': name1, 'name2': name2, 'price': price, 'qty': qty,
                'tax_name': tax['name'], 'tax_rate': tax['rate']}

    def random_item_list(self):
        ni = int(math.ceil(random.lognormvariate(0, 0.5) * 5))
        item_flag1 = ['']*5 + [random.choice(['*', 'Z', 'F', '*'])]
        items = [self.random_item(item_flag1) for _ in range(0, ni)]
        df_items = pd.DataFrame(items)
        df_items['net_price'] = (df_items['price'] / (1 + df_items['tax_rate'])).round(2)
        df_items['tax_value'] = df_items['price'] - df_items['net_price']
        taxes = df_items.groupby('tax_name')['tax_value'].sum().sort_values(ascending=False)
        taxes = list(zip(taxes.index, taxes))
        #taxes = list(taxes.items())
        discounts = self.random_discount_list(items)
        total_item = sum([item['price']*item['qty'] for item in items])
        total_disc = sum([disc['value']*disc['qty'] for disc in discounts])
        net_value = round(df_items['net_price'].sum(), 2)
        to_pay = total_item + total_disc
        item_list = {'items': items, 'savings': discounts,
                     'total_item': round(total_item, 2),
                     'total_disc': round(total_disc, 2),
                     'total': round(to_pay, 2),
                     'total_net': net_value}
        if len(taxes) > 0:
            item_list.update({'tax_band1': taxes[0][0],
                              'tax_value1': round(taxes[0][1], 2)})
#                              'tax_value1': round(np.asscalar(taxes[0][1]), 2)})
        return item_list

    def random_discount_list(self, items):
        item_disc = [self.random_discount(item) for item in items]
        item_disc = [disc for disc in item_disc if disc is not None]
        return item_disc

    def random_discount(self, item=None, total=0):
        disc_yn = True if random.random() < self.discount_prob else False
        disc = None
        if disc_yn:
            if item is None:
                if total > 0:
                    text = random.choice(self.disc_header_choice)
                    value = self.disc_value(total)
                    disc = {'saving': text, 'value': value, 'qty': 1}
            else:
                text = str(item['SKU']) + ' saving'
                value = self.disc_value(item['price'])
                disc = {'saving': text, 'value': value, 'qty': 1}
        return disc

    def disc_value(self, total):
        per = math.pow(random.random(), 2)
        value = round(total * per + 0.01, 2) * -1
        return value

    def random_pos(self):
        name = self.random_name().title()
        address = self.random_address()
        phone = self.random_phone()
        return {'POS_name': name, 'POS_address': str(address[0])+' '+address[1],
                'POS_city': address[2], 'POS_postcode': address[3], 'POS_phone': phone}

    def random_document(self):
        doc = self.random_pos()
        doc.update(self.random_item_list())
        doc.update(self.random_payment())
        doc.update(self.random_meta())
        return doc

    def random_payment(self):
        pay_mean = self.random_pay_mean()
        payment = {"term_id": '{0:08d}'.format(random.randint(0, 10**7-1)),
                   "pay_id": '{0:06d}'.format(random.randint(0, 10**6-1)),
                   "pay_batch": '{0:03d}'.format(random.randint(0, 10**3-1))}
        if pay_mean['pay_type'].lower() == "cash":
            payment.update(pay_mean)
        elif pay_mean['pay_type'].lower() == "card":
            payment.update(self.random_card_payment(pay_mean))
        return payment

    def random_docid(self):
        return random.randint(10**10, 10**15)

    def random_transid(self):
        return random.randint(10**4, 10**6)

    def random_datetime(self):
        days_back = math.ceil(random.lognormvariate(0, .5) * 5)
        seconds = random.randint(8*3600, 20*3600)
        dt = datetime.combine(date.today() + timedelta(days=-days_back), time.min) + timedelta(seconds=seconds)
        date_format = '%d/%m/%y %H:%M'
        return dt.strftime(date_format)

    def random_meta(self):
        meta = {'doc_id': self.random_docid(),
                'trans_id': self.random_transid(),
                'date_time': self.random_datetime(),
                'staff_name': self.random_staff_name(),
                'staff_surname': random.choice(self.surnm),
                'pos_num': '{0:02d}'.format(math.ceil(random.lognormvariate(0, 0.5)*10)),
                'pos_shift': '{0:03d}'.format(math.ceil(random.lognormvariate(0, 0.5)*10)),
                'pos_rec_num': '{0:05d}'.format(random.randint(0, 9999)),
                'pos_cnum': '{0:04d}'.format(random.randint(0, 9999))}
        return meta

    def random_card_payment(self, mean):
        assert mean['pay_type'].lower() == "card"
        payment = mean
        payment['pay_mode'] = np.random.choice(["Contactless", "PIN"], p=[.7, .3])
        payment['pay_seq'] = '03'
        payment['pay_auth_id'] = 'A' + '{0:015d}'.format(random.randint(0, 10**8))
        payment['pay_crypt'] = ''.join("0123456789ABCDEF"[random.randint(0, 15)] for _ in range(0, 16))
        payment['pay_auth_code'] = ''.join("0123456789ABCDEF"[random.randint(0, 15)] for _ in range(0, 6))
        payment['pay_response'] = 'Approved'
        payment['pay_verification'] = "No verification"
        return payment


    def random_pay_mean(self):
        p = [mean['p'] for mean in self.pay_means]
        rv = np.random.choice(self.pay_means, p=p)
        return rv
