import os


def file_accessible(file_path, mode):
    # Check if a file exists and is accessible.
    try:
        f = open(file_path, mode)
        f.close()
    except IOError as e:
        return False
    return True


def check_file(file):
    if not os.path.isfile(file):
        print("Error: file %s not found", file)
        return False
    if not file_accessible(file, "r"):
        print("Error: file %s is not accessible or not exists", file)
        return False
    return True


def is_sequence(obj):
    return type(obj) in [list, tuple, range]
