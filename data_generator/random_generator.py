from data_generator.entity import KwargsObject
import pandas as pd
import numpy as np


class RandomGenerator(KwargsObject):

    n = 1
    distribution = 'uniform'
    low = 0
    high = 1
    mean = None
    scale = None
    choice_from = None
    replace = True
    p = None
    trials = None

    def __init__(self, kwargs):
        super().__init__(kwargs)
        if 'seed' in kwargs:
            np.random.seed(kwargs['seed'])
        if 'file' in kwargs and 'choice_from' not in kwargs:
            self.choice_from, self.p = self.read_from_file(kwargs['file'])
        self.function = self.validate_and_make()
        self.values = None
        self.index = None

    def __iter__(self):
        self.values = self.function(self.n)
        self.index = 0
        return self

    def __next__(self):
        try:
            result = self.values[self.index]
        except IndexError:
            raise StopIteration
        self.index += 1
        return result

    def __len__(self):
        return self.n

    def sample(self, n=1):
        return self.function(n)

    @staticmethod
    def read_from_file(filename):
        # check the file
        cols = ['choice', 'p']
        df = pd.read_csv(filename, header=None, names=cols, usecols=[0, 1])
        # if second column was missing from file, it will be filled with NaNs
        df.p = df.p.fillna(1.0)
        if type(df.iloc[0, 1]) not in [int, float, np.float16, np.float32, np.float64]:
            df = df[1:].reset_index(drop=True)
            try:
                df.p = df.p.astype(np.float32)
            except ValueError:
                raise ValueError('Error: Second column (weights) has non-numeric values.')
        return df.choice, df.p

    def validate_and_make(self):
        distributions = {'uniform': self._make_uniform, 'normal': self._make_normal,
                         'choice': self._make_choice, 'lognormal': self._make_lognormal,
                         'randint': self._make_randint, 'binomial': self._make_binomial}
        if self.distribution not in distributions:
            raise ValueError('Distribution type \'%s\' is not supported' % self.distribution)
        if self.n < 1 or type(self.n) is not int:
            raise ValueError('Parameter size (n) must be integer >=1')
        return distributions[self.distribution]()

    def _make_uniform(self):
        if self.distribution != 'uniform':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % self.distribution)
        if self.low is None:
            raise ValueError('Parameter \'low\' is required for uniform distribution')
        if self.high is None:
            raise ValueError('Parameter \'high\' is required for uniform distribution')

        def uniform_generator(n):
            return np.random.uniform(self.low, self.high, n)
        return uniform_generator

    def _make_normal(self):
        if self.distribution != 'normal':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % self.distribution)
        if self.mean is None:
            raise ValueError('Parameter \'mean\' is required for normal distribution')
        if self.scale is None:
            raise ValueError('Parameter \'scale\' is required for normal distribution')

        def local_generator(n):
            return np.random.normal(self.mean, self.scale, n)
        return local_generator

    def _make_lognormal(self):
        if self.distribution != 'lognormal':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % self.distribution)
        if self.mean is None:
            raise ValueError('Parameter \'mean\' is required for lognormal distribution')
        if self.scale is None:
            raise ValueError('Parameter \'scale\' is required for lognormal distribution')

        def local_generator(n):
            return np.random.lognormal(self.mean, self.scale, n)
        return local_generator

    def _make_choice(self):
        if self.distribution != 'choice':
            raise ValueError('Wrong function was called for %s sampling. Did you call it from \'validate_and_make\'?' %
                             self.distribution)
        if self.choice_from is None:
            raise ValueError('Parameter \'choice_from\' is required for choice sampling')
        if not any([isinstance(self.choice_from, pd.DataFrame), isinstance(self.choice_from, pd.Series),
                   isinstance(self.choice_from, list)]):
            raise ValueError('Parameter \'choice_from\' must be a list, a pandas dataframe or a pandas series object')
        if self.replace is None:
            raise ValueError('Parameter \'replace\' is required for choice sampling')
        if self.p is not None:
            if len(self.p) != len(self.choice_from):
                raise ValueError('Length of weights (probabilities) must match length of choice list')
        if any([isinstance(self.choice_from, pd.DataFrame), isinstance(self.choice_from, pd.Series)]):
            def local_generator(n):
                return self.choice_from.sample(n=n, replace=self.replace, weights=self.p).values
        else:
            def local_generator(n):
                return np.random.choice(self.choice_from, n, replace=self.replace, p=self.p)
        return local_generator

    def _make_randint(self):
        if self.distribution != 'randint':
            raise ValueError('Wrong function was called for %s distribution. Did you call it from'
                             ' \'validate_and_make\'?' % self.distribution)
        if self.low is None:
            raise ValueError('Parameter \'low\' is required for randint distribution')
        if self.high is None:
            raise ValueError('Parameter \'high\' is required for randint distribution')
        if self.high - self.low <= 1:
            raise ValueError('High parameter must be greater than low parameter by at least 2')

        def local_generator(n):
            return np.random.randint(low=self.low, high=self.high, size=n)
        return local_generator

    def _make_binomial(self):
        if self.distribution != 'binomial':
            raise ValueError('Wrong function was called for %s distribution. Did you call it from'
                             ' \'validate_and_make\'?' % self.distribution)
        if self.trials is None:
            self.trials = 1
        if self.p is None:
            raise ValueError('Parameter \'p\' is required for binomial distribution')

        def local_generator(n):
            return np.random.binomial(n=self.trials, p=self.p, size=n)
        return local_generator
