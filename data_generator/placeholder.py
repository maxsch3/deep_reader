from data_generator import collection_entity as coll_entity, entity
from data_generator import text_entity, image_entity, barcode_entity
from data_generator import geom_transform as gt
# from data_generator import affine_transform
from PIL import Image


class Placeholder(coll_entity.CollectionEntity):
    """This object is an object's 'packaging' of a set size which will align its object
    within this size according to align specification (left, center, right, top, middle, bottom)
    The object will have no own context

    It is useful, when primitives not supporting padding and alignment such as simple image,
    are used inside containers with some alignment, e.g. in tables with right alignment in cells

    """

    # this is a mutual import scenario. it is needed to allow placeholder to keep other tables

    _primitives = None

    height = 0      # 0 means float size calculated from object dimensions
    width = 0
    align = 'lt'
    padding = 0

    _block_name = 'placeholder'

    def __init__(self, block):
        if self._primitives is None:
            self.register_entity()
        ph_kwargs = {'type': self._block_name, self._block_name: ''}
        if 'padding' in block:
            ph_kwargs.update({'padding': block.pop('padding')})
        if 'align' in block:
            ph_kwargs.update({'align': block.pop('align')})
        super().__init__(ph_kwargs)
        if 'width_max' in block:
            self.width = block['width_max']
        if 'height_max' in block:
            self.height = block['height_max']
        block = self.pushdown_parameters(block)
        self.obj = self._primitives[block['type']](block)
        assert isinstance(self.obj, entity.Entity)
        self.offset = 0, 0
        self.refresh()
        # below statement is used to break circular import Table > Collection > Placeholder > table
        from data_generator.table import Table
        self._primitives['table'] = Table

    @classmethod
    def register_entity(cls, entity_class=None):
        if cls._primitives is None:
            from data_generator.table import Table
            from data_generator.text_entity import TextEntity
            from data_generator.barcode_entity import BarcodeEntity
            from data_generator.image_entity import ImageEntity
            cls._primitives = {'text': TextEntity,
                               'image': ImageEntity,
                               'barcode': BarcodeEntity,
                               'table': Table}
        if entity_class is not None:
            if not issubclass(entity_class, entity.Entity):
                raise ValueError('Newly registered entity must be a subclass of Entity')
            cls._primitives.update({entity_class.entity_type(): entity_class})

    def pushdown_parameters(self, block):
        parameter_names = self._primitives[block['type']].accepts_pushed_down_parameters()
        if len(parameter_names) == 0:
            return block
        parameters = {key: getattr(self, key) for key in parameter_names if hasattr(self, key)}
        if 'width' in parameters and self.width > 0:
            if parameters['width'] > self.width - self.padding[1] - self.padding[3]:
                parameters['width'] = self.width - self.padding[1] - self.padding[3]
#            parameters['width'] -= self.padding[1] + self.padding[3]
        if 'height' in parameters and self.height > 0:
            if parameters['height'] > self.height - self.padding[0] - self.padding[2]:
                parameters['height'] = self.height - self.padding[0] - self.padding[2]
        # return combined block with added pushdown parameters so that explicit values set for an object
        # take preference in case of conflicts
        return {**parameters, **block}

    def get_size(self):
        width, height = self.get_obj_size()
        if self.width > 0:
            width = self.width
        if self.height > 0:
            height = self.height
        return width, height

    def get_obj_size(self, with_padding=True):
        size = self.obj.get_size()
        if not with_padding:
            return size
        return size[0] + self.padding[1] + self.padding[3], size[1] + self.padding[0] + self.padding[2]

    def set_height(self, height):
        self.height = height
        self.refresh()

    def set_width(self, width):
        self.width = width
        self.refresh()

    def set_size(self, size):
        self.width, self.height = size
        self.refresh()

    def set_padding(self, padding):
        self.padding = self.ingest_padding(padding)
        self.refresh()

    def set_parameters(self, kwargs):
        """This is a single function for setting all the parameters all together"""
        refresh_needed = False
        if 'size' in kwargs:
            self.width, self.height = self._ingest_size(kwargs['size'])
            refresh_needed = True
        else:
            if 'width' in kwargs:
                self.width = kwargs['width']
                refresh_needed = True
            if 'height' in kwargs:
                self.height = kwargs['height']
                refresh_needed = True
        if 'align' in kwargs:
            self.halign, self.valign = self.ingest_alignment(kwargs['align'])
            refresh_needed = True
        if 'padding' in kwargs:
            self.padding = self.ingest_padding(kwargs['padding'])
            refresh_needed = True
        if refresh_needed:
            self.refresh()

    def refresh(self):
        self.offset = self.calculate_offset()
        self._img_ = self.__render()

    def calculate_offset(self):
        x0, y0 = self.padding[3], self.padding[0]
        if self.width == 0 and self.height == 0:
            return x0, y0
        if self.halign == 'l' and self.valign == 't':
            return x0, y0
        width, height = self.get_size()
        width = width if self.width == 0 else self.width
        height = height if self.height == 0 else self.height
        obj_size = self.get_obj_size()
        if self.halign == 'r':
            x0 += width - obj_size[0]
        elif self.halign == 'c':
            x0 += int((width - obj_size[0])/2)
        if self.valign == 'b':
            y0 += height - obj_size[1]
        elif self.valign == 'm':
            y0 += int((height - obj_size[1])/2)
        return x0, y0

    @staticmethod
    def _ingest_size(size):
        if type(size) is not tuple:
            raise ValueError('Size value %s is not supported. Only tuple with two integers is supported'
                             % size)
        if len(size) != 2:
            raise ValueError('Size value %s is not supported. Only tuple with two integers is supported'
                             % size)
        return size

    def bbox(self, affine_ext=None):
        if self.offset != (0, 0):
            affine_ext = gt.GeomTransform({'offset': self.offset}) + affine_ext
        return self.obj.bbox(affine_ext)

    def __render(self):
        """This method is actually generating an image representing an entity"""
        image = Image.new(self.color_mode, self.get_size(), self.background_color)
        image.paste(self.obj.get_img(), self.offset)
        return image

    def apply_transformations(self, transformations):
        self.obj.apply_transformations(transformations)
        self.refresh()

    def reset_transformation(self):
        self.obj.reset_transformation()
        self.refresh()
