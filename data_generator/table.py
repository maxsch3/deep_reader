from data_generator import collection as co
import numpy as np


class Table(co.Collection):

    _block_name = "table"
    _default_parameters = ['padding', 'align']
    # default values of attributes that can be changed through class method Table.set_defaults(kwargs)
    # padding will be applied to all cells
    padding = [0, 0, 0, 0]
    column_padding = 0
    widths = None
    heights = None
    col_align = 'lt'  # left-top by default
    row_align = None  # Not used
    columns = 0
    rows = 0

    def __init__(self, block_definition):
        super().__init__(block_definition)
        pass

    def ingest_settings(self):
        super().ingest_settings()
        self.columns = self._infer_columns()
        self.column_padding = self._ingest_column_padding(self.column_padding)

    def setup_layout(self):
        # allocate objects adds row and column number to each element of layout
        self._allocate_objects()
        self.rows = self.obj_layout[-1]['row'] + 1
        # assign padding and align to each of placeholders
        padding = [self._cell_padding(l['row'], l['col']) for l in self.obj_layout]
        align = [self._cell_align(l['row'], l['col']) for l in self.obj_layout]
        self.obj_layout = [{**l, 'padding': p, 'align': a} for l, p, a in zip(self.obj_layout, padding, align)]
        # At this point, objects are not created yet, so their dimensions are unknown. Here we will only push down
        # width to contained objects if the dimension of a table is not floating and set explicitly.
        # E.g. widths of columns is set at a table header level.
        # If a dimension is floating, than it will not be pushed down to any of the objects
        if self.widths is not None:
            for l in self.obj_layout:
                l['width'] = self.widths[l['col']]
        if self.heights is not None:
            for l in self.obj_layout:
                l['height'] = self.heights[l['row']]

    def calculate_layout(self):
        """This is a core method of collection object that defines features of each collection's subclass.
         Its only role is to make a meta object that describe how objects of collection will be layed out.

         This function is a template function defined in a parent class and it is already plugged in into collection
         workflow, so ideally, only this method should be re-defined in any of collection's sub object

         This method returns a list of dictionaries where each element correspond to an object and describes location
         of this object. All objects in collection classes are placed in placeholders, that are just a transparent
         boxes for unification of external object size. Placeholders support external size; alignment and padding
         of its object inside this set size

         Each element of layout has the following fields:
         - offset: (x, y) - mandatory - external top left corner of a padded object
         - size: (w, h) - optional - sets external size of a placeholder, if missing, placeholder will have size
                                     of an object it holds with padding
         - padding: [top, right, bottom, left] - optional - sets padding of a object inside placeholder. If missing,
                                    padding will not be applied
         - align: 'br' - optional - sets alignment of padded object inside placeholder.
                                    If missing, left top will be used

        """
        sizes = [x.get_size() for x in self.objects]
        if self.widths is None:
            widths = [max([sizes[x][0] for x, _ in enumerate(self.obj_layout)
                           if self.obj_layout[x]['col'] == c]) for c in range(self.columns)]
            for o, l in zip(self.objects, self.obj_layout):
                l['width'] = widths[l['col']]
                o.set_parameters({'width': l['width']})
        else:
            widths = self.widths
        if self.heights is None:
            heights = [max([sizes[x][1] for x, _ in enumerate(self.obj_layout)
                            if self.obj_layout[x]['row'] == r]) for r in range(self.rows)]
            for o, l in zip(self.objects, self.obj_layout):
                l['height'] = heights[l['row']]
                o.set_parameters({'height': l['height']})
        else:
            heights = self.heights
        # now, objects are all set. The last bit is setting offsets for each cell
        self.size = sum(widths), sum(heights)
        x_col = list(np.cumsum([0] + widths[:-1]))
        y_row = list(np.cumsum([0] + heights[:-1]))
        # update grid object with x0, y0 for each cell
        for l in self.obj_layout:
            l['offset'] = x_col[l['col']], y_row[l['row']]

    def _cell_align(self, row, col):
        return self.col_align[col]

    def _ingest_column_padding(self, column_padding):
        if column_padding == 0:
            return 0
        assert type(column_padding) == list
        assert len(column_padding) == self.columns
        col_padding = [self._ingest_padding(p) for p in column_padding]
        return col_padding

    def _cell_padding(self, row, col):
        if self.column_padding == 0:
            return self.padding
        return self.column_padding[col]

    def _calc_cells_wh(self, layout):
        # calculate widths of columns as max of widths of its elements
        sizes = [x.get_size() for x in self.objects]
        if self.widths is None:
            widths = [max([sizes[x][0] for x, _ in enumerate(layout)
                           if layout[x]['col'] == c]) for c in range(self.columns)]
        else:
            widths = self.widths
        heights = [max([sizes[x][1] for x, _ in enumerate(layout)
                        if layout[x]['row'] == r]) for r in range(self.rows)]
        return widths, heights

    def _allocate_objects(self):
        # allocate each object to a cell. self.content contains all definitions of objects,
        # including None objects. self.objects holds all non-None objects created
        io, r, c = 0, 0, 0
        for i, o in enumerate(self.content):
            if o is not None:
                self.obj_layout[io]['row'] = r
                self.obj_layout[io]['col'] = c
                io += 1
            # next cell's row/column:
            if c + 1 >= self.columns:
                r += 1
                c = 0
            else:
                c += 1

    def _infer_columns(self):
        """This method will infer number columns from input parameters if it hasn't been provided
         explicitly and will then check if all column specific parameters are either
         1. single setting that is valid for all columns. It will be replicated for each column
         2. a list of settings. It will check then if its length equals number of columns"""
        attributes_to_check = [self.column_padding, self.col_align, self.widths]
        attribute_strings = ['column_padding ', 'col_align', 'widths']
        is_iterable = [type(o) in [list, tuple] for o in attributes_to_check]
        if self.columns == 0:
            if not any(is_iterable):
                raise ValueError('When creating table, neither list of column alignments nor number of columns'
                                 'were provided')
            inferred_cols = [len(o) for o, i in zip(attributes_to_check, is_iterable) if i]
            columns = inferred_cols[0]
        else:
            columns = self.columns
        for att, att_name in zip(attributes_to_check, attribute_strings):
            if type(att) == list:
                if len(att) != columns:
                    raise ValueError('Number of columns and length of %s do not match when creating table object'
                                     % att_name)
        return columns

    def bbox(self, affine_ext=None):
        """This method will enhance collection's bbox with rows and columns"""
        bbox = super().bbox(affine_ext)
        columns = [[bbox['content'][x]['x'] for x, _ in enumerate(self.obj_layout)
                    if self.obj_layout[x]['col'] == c] for c in range(self.columns)]
        columns = [self.outside_box(np.array(sum(c, []))) for c in columns]
        bbox['columns'] = columns
        rows = [[bbox['content'][x]['x'] for x, _ in enumerate(self.obj_layout)
                if self.obj_layout[x]['row'] == r] for r in range(self.rows)]
        rows = [self.outside_box(np.array(sum(r, []))) for r in rows]
        bbox['rows'] = rows
        return bbox

    @staticmethod
    def outside_box(box):
        mn = np.min(box, axis=0)
        mx = np.max(box, axis=0)
        return [(mn[0], mn[1]), (mn[0], mx[1]), (mx[0], mx[1]), (mx[0], mn[1])]
