from data_generator.abstract_value import RandomValue, AbstractValue
import pandas as pd
import numpy as np


class RandomTable:
    def __init__(self, kwargs):
        self.index = 0
        self.round_dec = 2
        kwargs = self._instantiate_parameters(kwargs)
        kwargs['columns'] = self._instantiate_columns(kwargs)
        self.function = self._make_function(kwargs)
        self.data = self.refresh()
        # self.data['amount'] = self.data.price * self.data.quantity
        # self.data['tax'] = self.data.price * self.data.tax_rate
        # self.data['tax_amount'] = self.data.tax * self.data.quantity

    def refresh(self):
        return self.function()

    def _instantiate_columns(self, kwargs):
        if 'columns' not in kwargs:
            raise ValueError('Error: parameter \'columns\' is required')
        if type(kwargs['columns']) != dict:
            raise ValueError('Error: parameter \'columns\' must be a dict')
        columns = {key: value if isinstance(value, AbstractValue) else RandomValue(value)
                   for key, value in kwargs['columns'].items()}
        return columns

    def _instantiate_parameters(self, kwargs):
        exclude_list = ['columns']
        params = {key: kwargs[key] if (isinstance(kwargs[key], RandomValue) or key in exclude_list) else
                       RandomValue(kwargs[key])
                  for key, value in kwargs.items()}
        return params

    @staticmethod
    def _make_function(kwargs):

        def local_func():
            nrow = kwargs['nrow']()
            if nrow < 1:
                nrow = 1
            column_data = {key: value(nrow) for key, value in kwargs['columns'].items()}
            column_data = {key: value if type(value) == list else [value] for key, value in column_data.items()}
            return pd.DataFrame.from_dict(column_data)
        return local_func

    def __call__(self):
        self.data = self.refresh()
        return self

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        try:
            result = self.data.iloc[self.index].to_dict()
        except IndexError:
            raise StopIteration
        self.index += 1
        # result['price'] = self.round(result['price'])
        return result

    def sum(self, field):
        return self.round(sum(self.data.loc[:, field]))

    def round(self, val):
        return round(val, self.round_dec)


class ItemTable(RandomTable):
    def refresh(self):
        data = super().refresh()
        # add/modify few columns
        col_names = data.columns
        assert 'name' in col_names
        assert 'qty' in col_names
        assert 'price' in col_names
        # round qty up and make it int
        data['qty'] = np.ceil(data['qty']).astype(int)
        data['price'] = data['price'].round(decimals=2)
        data['amount'] = data['price'] * data['qty']
        return data
