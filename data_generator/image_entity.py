from data_generator import entity as ent
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import os


class ImageEntity(ent.Entity):

    _block_name = 'image'
    _pushed_down_parameters = ['width']  # container classes will push down (enforce) these parameters

    width = 0
    height = 0

    def __init__(self, kwargs):
        super().__init__(kwargs)
        assert self._block_name in kwargs
        self.content = kwargs[self._block_name]
        assert type(self.content) == str
        self.width = int(self.width)
        self.height = int(self.height)
        image = Image.open(self.image_path(kwargs[self._block_name]))
        self.img = self.__render(image)

    def image_path(self, img_file):
        return os.path.join(self.img_root, img_file)

    def __render(self, image):
        if image.size[0] == 0:
            raise ValueError('Cannot resize zero sized image in ImageEntity')
        if self.width > 0 or self.height > 0:
            width, height = self.width, self.height
            if self.width > 0 and self.height == 0:
                height = int(image.size[1] * self.width / image.size[0])
            if self.height > 0 and self.width == 0:
                width = int(image.size[0] * self.height / image.size[1])
            image = image.resize((width, height), Image.ANTIALIAS)
        return image
