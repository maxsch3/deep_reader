from data_generator import entity as ent
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import os
import re
import numpy as np


class TextEntity(ent.Entity):

    _block_name = 'text'
    _pushed_down_parameters = ['width', 'align']
    # below attributes with their default values. They will be set from values provided in
    # kwargs dictionary where key matches attribute name.
    font = 'Set me through set_defaults class method beforehand'
    font_size = 'Set me through set_defaults class method beforehand'
    font_color = 'Set me through set_defaults class method beforehand'
    width = 0
    align = 'l'
    spacing = 4

    def __init__(self, kwargs):
        super().__init__(kwargs)
        self.char_bbox = None
        self.align = self.ingest_align(self.align)
        self.content = kwargs[self._block_name]
        self.img = self.__render(kwargs[self._block_name])

    def __render(self, text):
        """This function creates an image using text as an argument and font settings that
         are already available as self. attributes. These settings are normally default class attributes,
         that may be overwritten in any instance of the class"""
        font = self.load_font()
        text, size = self.break_lines(font, text)
        self.char_bbox = self.character_boxes_multiline(text, font)
        image = Image.new(self.color_mode, size, self.background_color)
        draw = ImageDraw.Draw(image)
        draw.multiline_text((0, 0), text, self.font_color, font, align=self.align, spacing=self.spacing)
        return image

    def break_lines(self, font, text):
        """This method adds breakline characters into the text where needed to fit the text into
        a block of width set by self.width"""
        if self.width == 0:
            return text, self.text_size(text, font)
        # if width is set, first element of size returned must be this width value.
        # this will make sure that text alignment is correct later on when whole block will be
        # inserted into a table for example
        input_lines = text.splitlines()
        lines = [""]
        for l in input_lines:
            if len(lines[-1]) > 0:
                lines.append("")
            for pos, word in enumerate(l.split(' ')):
                if font.getsize(lines[-1] + ' ' + word)[0] < self.width or len(lines[-1]) == 0:
                    lines[-1] += word if pos == 0 else " " + word
                else:
                    lines.append(word)
        text = "\n".join(lines)
        size = self.text_size(text, font)
        return text, size

    def text_size(self, text, font):
        """There is a feature in PIL that calculates text height of single liners different for lines
        with and without descenders. E.g. text height of 'ccc' and 'ccg' will be 12 and 15 when
        font size 12 is used. This causes misalignment issues in tables.
        To mitigate this issue, for one liners, I will calculate size's width and height separately:
        size = width(text), height('g')"""
        lines = text.splitlines()
        accent, descent = font.getmetrics()
        if type(lines) == list and len(lines) > 1:
            # below getsize_multiline doesnt work because of bad coding in it:
            # it calculates height = getsize('A')[1] * lines + spacing * (lines - 1)
            # therefore it does not leave space for possible descenders in the last line
            return font.getsize_multiline(text, spacing=self.spacing)[0],\
                   (accent + self.spacing) * len(lines) - self.spacing + descent
        else:
            return font.getsize(text)[0], accent + descent

    def load_font(self):
        font_file = os.path.join('fonts', self.font)
        font = ImageFont.truetype(font_file, self.font_size)
        return font

    def ingest_align(self, align):
        if align not in ['left', 'center', 'right']:
            h = re.search('[lcr]', align)
            align = h.group(0) if h is not None else "left"
            if align == 'l':
                align = 'left'
            elif align == 'c':
                align = 'center'
            elif align == 'r':
                align = 'right'
        return align

    def character_boxes_multiline(self, text, font):
        lines = text.splitlines()
        line_boxes = [self.character_boxes_line(l, font) for l in lines]
        return line_boxes

    def character_boxes_line(self, line, font):
        # using cumulative line growth to infer last character width, rather than just a single character
        # width because of possible ligatures that change char width
        bboxes = [self.single_character_bbox(line[:i+1], font) for i, c in enumerate(line)]
        # These boxes have no offset so the characters are on the top of each other
        # I will now add cumulative offset
        pos = [0] + [b['x'][3][0] for b in bboxes][:-1]
        bboxes = [self.set_char_bbox_offset(b, x=x) for b, x in zip(bboxes, pos)]
        # bboxes now contain all characters including whitespaces. It can now be split into words
        ws_pos = [ix for ix, box in enumerate(bboxes) if box['content'].isspace()] + [len(bboxes)]
        start_end = list(zip([-1] + ws_pos[:-1], ws_pos))
        # remove all pairs with distance lower than 2 (consecutive spaces will have 1)
        word_boxes = [bboxes[(s+1): e] for s, e in start_end if e-s > 1]
        # now, each of word boxes need to be packed into its own bbox:
        word_boxes = [{"type": "word", "content": wb,
                       "text": "".join([c['content'] for c in wb]),
                       "x": self.get_outer_box(wb)} for wb in word_boxes]
        # pack all words into line and return
        return {"type": "line", "content": word_boxes, 'text': " ".join([w['text'] for w in word_boxes]),
                "x": self.get_outer_box(word_boxes)}

    def single_character_bbox(self, c, font):
        size = font.getsize(c)
        return {"type": "char", "content": c[-1], "x": [(0, 0), (0, size[1]), (size[0], size[1]), (size[0], 0)]}

    def set_char_bbox_offset(self, b, x=0, y=0):
        b['x'][0] = (b['x'][0][0] + x, b['x'][0][1] + y)
        b['x'][1] = (b['x'][1][0] + x, b['x'][1][1] + y)
        return b

    @staticmethod
    def get_outer_box(bboxes):
        if type(bboxes) != list:
            raise ValueError("Error: only list of bboxes is accepted as argument in text_entity.get_outer_box, "
                             "but %s was provided" % type(bboxes))
        x = np.array([bboxes[k]['x'][l] for k in range(len(bboxes)) for l in range(4)])
        return [(x[:, 0].min(), x[:, 1].min()), (x[:, 0].min(), x[:, 1].max()),
                (x[:, 0].max(), x[:, 1].max()), (x[:, 0].max(), x[:, 1].min())]

    def bbox(self, affine_ext=None):
        """This function overrides standard implementation inherited from parent class 'entity' because
        text class provides non-standard bounding box, which is a multi-layered deep structure unlike other
        'entity' type classes which have flat bounding boxes"""
        transform = self.transform + affine_ext if affine_ext is not None else self.transform
        # now we need to browse through all levels of bboxes: block->line->word->character and
        # apply the same transformation transform
        bbox = {'type': self._block_name, 'text': self.content, 'content': self.char_bbox,
                "x": self.get_outer_box(self.char_bbox)}
        bbox = self._transform_bboxes(bbox, transform)
        return bbox

    def _transform_bboxes(self, boxes, transform):
        """This function is an iterative function that calls itself multiple times when traversing
        complex structure of bounding boxes. While traversing, it updates named parameter 'x' of each
        bounding box found inside by applying the same affine transformation 'transform' to its values.
        Since we want to keep reference structure self.bbox intact, we are building a copy while
        applying transform"""
        if type(boxes) == list:
            # return [self._transform_bboxes(b, transform) for b in boxes]
            return [self._transform_bboxes(b, transform) for b in boxes]
        if type(boxes) == dict:
            box1 = dict(boxes)
            self.transform_box(box1, transform)
            if type(boxes['content']) == list:
                # boxes['content'] = [self._transform_bboxes(b, transform) for b in boxes['content']]
                box1['content'] = [self._transform_bboxes(b, transform) for b in boxes['content']]
            return box1
        else:
            raise ValueError('Error: transform_bboxes only accepts dict or list as argument,'
                             ' %s was provided' % type(boxes))
