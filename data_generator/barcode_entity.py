from data_generator import text_entity as tx
from data_generator import barcodes as bc
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import os
import math


class BarcodeEntity(tx.TextEntity):

    _block_name = 'barcode'
    _barcode_types = {'code128': bc.Code128}

    barcode_height = 100
    barcode_thickness = 3
    barcode_quietzone = True
    barcode_type = 'code128'

    def __init__(self, kwargs):
        super().__init__(kwargs)
        if 'barcode_type' in kwargs:
            if kwargs['barcode_type'] not in self._barcode_types:
                raise NotImplementedError('Barcode type \'%s\' not implemented!')
        self.engine = self._barcode_types[self.barcode_type](kwargs)
        self.content = kwargs['barcode']
        self.img = self.__render()

    def __render(self):
        barcode_img = self.engine.render()
        draw = ImageDraw.Draw(barcode_img)
        font = self.load_font()
        tw, th = draw.textsize(str(self.content), font)
        w, h = barcode_img.size
        combined = Image.new(self.color_mode, (max(w, tw), math.floor(th + h)), self.background_color)
        hpos = math.floor((combined.size[0]-w)/2)
        combined.paste(barcode_img, (hpos, 0))
        draw = ImageDraw.Draw(combined)
        hpos = math.floor((combined.size[0]-tw)/2)
        hpos = 30
        # print code under the barcode
        code = str(self.content)
        chars = len(code)
        last_char_w = draw.textsize(code[-1], font)[0]
        if chars > 1:
            step = (combined.size[0] - 60 - last_char_w) / (chars - 1)
        else:
            step = 0
        for i, c in enumerate(str(code)):
            draw.text((hpos + i*step, self.barcode_height), c, self.font_color, font)
        return combined
        #return barcode_img

