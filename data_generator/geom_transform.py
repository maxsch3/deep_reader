import numpy as np
import math
from PIL import Image


class GeomTransform:
    """This class is used to apply geometry transformation to image or set of points on 2D plane represented
    by their coordinates x,y. This class supports any projection transformation including its special case -
    the affine transform.

    The transformation supports simple transforms: translation, rotation, shear, scale as well as generic transforms:
    matrix (where transformation matrix is set explicitly) or 4 point project, where 4 corners of a rectangle are mapped
    to their new locations. The class also supports chains of transforms, where any supported transformation can
    be included into the sequence of transformations where order matters. The class will sum them up and calculate
    resulting transform automatically."""

    def __init__(self, transformations=None):
        if transformations is not None:
            assert type(transformations) in (list, dict)
        self.transform_map = {'offset': self.add_translation,
                              'translate': self.add_translation,
                              'rotation': self.add_rotation,
                              'shear': self.add_shear,
                              'scale': self.add_scale,
                              'project': self.add_projection,
                              'matrix': self.assign_matrix}
        self.matrix = np.eye(3)
        self.is_initial = True
        if transformations is not None:
            self.add_transformations_all(transformations)

    def __add__(self, other):
        if other is None:
            return self
        if not isinstance(other, GeomTransform):
            raise ValueError('Type error: type \'GeomTransform\' was expected but got %s' % type(other))
        return GeomTransform({'matrix': self.sum(other.matrix)})

    def sum(self, other):
        """This function combines existing affine matrix with new matrix representing new transformation.
        It is assumed, that new transformation B (other) is applied after the existing one A (self.affine):
        x1 = A*x
        x2 = B*x1 = B*(A*x) = (B*A)*x
        Therefore, new affine matrix is B*A
        """
        return np.matmul(other, self.matrix)

    def adjust_offset(self, points):
        # if type(points) != np.ndarray:
        #     points = np.array(points)
        # assert points.size[1] == 2
        self.matrix[0, 2] -= points[0]
        self.matrix[1, 2] -= points[1]

    def add_transformations_all(self, transformations):
        if type(transformations) == dict:
            transformations = [transformations]
        for t in transformations:
            self.add_transformation(t)

    def add_transformation(self, transform):
        assert type(transform) == dict
        assert len(transform) == 1
        key = list(transform.keys())[0]
        if key not in self.transform_map:
            raise ValueError('Transform of type %s is not supported' % key)
        self.transform_map[key](transform[key])

    @staticmethod
    def convert_angle(angle):
        return math.radians(angle)

    def _set_matrix(self, matrix):
        self.matrix = matrix
        self.is_initial = False

    def add_rotation(self, angle):
        c = math.cos(self.convert_angle(angle))
        s = math.sin(self.convert_angle(angle))
        matrix = np.array([[c, s, 0], [-s, c, 0], [0, 0, 1]])
        self._set_matrix(self.sum(matrix))

    def add_shear(self, angle):
        assert type(angle) == tuple
        assert len(angle) == 2
        tx = math.tan(self.convert_angle(angle[0]))
        ty = math.tan(self.convert_angle(angle[1]))
        matrix = np.array([[1, tx, 0], [ty, 1, 0], [0, 0, 1]])
        self._set_matrix(self.sum(matrix))

    def add_scale(self, scale):
        assert type(scale) == tuple
        assert len(scale) == 2
        matrix = np.array([[scale[0], 0, 0], [0, scale[1], 0], [0, 0, 1]])
        self._set_matrix(self.sum(matrix))

    def add_translation(self, shift):
        assert type(shift) == tuple
        assert len(shift) == 2
        matrix = np.array([[1., 0., shift[0]], [0., 1., shift[1]], [0., 0., 1.]])
        self._set_matrix(self.sum(matrix))

    def assign_matrix(self, matrix):
        if type(matrix) != np.ndarray:
            raise ValueError('GeomTransform matrix provided to transform init must be a numpy array')
        assert matrix.shape == (3, 3)
        self._set_matrix(matrix)

    def add_projection(self, points):
        if type(points) != list:
            raise ValueError('Error: projection argument must be a list [(u0,v0), (u1,v1), (u2,v2), (u3,v3)] if a '
                             'unity square [(0,0), (0,1), (1,1), (1,0)] used as source or '
                             '[(x0,y0), (x1,y1), (x2,y2), (x3,y3), (u0,v0), (u1,v1), (u2,v2), (u3,v3)]] if other '
                             'source points are used. Got %s' % type(points))
        if len(points) == 4:
            self._add_projection(points, [(0, 0), (0, 1), (1, 1), (1, 0)])
        elif len(points) == 8:
            self._add_projection(points[4:], points[:4])
        else:
            raise ValueError('Error: projection argument must be a list of 4 or 8 tuples (x,y). '
                             'Got %s points' % len(points))

    def _add_projection(self, target_x, source_x):
        """It is not a trivial exercise to determine transformation matrix from old and new positions of
        4 points. There are number of ways for doing this:
        1. One of the most used is described here:
        https://stackoverflow.com/a/14178717/744230
        https://web.archive.org/web/20150222120106/xenia.media.mit.edu/~cwren/interpolator/
        But a don't like this approach because of 8x8 matrix collection, which is not elegant. However,
        this approach is compatible with PIL
        2 Another matrix based approach, which I like the most https://math.stackexchange.com/a/339033
        this one both elegant and compatible with PIL, so I will use it"""
        a = self.step1(source_x)
        b = self.step1(target_x)
        c = np.matmul(b, np.linalg.inv(a))
        matrix = c / c[2, 2]
        self._set_matrix(self.sum(matrix))

    @staticmethod
    def _x2mat(x):
        A = np.array(x)
        return np.hstack((A, np.ones((A.shape[0], 1)))).T

    @staticmethod
    def _mat2x(m):
        return [(m[0, i], m[1, i]) for i in range(m.shape[1])]

    def step1(self, x):
        A = self._x2mat(x)
        y = A[:, -1]
        A = A[:, :-1]
        v = np.matmul(np.linalg.inv(A), y)
        return np.multiply(A, v)

    def reset(self):
        self.matrix = np.eye(3)
        self.is_initial = True

    @staticmethod
    def expand_dim(x):
        """This function just adds new fake dimension to the existing box so that matrices are compatible"""
        return np.hstack([x, np.ones((x.shape[0], 1), dtype=x.dtype)])

    def transform(self, x):
        if type(x) is list:
            p = np.array(x)
            p = np.rint(self._transform(p)).astype(int)
            return [(p[i, 0], p[i, 1]) for i in range(p.shape[0])]
        elif type(x) is np.ndarray:
            return self._transform(x)

    def _transform(self, x):
        """This function implements generalized transformation based on 3x3 transform matrix
            | a b c |
        A = | d e f |
            | g h 1 |
        GeomTransform transformation uses the same matrix with g = 0 and h = 0. The transformation is applied
        x1   a b c     x0                  | x0 |
        y1 = d e f  *  y0   /  [ g h 1 ] * | y0 |
        1    g h 1     1                   | 1  |

        With g = 0 and h = 0, denominator becomes 1 and whole formula reduces to affine transform
        """
        if x.shape[1] == 2:
            x = self.expand_dim(x)
        x0 = np.transpose(np.matmul(self.matrix, np.transpose(x)))
        # W0 = np.expand_dims(np.matmul(self.affine[2, :], np.transpose(x)), axis=1)
        x0 = np.divide(x0, np.expand_dims(x0[:, 2], axis=1))
        return x0[:, :2]

    def transform_image(self, img, new_size):
        mat = self.convert_affine2persp()
        return img.convert('RGBA').transform(new_size, Image.PERSPECTIVE, mat, resample=Image.BILINEAR)

    def convert_affine2pil(self):
        """"This function converts np array representation of affine matrix to pil.
        For affine parameters, PIL takes a tuple [a,b,c,d,e,f] which is used to calculate
        transformed (x0,y0) = (a x1 + b y1 + c, d x1 + e y1 + f). However, unlike
        regular affine transform, (x0, y0) is a pixel in old untransformed image, and (x1, y1)
        is a pixel in new transformed image. Therefore, PIL operates the inverse of
        affine transform matrix.

        This function flattens the inverse of 3x3 affine matrix and adds offsets c and f
        automatically. The offsets are normally non-zero because PIT transforms using upper left
        corner as origin. In order to keep image centered, these values must be calculated.
        """
        ai = np.linalg.inv(self.matrix)
        return ai[0, 0], ai[0, 1], ai[0, 2], ai[1, 0], ai[1, 1], ai[1, 2]

    def convert_affine2persp(self):
        """"This function converts np array representation of affine matrix to pil format for perspective
        transformation. For this type of transformation, PIL takes a tuple [a,b,c,d,e,f, g, h] which are
        parts of transformation matrix:
        x0     a b c   x1
        y0  =  d e f   y1
        1      g h 1   1
        Unlike affine transform in this class, PIL considers (x0,y0) is a pixel in old untransformed image,
        and (x1,y1) is a pixel in new transformed image. Therefore, PIL operates the inverse of transform matrix.

        This function flattens the inverse of 3x3 affine matrix and adds offsets c and f
        automatically. The offsets are normally non-zero because PIT transforms using upper left
        corner as origin. In order to keep image centered, these values must be calculated.
        """
        ai = np.reshape(np.linalg.inv(self.matrix), -1).tolist()
        return ai[:8]

    def get_matrix_a7i(self):
        """This method just returns flattened transform matrix as a list of 9 [a,b,c,d,e,f,g,h,i]"""
        return self.matrix.flatten().tolist()
