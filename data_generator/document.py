from data_generator import collection
# from data_generator import collection


class Document(collection.Collection):
    """ A document will be a top level container that will hold all of the objects.

    Document class will be derived from a table class. In fact, document will be a single column table
    modified to allocate objects one under another and not to export rows and columns in bbox
    """

    _block_name = 'document'
    width = 0
    height = 0

    def __init__(self, block):
        super().__init__(block)

    # def setup_layout(self):
    #     for l in self.obj_layout:
    #         l['align'] = self.align
    #         l['width'] = self.width - self.padding[1] - self.padding[3]
    #         l['height'] = self.height - self.padding[0] - self.padding[2]

    def calculate_layout(self):
        # set width in all placeholders, and offsets
        y = 0
        for o, l in zip(self.objects, self.obj_layout):
            l['width'] = self.width-self.padding[1]-self.padding[3]
            l['offset'] = self.padding[3], y + self.padding[0]
            y += o.get_size()[1]

    # def get_size_untransformed(self):
    #     """This function is used for image creation. The idea behind all entity objects is to generate a minimal
    #     patch image that has no borders. Each object is 'unaware' of its location, padding or alignment - these
    #     are managed at collection levels. However, collections, can also be used as entites and they show the
    #     same behaviour: a collection positions its own objects, but it is not aware about where in document it is
    #     placed with all its objects. Its location is set at level above
    #     Document class is an exception: it is a top class and has no level above. Therefore, self positioning
    #     is managed by itself.
    #
    #     Below code adds padding back to a minimal image """
    #     width, height = super().get_size_untransformed()
    #     height = height + self.padding[0] + self.padding[2]
    #     width = width + self.padding[1] + self.padding[3]
    #     return width, height