import pandas as pd
from data_generator.geom_transform import GeomTransform


class BboxConverter(object):

    def __init__(self, bbox, transformations=None):
        self.bbox_structure = bbox
        self.idx = 0

    def transform(self, transformations):
        self.affine = GeomTransform(transformations)

    def to_dataframe(self):
        return pd.DataFrame(list(self.flatten()), columns=['id', 'type', 'parent', 'content', 'x', 'transform'])

    def flatten(self, box=None, parent=0):
        if box is None:
            box = self.bbox_structure
            self.idx = 0
        yield self._format(box, parent)
        self.idx += 1
        if 'content' in box:
            if type(box['content']) is list:
                parent = self.idx - 1
                for c in box['content']:
                    yield from self.flatten(c, parent)

    def _format(self, box, parent):
        if type(box) is not dict:
            raise ValueError('Error: wrong type of argument provided to internal format function. dict was expected, '
                             'got %s ' % type(box))
        content = None
        if 'text' in box:
            content = box['text']
        transform = None
        if 'transform' in box:
            transform = box['transform']
        return [self.idx, box['type'], parent, content, box['x'], transform]
