import pandas as pd
import numpy as np
import os
import pathlib

class AbstractValue:
    def __init__(self, kwargs):
        self.modifier_func = None
        self.literal = False
        if type(kwargs) == dict:
            self.modifier_func = kwargs.pop('mod_func', None)
            self.func = self._make_function(kwargs)
        else:
            self.func = self._echo_function(kwargs)

    def __call__(self, n=1):
        values = self.func(n)
        if self.modifier_func is not None:
            values = [self.modifier_func(v) for v in values]
        if n == 1 and not self.literal:
            return values[0]
        else:
            return values

    def _echo_function(self, val):
        """This function makes a factory function that simply return value. This is used when abstract value
        is created for a constant of any type except dictionary, which is reserved"""
        self.literal = True
        def local_func(n=1):
            if n > 1:
                return [val for _ in range(n)]
            else:
                return val
        return local_func

    @staticmethod
    def _make_function(kwargs):
        """This function will be implemented in subclasses"""
        raise NotImplementedError('Abstract value is not supposed to be used on its own. Please use one of its'
                                  ' derivatives where this function is implemented')


class RandomValue(AbstractValue):
    """This class is a root class for all abstract classes. The idea of AbstractValue is to provide one interface
    to values that can be normal constant variables and randomly generated vars. This concept will be used later
    for creation complex random generators, e.g. generator of phone number, which have parameters digits,
    prefix and format. Using AbstractValues, all 3 might be constants or random values with their own distributions.

    Example:
        I would like to create an object returning a sample from normal distribution with randomly selected mean every
        time:

        s = AbstractValue({'distribution': 'normal',
                           'mean': {'distribution': 'randint', 'low': 1, 'width': 5},
                           'scale': 3})
        np.mean(s(1000))
        > 2.12
        np.mean(s(1000))
        > 3.98

    Parameters:
        - any scalar value - when called, AbstractClass instance will return this constant value
        - dictionary describing a distribution - AbstractClass instance will sample from this distribution when called
            {"distribution": "uniform|normal|lognormal|randint|lognormint|binary|choice",
            "low|mean|trials|choice_from|file": <value>,
            "high|scale|sigma|width|p": <value>}
            see details about each distribution supported below

    At the core of it, lies factory function self.function that is defined at the time of initialising. Later,
    when object is called for a value, it uses this factory function to return value. It defines whether the value
    will be constant or sampled from some distribution"""

    def __init__(self, kwargs):
        self.distributions = {'uniform': self._make_uniform, 'normal': self._make_normal,
                              'lognormal': self._make_lognormal, 'lognormint': self._make_lognormint,
                              'randint': self._make_randint, 'binomial': self._make_binomial,
                              'choice': self._make_choice, 'randstr': self._make_randstr}
        super().__init__(kwargs)

    @staticmethod
    def instantiate_parameters(kwargs):
        """This function causes recursion in creating AbstractValue classes. Any parameter with exception of those
        listed in except_list will be converted to instances of AbstractValue. If parameters are not constant values,
        they will have their own parameters, which will be instantiated too."""
        except_list = ['distribution', 'seed', 'choice_from', 'weights', 'file']
        instances = {k: (v if k in except_list else (v if isinstance(v, RandomValue) else RandomValue(v)))
                     for k, v in kwargs.items()}
        return instances

    def _make_function(self, kwargs):
        if 'distribution' not in kwargs:
            # if distribution is missing from dictionary, it is treated as literal
            return self._echo_function(kwargs)
            # raise ValueError('Mandatory parameter \'distribution\' is missing')
        kwargs = self.instantiate_parameters(kwargs)
        if 'seed' in kwargs:
            np.random.seed(kwargs['seed'])
        if kwargs['distribution'] not in self.distributions:
            raise ValueError('Distribution type \'%s\' is not supported. If you are extending the class with new type '
                             'of distribution, please make sure you created a function _make_*newdist* and '
                             'added this function to self.distribution appending its existing values in __init__'
                             % kwargs['distribution'])
        return self.distributions[kwargs['distribution']](kwargs)

    @staticmethod
    def _make_uniform(kwargs):
        if kwargs['distribution'] != 'uniform':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % kwargs['distribution'])
        if 'low' not in kwargs:
            raise ValueError('Parameter \'low\' is required for uniform distribution')
        if 'high' not in kwargs:
            raise ValueError('Parameter \'high\' is required for uniform distribution')

        def local_generator(n):
            sample = np.random.uniform(kwargs['low'](), kwargs['high'](), n)
            return sample.tolist()
        return local_generator

    @staticmethod
    def _make_normal(kwargs):
        if kwargs['distribution'] != 'normal':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % kwargs['distribution'])
        if 'mean' not in kwargs:
            raise ValueError('Parameter \'mean\' is required for normal distribution')
        if 'scale' not in kwargs:
            raise ValueError('Parameter \'scale\' is required for normal distribution')

        def local_generator(n):
            sample = np.random.normal(kwargs['mean'](), kwargs['scale'](), n)
            return sample.tolist()
        return local_generator

    @staticmethod
    def _make_lognormal(kwargs):
        if kwargs['distribution'] != 'lognormal':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % kwargs['distribution'])
        if 'mean' not in kwargs:
            raise ValueError('Parameter \'mean\' is required for lognormal distribution')
        if 'scale' not in kwargs:
            raise ValueError('Parameter \'scale\' is required for lognormal distribution')
        # numpy's standard lognorm function is inconvenient to use because it sets parameters of
        # underlying normal distribution rather than mean and sigma of log distribution itself.
        # this leads to strange and unexpected results. Instead, im using classic lognorm distribution
        # X = e^(mu + sigma * Z) where Z is standard normal distribution.
        # see wiki https://en.wikipedia.org/wiki/Log-normal_distribution
        mean = kwargs['mean']()
        if mean <= 0.0:
            raise ValueError('Mean of log normal distribution must be positive. Got %s' % str(mean))
        scale = kwargs['scale']()
        mu = np.log(mean/np.sqrt(1 + scale**2 / mean**2))
        sigma = np.sqrt(np.log(1 + scale**2 / mean**2))

        def local_generator(n):
            sample = np.exp(mu + sigma * np.random.normal(0, 1., n))
            return sample.tolist()
        return local_generator

    def _make_lognormint(self, kwargs):
        if kwargs['distribution'] != 'lognormint':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % kwargs['distribution'])
        if 'mean' not in kwargs:
            raise ValueError('Parameter \'mean\' is required for lognormint distribution')
        if 'scale' not in kwargs:
            raise ValueError('Parameter \'scale\' is required for lognormint distribution')

        def local_generator(n):
            kwargs['distribution'] = 'lognormal'
            func = self._make_lognormal(kwargs)
            sample = np.rint(func(n)).astype(int)
            return sample.tolist()
        return local_generator

    @staticmethod
    def _make_randint(kwargs):
        if kwargs['distribution'] != 'randint':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % kwargs['distribution'])
        if 'low' not in kwargs:
            raise ValueError('Parameter \'low\' is required for randint distribution')
        if 'width' not in kwargs:
            raise ValueError('Parameter \'width\' is required for randint distribution')

        def local_generator(n):
            low = kwargs['low']()
            width = kwargs['width']()
            if width <= 1:
                raise ValueError('Range parameter (with of interval) must be > 1')
            sample = np.random.randint(low, low + width, n)
            return sample.tolist()
        return local_generator

    @staticmethod
    def _make_randstr(kwargs):
        if kwargs['distribution'] != 'randstr':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % kwargs['distribution'])
        if 'len' not in kwargs:
            raise ValueError('Error: randstr requires a length parameter \'len\'')

        def local_generator(n):
            if 'chars' in kwargs:
                characters = kwargs['chars']()
                if type(characters) is str:
                    characters = list(characters)
                elif type(characters) is not list:
                    raise ValueError('Error: unsupported data type for list of characters provided.'
                                     ' Please provide str or list')
                if len(characters) < 2:
                    raise ValueError('Error: number of characters provided for \'randstr\' to use is less than 2.'
                                     ' Please provide more.')
            else:
                if 'case' in kwargs:
                    case = kwargs['case']()
                    if case.upper()[0] == 'U':
                        characters = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                    elif case.upper()[0] == 'L':
                        characters = list('abcdefghijklmnopqrstuvwxyz')
                    else:
                        raise ValueError('Error: unsupported case value. Only \'u\' and \'l\' are supported')
                else:
                    characters = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
            sample = [''.join(np.random.choice(characters, replace=True, size=kwargs['len']()).tolist())
                      for _ in range(n)]
            return sample
        return local_generator

    @staticmethod
    def _make_binomial(kwargs):
        if kwargs['distribution'] != 'binomial':
            raise ValueError('Wrong function was called for %s distribution. '
                             'Did you call it from \'validate_and_make\'?' % kwargs['distribution'])
        trials = kwargs['trials'] if 'trials' in kwargs else AbstractValue(1)
        if 'p' not in kwargs:
            raise ValueError('Parameter \'p\' is required for binomial distribution')

        def local_generator(n):
            sample = np.random.binomial(trials(), kwargs['p'](), n)
            return sample.tolist()
        return local_generator

    def _make_choice(self, kwargs):
        if kwargs['distribution'] != 'choice':
            raise ValueError('Wrong function was called for %s sampling. Did you call it from \'validate_and_make\'?' %
                             kwargs['distribution'])
        if ('choice_from' not in kwargs) & ('file' not in kwargs):
            raise ValueError('Parameter \'choice_from\' or \'file\' are required for choice sampling')
        if 'choice_from' in kwargs:
            if not any([isinstance(kwargs['choice_from'], pd.Series), isinstance(kwargs['choice_from'], list)]):
                raise ValueError('Parameter \'choice_from\' must be a list '
                                 'or a pandas series object')
            choice_from = kwargs['choice_from']
            if 'weights' in kwargs:
                if len(kwargs['weights']) != len(kwargs['choice_from']):
                    raise ValueError('Length of weights (probabilities) must match length of choice list')
                weights = kwargs['weights']
            else:
                weights = [1 for _ in choice_from]
        else:
            choice_from, weights = self._read_from_file(kwargs['file'])
        if 'replace' in kwargs:
            replace = kwargs['replace']
        else:
            replace = True

        if any([isinstance(choice_from, pd.Series)]):
            def local_generator(n):
                choice = choice_from.sample(n=n, replace=replace, weights=weights).values.tolist()
                return choice
        else:
            def local_generator(n):
                w = np.array(weights)
                choice = np.random.choice(len(choice_from), n, replace=replace, p=w/sum(w)).tolist()
                return [choice_from[c] for c in choice]

        return local_generator

    @staticmethod
    def _read_from_file(filename):
        # check the file
        if type(filename) is not str:
            raise ValueError('Error: file path must be string, not %s' % type(filename))
        filename = pathlib.Path(filename)
        try:
            df = pd.read_csv(filename, header=None)
        except FileNotFoundError:
            raise ValueError('Could not open file %s in CWD %s' % (filename, os.getcwd()))
        # if second column was missing from file, it will be filled with NaNs
        if df.shape[1] > 2:
            df = df.iloc[:, 0:2]
        if df.shape[1] < 2:
            df['p'] = 1
        df.columns = ['choice', 'p']
        df.p = df.p.fillna(1.0)
        if type(df.iloc[0, 1]) not in [int, float, np.float16, np.float32, np.float64]:
            df = df[1:].reset_index(drop=True)
            try:
                df.p = df.p.astype(np.float32)
            except ValueError:
                raise ValueError('Error: Second column (weights) has non-numeric values.')
        return df.choice, df.p
