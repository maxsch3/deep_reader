from data_generator.combinator import SlotCombinator
from data_generator.random_table import ItemTable
from data_generator.abstract_value import RandomValue
from data_generator import random_template
from jinja2 import Environment, FileSystemLoader
from data_generator import document, text_entity
from data_generator.bbox import BboxConverter
import yaml
import datetime as dt
import os
import numpy as np

# Create generators


def str2int(x):
    return int(x)


def capitalize(x):
    return x.capitalize()

# the following data will be needed for receipt:
# seller = {'seller_name', 'address_number', 'address_street', 'address_town', 'address_postcode',
#           'phone', 'email', 'web'}
# payment = {'type', 'card_no', 'card_no_masked', AID, auth_code,  }
# loyalty = {name, number, masked_number, balance, gain, closing}
# items = {}
# templates = {doc_template, header_template, item_template}
# meta = {date, time, till_id, }

phnum = SlotCombinator({'slots': [{"distribution": "choice",
                                   "choice_from": ['01', '07', '020'],
                                   "weights": [5, 3, 2]},
                                  {"distribution": "randstr", "chars": "1234567890", "len": 11}],
                        'format': "{0}{1}"})
phone = SlotCombinator({'slots': [phnum],
                        'format': {"distribution": "choice",
                                   "choice_from": ["{0|0,5} {0|5,8} {0|8,11}",
                                                   "{0|0,5} {0|5,11}",
                                                   "{0|0,3} {0|3,7} {0|7,11}"],
                                   "weights": [5, 2, 1]}})

cashier = SlotCombinator({"slots": [{"distribution": "choice",
                                     "file": '../data_generator/data/words/names.csv'},
                                    {"distribution": "choice",
                                     "file": '../data_generator/data/words/surnames.csv'}],
                          "format": {'distribution': 'choice', 'choice_from': ['{0} {1}', '{0}', 'Mr {1}', 'Mrs {1}'],
                                     'weights': [10, 5, 1, 1]}})

seller = SlotCombinator({"slots": {"name1": {"distribution": "choice",
                                             "file": '../data_generator/data/words/adjectives.csv'},
                                   "name2": {"distribution": "choice",
                                             "file": '../data_generator/data/words/nouns.csv'},
                                   "email_account": {"distribution": "choice",
                                                     "choice_from": ["info", "contact", "customer.service"]},
                                   "domain": {"distribution": "choice",
                                              "choice_from": ["com", "co.uk", "net", "org"],
                                              "weights": [1, 1, .2, .1]},
                                   "web_prefix": {"distribution": "choice", "choice_from": ["www.", "http://", ""]},
                                   "cashier": cashier, "phone": phone,
                                   "branch": {"distribution": "choice",
                                              "file": '../data_generator/data/words/towns.csv'},
                                   "store": {"distribution": "randstr", "chars": "0123456789", "len": 3},
                                   "vat": {"distribution": "randstr", "chars": "0123456789", "len": 9},
                                   "company_no": {"distribution": "randstr", "chars": "0123456789", "len": 7}},
                        "format": {"distribution": "choice",
                                   "choice_from": [{'name': "{name1} {name2}",
                                                    'email': "{email_account}@{name1}-{name2}.{domain}",
                                                    "web": "{web_prefix}{name1}-{name2}.{domain}",
                                                    "cashier": "{cashier}", "branch": "{branch}", "store": "{store}",
                                                    "vat": "{vat}", "company_no": "{company_no}", "phone": "{phone}"},
                                                   {'name': "{name1}", 'email': "{email_account}@{name1}.{domain}",
                                                    "web": "{web_prefix}{name1}.{domain}",
                                                    "cashier": "{cashier}", "branch": "{branch}", "store": "{store}",
                                                    "vat": "{vat}", "company_no": "{company_no}", "phone": "{phone}"},
                                                   {'name': "{name2}", 'email': "{email_account}@{name2}.{domain}",
                                                    "web": "{web_prefix}{name2}.{domain}",
                                                    "cashier": "{cashier}", "branch": "{branch}", "store": "{store}",
                                                    "vat": "{vat}", "company_no": "{company_no}", "phone": "{phone}"}],
                                   "weights": [1, 1, 1]}})

datetime = dt.datetime(2018, 11, 12, 11, 30, 11)

transaction = SlotCombinator({"slots": {"datetime": datetime,
                                        "op": {"distribution": "randstr", "chars": "0123456789", "len": 9},
                                        "transaction": {"distribution": "randstr", "chars": "0123456789", "len": 6},
                                        "pos":  {"distribution": "randstr", "chars": "0123456789", "len": 3}},
                              "format": {"transaction": "{transaction}", "op": "{op}",
                                         "datetime": "{datetime}", "pos": "{pos}"}})


print(transaction()['datetime'].date())

street = SlotCombinator({"slots": [{"distribution": "choice", "mod_func": capitalize,
                                    "file": '../data_generator/data/words/nouns.csv'},
                                   {"distribution": "choice",
                                    "choice_from": ["Road", "Drive", "Close", "Mews", "Crescent", "Lane",
                                                    "Ride", "St.", "Cl.", "Cr.", "Ln."]}],
                         "format": {"distribution": "choice",
                                    "choice_from": ["{0} {1}"]}})

st = street()

postcode = SlotCombinator({'slots': [{'distribution': 'randstr', 'case': 'u',
                                      'len': {'distribution': 'choice', 'choice_from': [1, 2], 'weights': [1, 10]}},
                                     {'distribution': 'randint', 'low': 0, 'width': 99},
                                     {'distribution': 'randint', 'low': 0, 'width': 9},
                                     {'distribution': 'randstr', 'case': 'u', 'len': 2}],
                           'format': '{0}{1} {2}{3}'})

street_address = SlotCombinator({'slots': {"number": {"distribution": "lognormint", "mean": 10, "scale": 5},
                                           "street": street,
                                           "town": {"distribution": "choice",
                                                    "file": '../data_generator/data/words/towns.csv'},
                                           "postcode": postcode},
                                 'format': {"number": "{number}", "street": "{street}", "town": "{town}",
                                            "postcode": "{postcode}"}})

item_names = SlotCombinator({"slots": [{"distribution": "choice",
                                        "file": '../data_generator/data/words/adjectives.csv'},
                                       {"distribution": "choice",
                                        "file": '../data_generator/data/words/nouns.csv'},
                                       {"distribution": "choice",
                                        "file": '../data_generator/data/words/nouns.csv'}],
                             "format": {"distribution": "choice", "choice_from": ["{0} {1}", "{0} {1} {2}"],
                                        "weights": [1, 1]}})

items = ItemTable({'columns': {'name': item_names,
                               'sku': {"distribution": "randstr", "chars": "0123456789", "len": 10},
                               'qty': {"distribution": 'lognormal', 'mean': 1, 'scale': 5},
                               'price': {'distribution': 'lognormal', 'mean': 5, 'scale': 3},
                               'vat': {'distribution': 'choice', 'choice_from': [.2, .5, 0.], 'weights': [90, 5, 5]}},
                   "nrow": {'distribution': 'lognormint', 'mean': 5, 'scale': 5}})


# barcode must be char string, otherwise numbers with leading 0 will not be possible
barcode = RandomValue({"distribution": "randstr", "chars": "0123456789", "len": 10})

loyalty = RandomValue({"distribution": "choice", "file": '../data_generator/data/words/nouns.csv'})
loyalty = {'name': 'Clubcard', 'id': 'xxxxxxxxxxxx2505', 'balance': 6792}

payment = {"type": "card", "card_num": "************7654", "exp": "1020", "start": "1016", "pan_seq": "07",
           "approval": "R01657", "merchant": "08652834", "terminal": "94303274", "crypto": "A5904F8E934CB",
           "card_type": "MasterCard", "cash": 20.00, "AID": "A0000000250010402"}


def random_geom_distortion(sigma=0.02):
    box = np.array([0, 0, 0, 1, 1, 1, 1, 0]) + np.random.normal(0, sigma, 8)
    return [(box[2*i], box[2*i+1]) for i in range(4)]


template_root = "../data_generator/templates"
# template_header = random_template.RandomTemplate(template_root, "header", "json", weight_file="weights.csv")
template_header = random_template.RandomTemplate(template_root, "header", "yaml", weight_file="weights.csv")
template_address = random_template.RandomTemplate(template_root, "address", "txt")
# template_item = random_template.RandomTemplate(template_root, "item", "json", weight_file="weights.csv")
template_item = random_template.RandomTemplate(template_root, "item", "yaml", weight_file="weights.csv")
# template_payment = random_template.RandomTemplate(template_root, "payment", "json", weight_file="weights.csv")
template_payment = random_template.RandomTemplate(template_root, "payment", "yaml", weight_file="weights.csv")
# template_text = random_template.RandomTemplate(template_root, "text", "json", weight_file="weights.csv")
template_text = random_template.RandomTemplate(template_root, "text", "yaml", weight_file="weights.csv")
# template_datetime = random_template.RandomTemplate(template_root, "datetime", "json", weight_file="weights.csv")
template_datetime = random_template.RandomTemplate(template_root, "datetime", "yaml")
template_logo = random_template.RandomTemplate(template_root, "logo", "png")

defaults = {'font': 'DejaVuSansMono',
            'font_size': 14,
            'font_color': 'black',
            'color_mode': 'RGB',
            'background_color': 'white'}

text_entity.TextEntity.set_defaults(defaults)

env = Environment(loader=FileSystemLoader(template_root))

n = 10
img_dir = '../training_data/img'
box_dir = '../training_data/box'
filename = '0000001'

for i in range(n):
    filename = '{0:07d}'.format(i)
    template = env.get_template("document/document.yaml")
    parameters = {"template_header": template_header.value,
                  "template_address": template_address.value,
                  "template_item": template_item.value,
                  "template_payment": template_payment.value,
                  "template_text": template_text.value,
                  "template_datetime": template_datetime.value,
                  "logo_image": os.path.join(template_root, template_logo.value),
                  "seller": seller(),
                  "transaction": transaction(),
                  "address": street_address(),
                  "items": items(),
                  "payment": payment,
                  "currency": {"code": "GBP", "symbol": "£"},
                  "loyalty": loyalty,
                  "barcode": barcode()}
    out = template.render(**parameters)
    # d = json.loads(out)
    d = yaml.load(out)
    # print(d)

    doc = document.Document(d)
    doc.apply_transformations({'project': random_geom_distortion()})
    img = doc.get_img()
    img.save(os.path.join(img_dir, filename)+'.png', 'PNG')
    bbox = BboxConverter(doc.bbox())
    df = bbox.to_dataframe()
    df.to_csv(os.path.join(box_dir, filename)+'.csv', index=False)
    img.show()

