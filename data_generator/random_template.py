import os
import numpy as np
import glob
import pandas


class RandomTemplate:
    def __init__(self, templates_root, search_path, file_ext, weight_file=None, skip_rate = 0):
        # get the list of all files from the search_path with extension file_ext
        ls = glob.glob(os.path.join(templates_root, search_path, "*"+file_ext))
        if len(ls) == 0:
            raise ValueError("No *%s templates found in " % [file_ext, search_path])
        self.templates = [os.path.join(search_path, os.path.basename(f)) for f in ls]
        # look for a file weights.csv
        if weight_file is None:
            self.weights = [1/len(self.templates) for _ in self.templates]
        else:
            self.weights = self.load_weights(templates_root, search_path, weight_file)

        self.skip_rate = skip_rate

    def load_weights(self, templates_root, search_path, weight_file):
        weight_file = os.path.join(templates_root, search_path, weight_file)
        if not os.path.isfile(weight_file):
            raise ValueError("Error: weights file %s not found" % weight_file)
        weights = pandas.read_csv(weight_file, header=None)
        if weights.shape[1] != 2:
            raise ValueError("Weight file %s must have 2 columns: filename and weight")
        cols = ["filename", "p"]
        weights.columns = cols
        weights['filename'] = weights['filename'].apply(lambda x: os.path.splitext(os.path.basename(x))[0])
        template_filenames = [os.path.splitext(os.path.basename(f))[0] for f in self.templates]
        weights1 = pandas.DataFrame({"template": self.templates, "filename": template_filenames})
        weights1 = weights1.merge(weights, how="left")[cols]
        weights1.fillna(1, inplace=True)
        return weights1['p']/sum(weights1['p'])

    def get_value(self):
        if np.random.binomial(1, self.skip_rate):
            return None
        return np.random.choice(self.templates, p=self.weights)

    @property
    def value(self):
        return self.get_value()

    def __call__(self):
        return self.get_value()