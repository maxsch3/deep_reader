from data_generator import entity, text_entity, image_entity, barcode_entity, collection_entity
from data_generator import placeholder as ph
from data_generator import geom_transform as af
from PIL import Image
import numpy as np


class Collection(collection_entity.CollectionEntity):

    """Collections are to be used to arrange multiple objects in space. The aim of collection object is to create
    its objects and manage their positions therefore, it is all about sizes, offsets, alignments and padding

    Parameters:
        width - scalar int or float, sets outer width of collection object
        height - scalar int or float, sets outer height of collection object
        padding - scalar or list or tuple of 4. Sets space between outer dimensions of collection and inner dimensions
                available for its content: inner space = collection dimensions - padding
                It works like padding in HTML/CSS: when scalar value provided, it is applied to all 4 sides. When
                tuple or list of 4 is provided it is applied as
                (top-padding, right-padding, bottom-padding, left-padding)
        align - str of the following characters: r, l, c, t, m, b for right, left, center, top, middle, bottom.
                Sets out how objects will be aligned inside available space for an object. Available space depends on
                collection dimensions and object layout. E.g. space available for table cell depends on the width
                of column and height of row. This parameter applied to all objects in collection

    Each content object in collection is placed into a Placeholder class which intercepts positioning parameters like
    padding, align meaning it uses

    """

    _primitives = {'text': text_entity.TextEntity,
                   'image': image_entity.ImageEntity,
                   'barcode': barcode_entity.BarcodeEntity}

    _block_name = 'collection'
#    _enforce_parameters = ['width', 'height']
    _default_parameters = ['align']
    #_pushed_parameters = {'align', 'size', 'padding'}

    padding = [0, 0, 0, 0]
    size = (0, 0)
    width = 0
    height = 0

    def __init__(self, kwargs):
        super().__init__(kwargs)
        if self._block_name not in kwargs:
            raise ValueError('\'%s\' field must be available in kwargs when creating a container type %s'
                             % (self._block_name, self._block_name))
        self.ingest_settings()
        self.obj_layout = self.template_layout()
        self.setup_layout()
        self.objects = self._make_objects()
        self.calculate_layout()
        # self.set_obj_parameters()
        self.img = self.__render()

    def refresh(self):
        self.calculate_layout()
        # self.set_obj_parameters()
        self.img = self.__render()

    def ingest_settings(self):
        """this function controls and dispatches pre-processing and checks of settings set as
        class attributes by copying from block parameters dict where key matches attribute name
        (this functionality is automatic and inherited from Entity object). Pre-processing
        and checks are to validate settings and to 'ingest' them by allocating to object specific
        structures in a harmonised format. E.g. padding can be set as a single value 30
        for all sides, or it can be set as 4 numbers 10,10,30,10 setting specific value for
        each side. Single value will be expanded to 4 number format, which is a harmonised
        representation of padding in collection object"""
        pass

    def setup_layout(self):
        """This method populates parameters from the collection to the list of objects (containers) prior to
        their creation. It adds parameters by modifying self.obj_layout
        This is a generic method that will populate basic settings that do not depend on other objects like
        padding or alignment.

        This method is supposed to be re-defined in subclasses where below logic doesn't work.

        Assign parameters that 'pushed down' from collection class to its objects: alignment, dimensions less padding"""
        for l in self.obj_layout:
            l['align'] = self.align
            l['width'] = self.width - self.padding[1] - self.padding[3]
            l['height'] = self.height - self.padding[0] - self.padding[2]
            if l['width'] < 0:
                l['width'] = 0
            if l['height'] < 0:
                l['height'] = 0

    def calculate_layout(self):
        """ This is a local class-specific method that needs to be re-defined for all descendant
        classes.
        This method will lay all sub objects out according to the type of collection. This is the key
        function of all classes inherited from collection and it must be re-defined for each of them.
        This method is automatically called when child objects are created or changed. If any of the
        standard methods for manipulating child objects are re-defined, make sure this method is called
        Make sure the offsets always start from 0,0 otherwise there will be clipping"""
        if not hasattr(self, "obj_layout"):
            raise ValueError('obj_layout is not available to calculate_layout method. Please call setup_layout first')
        current_ref = (0, 0)
        for o, l in zip(self.objects, self.obj_layout):
            l['offset'] = current_ref
            current_ref = tuple(map(sum, zip(current_ref, o.get_size())))

    def template_layout(self):
        return [{'offset': (0, 0), 'block': b} for b in self.content if b is not None]

    # def set_obj_parameters(self):
    #     """This method pushes down the calculated parameters from self.layout to object placeholders"""
    #     for obj, lo in zip(self.objects, self.obj_layout):
    #         obj.set_parameters(lo)

    def get_size_untransformed(self):
        """Generic method for all collection objects
        It returns dimensions of the layout of child objects without collection transformation applied"""
        offsets = np.array([m['offset'] for m in self.obj_layout], dtype=int)
        sizes = np.array([o.get_size() for o in self.objects], dtype=int)
        far_ends = offsets + sizes
        # calculate actual bounding box of all objects in collection.
        width, height = np.max(far_ends[:, 0]) - np.min(offsets[:, 0]), np.max(far_ends[:, 1]) - np.min(offsets[:, 1])
        # if padding is available, above bbox dimensions must be extended to include padding.
        width, height = width + self.padding[1] + self.padding[3], height + self.padding[0] + self.padding[2]
        width = width if self.width == 0 else self.width
        height = height if self.height == 0 else self.height
        return width, height

    def body_size(self):
        """This function returns the size of an area available for objects. It is calculated as declared size of
        the collection (width and height) less padding
        Returns:
              size - tuple (width, height)  """
        width = self.width - self.padding[1] - self.padding[3]
        width = 0 if width < 0 else width
        height = self.height - self.padding[0] - self.padding[2]
        height = 0 if height < 0 else height
        return width, height

    def __render(self):
        """ Generic method for all collection objects
        This function creates new image of a size to fit all sub-objects and pastes them into this
        new image. It is supposed to be generic for all containers"""
        new_img = Image.new(self.color_mode, self.get_size_untransformed(), self.background_color)
        for ol, ob in zip(self.obj_layout, self.objects):
            new_img.paste(ob.get_img(), ol['offset'])
        return new_img

    def get_box_content(self, affine_ext=None):
        """ Generic method for all collection objects
        It creates a list of bbox structures from all children objects"""
        content = [self.child_bbox(i, affine_ext) for i, _ in enumerate(self.objects)]
        return content

    def transform_child(self, idx, transformations):
        """Generic method for all collection objects
        It passes list of transformations onto a child 'idx' and updates the layout and image"""
        self.objects[idx].apply_transformations(transformations)
        self.refresh()

    def child_bbox(self, index, affine_ext=None):
        """ Generic method for all collection objects
        This method returns a bbox of a child in local coordinates with all transformations applied
        in the following order:
        1. apply child object's local transformation (done by object itself) C
        2. collection specific transformation as a sum of below two: L = S + B
            2a. offset within collection's local space (shift object to its place) S
            2b. apply collection's transformation B
        3. Apply external transformation provided by affine_ext E
        The transformations are always pushed down to children by providing external transformation
        from a parent that should be added to child's transform. Parent hierarchy might be deep with
        multiple parents and multiple independent transformations that have to be stacked. Stacking
        of transformations is performed while pushing down them to children. Each parent adds its own
        transformation L to the transformation pushed down from its parent through affine_ext parameter (E)
        and passes L + E to its children as their affine_ext, and so on
        Here, in collection object, L is transformation applied to a each child with common origin at 0,0
        of a collection object. Therefore, in order to push down L to a child where all calculations are
        performed with child's local origin, we have to convert L to child's origin by adding a translation
        which is equal to child object location in a collection's space, before L
        """
        local = af.GeomTransform({'offset': self.obj_layout[index]['offset']}) + self.transform  # order does matter here
        # the transformation will be pushed down to a child to avoid re-calculation of all
        # nested structures here
        bbox = self.objects[index].bbox(affine_ext=local+affine_ext)
        return bbox

    def _make_objects(self):
        """ Generic method for all collection objects
        self.content is assumed to be a list of dictionaries, each of which is a descriptor of a primitive.
        below I make instances of classes from self._primitives class dictionary for each element of self.content"""
        return [self._make_placeholder(l) for l in self.obj_layout]

    def _make_placeholder(self, obj_meta):
        """This method creates an object inside a placeholder according to object specification provided as
        block argument, and some pre calculated fields from layout (TODO)"""
        block = obj_meta['block']
        if 'type' not in block:
            raise ValueError('\'type\' field is missing from sub-object definition in %s' % self._block_name)
        placeholder_block = self.push_parameters(block, obj_meta)
        return ph.Placeholder(placeholder_block)

    def push_parameters(self, child_block, placeholder_settings):
        """This method sets policy how collection parameters are applied to each object in collection.
        only height and width are forced onto object. Align is only pushed as default option
         This function is created to make future changes extending
        those list easier"""
        # relevant_par = {key: placeholder_settings[key] for key in self._enforce_parameters
        #                 if key in placeholder_settings}
        # # set those parameters overwriting existing values in child block definition
        # block = {**child_block, **relevant_par}
        if 'width' in placeholder_settings:
            child_block['width_max'] = placeholder_settings['width']
        if 'height' in placeholder_settings:
            child_block['height_max'] = placeholder_settings['height']
        relevant_par = {key: placeholder_settings[key] for key in self._default_parameters
                        if key in placeholder_settings}
        # set those parameters in child block definition if they were not available
        block = {**relevant_par, **child_block}
        return block

    def reset_transformation(self):
        """ Generic method for all collection objects

        This method resets transformation of collection and all child objects to the default
        identity transform"""
        super().reset_transformation()
        for o in self.objects:
            o.reset_transformation()
        self.refresh()

    @staticmethod
    def _ingest_padding(padding):
        """This method harmonizes various ways of padding input and returns harmonized representation
        in a format used in web/css [top, right, bottom, left]
        padding can be inputed in two ways:
        1. As a single number meaning same value for all sides
        2. As 4 numbers setting padding for each side separately
        """
        if type(padding) not in [list, tuple, int]:
            raise ValueError('Padding value %s is not supported. Supported types are list/tuple of'
                             ' integers [top, right, bottom, left] or a single integer' % padding)
        if type(padding) in [int]:
            padding = [padding for _ in range(4)]
        if type(padding) in [list, tuple]:
            if len(padding) != 4:
                raise ValueError('Length of padding argument is wrong. Only 4 integers are accepted')
        # maybe check for negative values?
        return padding
