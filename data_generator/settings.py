

class Settings:
    """This abstract class is just a placeholder for application-wide settings. It is a base class for
    all other classes and it should not be instantiated and used alone"""

    img_root = ''
    font_root = ''
    template_root = ''
    output_root = ''
    test_data_root = ''

    def __init__(self):
        if type(self) == Settings:
            raise NotImplementedError('Settings class must not be instantiated')
        pass

    @classmethod
    def set_defaults(cls, kwargs):
        if 'img_root' in kwargs:
            cls.img_root = kwargs['img_root']
        if 'font_root' in kwargs:
            cls.font_root = kwargs['font_root']
        if 'template_root' in kwargs:
            cls.template_root = kwargs['template_root']
        if 'output_root' in kwargs:
            cls.output_root = kwargs['output_root']
        if 'test_data_root' in kwargs:
            cls.test_data_root = kwargs['test_data_root']
