from data_generator import settings as st
from data_generator import geom_transform as gt
from PIL import Image
import numpy as np

class KwargsObject(st.Settings):
    def __init__(self, kwargs):
        self.set_instance_parameters(kwargs)

    def set_instance_parameters(self, kwargs):
        """This method will overwrite class attributes with instance ones if kwargs provided to
        __init__ contain attributes already available in class/intance
        It is used in every __init__ call for automatic overwriting default parameters where needed
        Parameters with names starting with _ will not be updated here"""
        for k, v in (x for x in kwargs.items() if x[0][0] != '_'):
            if hasattr(self, k):
                setattr(self, k, v)


class Entity(KwargsObject):

    background_color = 'white'
    color = (255, 0, 0)
    color_mode = 'RGB'
    _pushed_down_parameters = []

    _block_name = 'entity'

    _instance_counter = 0
    name = None

    def __init__(self, kwargs, render=True):
        super().__init__(kwargs)
        assert 'type' in kwargs
        if kwargs['type'] != self._block_name:
            raise ValueError("Attribute \'type\' in a block must be \'%s\', but \'%s\' was provided" %
                             (self._block_name, kwargs['type']))
        assert self._block_name in kwargs
        assert kwargs[self._block_name] is not None
        self.content = self.get_content(kwargs)
        if self.name is None:
            self.name = self._block_name + '_' + str(self._instance_counter)
            self.__class__._instance_counter += 1
        self.box_content = None
        # self.box_content = self.get_box_content()
        if render:
            self._img_ = self.__render()
            self.box = self.make_box()
        self.transform = gt.GeomTransform()
        if 'transform' in kwargs:
            self.apply_transformations(kwargs['transform'])

    @classmethod
    def set_defaults(cls, kwargs):
        # if 'background_color' in kwargs:
        #     if type(kwargs['background_color']) == str:
        #         cls.background_color = kwargs['background_color']
        # if 'color_mode' in kwargs:
        #     if type(kwargs['color_mode']) == str:
        #         cls.color_mode = kwargs['color_mode']
        # for k, v in (x for x in kwargs.items() if x not in ['block_name']):
        for k, v in (x for x in kwargs.items() if x[0][0] != '_'):
            if hasattr(cls, k):
                setattr(cls, k, v)

    @classmethod
    def accepts_pushed_down_parameters(cls):
        return cls._pushed_down_parameters

    @classmethod
    def entity_type(cls):
        return cls._block_name

    def make_box(self):
        """This function returns 4 corners of an image clockwise from top left corner as
        numpy array of size 4x2"""
        return np.array([[0, 0],
                         [self._img_.size[0], 0],
                         [self._img_.size[0], self._img_.size[1]],
                         [0, self._img_.size[1]]])

    def __render(self):
        """The method is private because it is overriden in subclasses
        and it is called as a part of __init__ above. If not made private, call to render in above
        __init__ will call newer version from a subclass, which will end in error, because subclasses
        have arguments in their implementations of render"""
        return Image.new('RGB', (1, 1), self.color)

    def get_frame_offset(self):
        b = self.get_box()
        return min(b[:, 0]), min(b[:, 1])

    def apply_transformations(self, transformations):
        """This function applies a list of transformations in an order listed in transformations argument.

        Arguments:
            transformations - one or list of dictionaries of format {'rotation': 30} or
                              [{'rotation': 30}, {'scale': (1., 1.5)}]
        """
        if type(transformations) == dict:
            if len(transformations) > 1:
                raise ValueError('Error: chained transformations must be provided in a list to preserve order. '
                                 'Got dict')
            transformations = self._adjust_transformation(transformations)
        elif type(transformations) == list:
            transformations = [self._adjust_transformation(t) for t in transformations]
        else:
            raise ValueError('Error: transform parameter must be a list or a dict, got %s' % type(transformations))
        self.transform.add_transformations_all(transformations)
        self.transform.adjust_offset(self.get_frame_offset())

    def reset_transformation(self):
        self.transform.reset()

    def _adjust_transformation(self, transformation):
        """This method scans through the list of transformations looking for projection transforms. For all found
        projections, it adjusts projection scale where needed, leaving all other transformations unchanged

        Unlike affine transformations, projection transform is not linear - i.e. if same projection applied to
        the same image at different scales, the results will not be different scales of the same output. The rate
        of distortions will be different at different scales.

        Therefore, if a projection transformation is set by 4 points representing distorted shape of unity box
        e.g. [(0,0), (0,1), (1,1), (1,0)] these points have to be scaled to image size. E.g. if a projection is set by
        [(0,0), (0,1), (1.1,1.1), (1,0)] and image size is (500,1000), projection must be expanded to explicit image
        corner mapping [(0,0), (0,1000), (500,1000), (500,1000), (0,0), (0,1000), (550,1100), (500,1000)]"""
        assert type(transformation) == dict
        assert len(transformation) == 1
        if 'project' in transformation:
            transformation['project'] = self._adjust_project_size(transformation['project'])
        return transformation

    def _adjust_project_size(self, points):
        assert type(points) == list
        assert len(points) in [4, 8]
        if len(points) == 4:
            source = [(0, 0), (0, 1), (1, 1), (1, 0)]
            target = points
        else:
            source = points[:4]
            target = points[4:]
        w, h = self.img.size
        img_corners = [(0, 0), (0, h), (w, h), (w, 0)]
        if all([s == i for s, i in zip(source, img_corners)]):
            # source size has already been adjusted - nothing to do
            return source + target
        x0s, y0s = sum([x[0] for x in source])/4, sum([x[1] for x in source])/4
        x0t, y0t = w/2, h/2
        # loop through and see
        # source = [(x0t - 2*abs(x0s-xs[0])*(x0t-xt[0]), y0t - 2*abs(y0s-xs[1])*(y0t-xt[1]))
        #           for xs, xt in zip(source, img_corners)]
        # target = [(x0t - 2*abs(x0s-xs[0])*(x0t-xt[0]), y0t - 2*abs(y0s-xs[1])*(y0t-xt[1]))
        #           for xs, xt in zip(target, img_corners)]
        # # scale shortest side by long side to enforce distortion of the short side
        # source = [(xs[0]*h, xs[1]*w) for xs in source]
        # target = [(xt[0]*h, xt[1]*w) for xt in target]
        side = max(h, w)
        source = [(xs[0]*side, xs[1]*side) for xs in source]
        target = [(xt[0]*side, xt[1]*side) for xt in target]
        return source + target

    @property
    def img(self):
        return self._img_

    @img.setter
    def img(self, img):
        """Setter method. Please note: it must not reset transformations as they are sometimes
        set before the image itself."""
        self._img_ = img
        self.box = self.make_box()
        self.transform.adjust_offset(self.get_frame_offset())

    def get_img(self):
        """This function returns an image object with affine transformation applied"""
        size = self.get_size()
        # Todo: instead of RGBA below I have to use self.color_mode + Alpha channel
        canvas = Image.new('RGBA', size, (255,)*4)
        # transformed patch
        # affine = self.affine + self.margin_transform()
        affine = self.transform
        transformed = affine.transform_image(self.img, size)
        return Image.composite(transformed, canvas, transformed).convert(self.color_mode)

    def get_box(self):
        # if affine_matrix is None:
        #     affine_matrix = self.affine
        return self.transform.transform(self.box)

    def get_size(self):
        new_box = self.get_box()
        # because transformed image is automatically aligned by adding correct translation
        # to affine matrix, min of both coordinates should be 0
        # margins = self.get_margin()
        margins = [0, 0, 0, 0]
        size = int(round(max(new_box[:, 0]))) + margins[1] + margins[3], \
            int(round(max(new_box[:, 1]))) + margins[0] + margins[2]
        return size

    # def get_margin(self):
    #     if type(self.margin) in (list, tuple):
    #         assert len(self.margin) == 4
    #         return self.margin
    #     else:
    #         return [self.margin]*4
    #
    # def margin_transform(self):
    #     margins = self.get_margin()
    #     return af.GeomTransform({'translate': (margins[3], margins[0])})

    def bbox(self, affine_ext=None):
        """This function returns a bounding box as a dictionary. It accepts external affine transformation
        from a parent object and applies it after the objects own transform.
        This means that parents transformation is pushed down to their children to this function where
        coordinates of points are calculated. In fact, all transformations in the hierarchy of are pushed down
        to this function. affine_ext parameter will bring the sum of all transformations from all
        hierarchy level above
        This function is supposed to be generic."""
        # if self.box_content is None:
        #     self.box_content = self.get_box_content()
        box = self.get_box()
        post_transform = affine_ext
        # add margin shift to box coordinates
        # post_transform = self.margin_transform()
        # if affine_ext is not None:
        #     post_transform = post_transform + affine_ext
        if post_transform is not None:
            box = post_transform.transform(box)
        box = np.rint(box).astype(int)
        points = [(box[i, 0], box[i, 1]) for i in range(box.shape[0])]
        bbox = {'type': self._block_name,
                'content': self.get_box_content(affine_ext=post_transform),
                'x': points}
        if not self.transform.is_initial:
            bbox['transform'] = self.transform.get_matrix_a7i()
        return bbox

    def get_box_content(self, affine_ext=None):
        """This function return box content of self. Unlike content, which is a parameter
        provided when object is created, box content also contains coordinates of all child objects
        if there are any. If an object does not have any children, box_content = content.
        The latter is true for Entity object and all its elementary descendants.
        It will be re-defined for collection objects"""
        return self.content

    def get_content(self, kwargs):
        """This function just grabs a particular field from parameter dictionary used to create an object"""
        return kwargs[self._block_name]

    @staticmethod
    def transform_box(box, transformation):
        """Helper function to transform finished bounding box dictionary. It does change the dictionary in place
        rather than returning it."""
        if type(box) != dict:
            raise ValueError('Error: transform_box only supports dictionary input, %s was provided' % type(box))
        if 'x' not in box:
            raise ValueError('Error: bounding box structure must have \'x\' key')
        if type(box['x']) != list:
            raise ValueError('Error: \'x\' in bounding box must be list, %s was provided' % type(box['x']))
        x = np.array(box['x'])
        x = np.rint(transformation.transform(x)).astype(int)
        x = [(x[i, 0], x[i, 1]) for i in range(x.shape[0])]
        box['x'] = x

