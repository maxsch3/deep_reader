from data_generator.abstract_value import AbstractValue, RandomValue
import numpy as np
import string


class SliceFormatter(string.Formatter):

    def get_value(self, key, args, kwds):
        if len(args) > 1:
            raise ValueError('')
        vals = args[0]
        if type(key) is int:
            if type(vals) != list:
                raise ValueError('Error in formatter: integer keys only allowed for list arguments')
            return vals[int(key)]
        if (type(key) == str) & (type(vals) == dict):
            if key in vals:
                return vals[key]
        if '|' in key:
            try:
                key, indexes = key.split('|')
                indexes = map(int, indexes.split(','))
                if key.isdigit():
                    return vals[int(key)][slice(*indexes)]
                return vals[key][slice(*indexes)]
            except KeyError:
                return kwds.get(key, 'Missing')
        return super(SliceFormatter, self).get_value(key, args, kwds)

    def _vformat(self, format_string, args, kwargs, used_args, recursion_depth,
                 auto_arg_index=0):
        """This is a slightly changed copy of a function from parent class. List of changes:
        1 it is not enforcing conversion to string any more
        2 it is not joining single result, so that original type can be preserved if only one value is returned
        """
        if recursion_depth < 0:
            raise ValueError('Max string recursion exceeded')
        result = []
        result_unformatted = []
        for literal_text, field_name, format_spec, conversion in \
                self.parse(format_string):

            # output the literal text
            if literal_text:
                result.append(literal_text)

            # if there's a field, output it
            if field_name is not None:
                # this is some markup, find the object and do
                #  the formatting

                # handle arg indexing when empty field_names are given.
                if field_name == '':
                    if auto_arg_index is False:
                        raise ValueError('cannot switch from manual field '
                                         'specification to automatic field '
                                         'numbering')
                    field_name = str(auto_arg_index)
                    auto_arg_index += 1
                elif field_name.isdigit():
                    if auto_arg_index:
                        raise ValueError('cannot switch from manual field '
                                         'specification to automatic field '
                                         'numbering')
                    # disable auto arg incrementing, if it gets
                    # used later on, then an exception will be raised
                    auto_arg_index = False

                # given the field_name, find the object it references
                #  and the argument it came from
                obj, arg_used = self.get_field(field_name, args, kwargs)
                used_args.add(arg_used)

                # do any conversion on the resulting object
                obj = self.convert_field(obj, conversion)

                # expand the format spec, if needed
                format_spec, auto_arg_index = self._vformat(
                    format_spec, args, kwargs,
                    used_args, recursion_depth-1,
                    auto_arg_index=auto_arg_index)

                # format the object and append to the result
                result.append(self.format_field(obj, format_spec))
                # This has been changed
                result_unformatted.append(obj)

        # below if condition was added
        if len(result) == 1:
            return result_unformatted[0], auto_arg_index
        else:
            return ''.join(result), auto_arg_index

class SlotCombinator(AbstractValue):

    named_slots = None

    def _make_function(self, kwargs):
        # if self.named_slots is not None:
        #     slot_abstracts, slot_probabilities = self._process_named_slots(kwargs)
        # elif 'slots' in kwargs:
        #     slot_abstracts, slot_probabilities = self._process_unnamed_slots(kwargs)
        slot_abstracts = self._process_named_slots(kwargs)
        if 'slots' in kwargs:
            slot_abstracts['slots'] = self._process_slots(kwargs['slots'])
        if 'fslots' in kwargs:
            slot_abstracts['fslots'] = self._process_slots(kwargs['fslots'])

        def local_func(n):
            outs = []
            for i in range(n):
                # sample parameters for this instance
                slot_values = self._draw_values(slot_abstracts)
                outs.append(self._format(slot_values['slots'], slot_values['format']))
                # outs.append(self._combine_slots(slot_values))
            return outs
            # if n > 1:
            #     return outs
            # else:
            #     return outs[0]
        return local_func

    def _format(self, values, format):
        if type(format) == list:
            return [self._format(values, f) for f in format]
        elif type(format) == dict:
            return {k: self._format(values, f) for k, f in format.items()}
        elif type(format) == str:
            return SliceFormatter().format(format, values)
        else:
            raise ValueError("Error: slot must be a list, dict or an instance of AbstractValue")

    def _draw_values(self, slots):
        if type(slots) == list:
            return [self._draw_values(v) for v in slots]
        elif type(slots) == dict:
            return {k: self._draw_values(v) for k, v in slots.items()}
        elif isinstance(slots, AbstractValue):
            return slots()
        else:
            raise ValueError("Error: slot must be a list, dict or an instance of AbstractValue")

    def _process_named_slots(self, kwargs):
        except_list = ['slots', 'fslots']
        # check that digits, format and prefix are provided
        slot_abstracts = {key: kwargs[key] if isinstance(kwargs[key], AbstractValue) else RandomValue(kwargs[key])
                          for key, val in kwargs.items() if key not in except_list}
        # slot_probabilities = {key: 1 for key, val in kwargs.items() if key not in except_list}
        # return slot_abstracts, slot_probabilities
        return slot_abstracts

    @staticmethod
    def _process_slots(slots):
        if type(slots) == list:
            slot_abstracts = [value if isinstance(value, AbstractValue) else RandomValue(value)
                              for value in slots]
        elif type(slots) == dict:
            slot_abstracts = {key: value if isinstance(value, AbstractValue) else RandomValue(value)
                              for key, value in slots.items()}
        else:
            raise ValueError('Error: Parameter \'slots\' must be a list or a dictionary')
        # if 'p' in kwargs:
        #     if type(kwargs['p']) is not list:
        #         raise ValueError('Error: p parameter must be a list')
        #     if len(kwargs['p']) != len(slot_abstracts):
        #         raise ValueError('Error: p must be the same length as slots parameter')
        #     slot_probabilities = kwargs['p']
        # else:
        #     slot_probabilities = [1 for _ in slot_abstracts]
        # return slot_abstracts, slot_probabilities
        return slot_abstracts

    def _combine_slots(self, values):
        formatter = SliceFormatter()
        if 'format' not in values:
            raise ValueError('Error: format is required by SlotCombinator object but it hasn\'t been provided')
        fmt = values['format']
        slot_values = values['slots']
        s = formatter.format(fmt, *values['slots'])
        # if 'trim' in values:
        #     s = s[values['trim']()]
        return s


# class PhoneGenerator(Combinator):
#
#     named_slots = ['digits', 'prefix', 'format']
#
#     def _combine_slots(self, slot_abstracts, slot_probabilities):
#         formatter = SliceFormatter()
#         digits = slot_abstracts['digits']()
#         fmt = slot_abstracts['format']()
#         prefix = slot_abstracts['prefix']()
#         number = prefix + "".join([str(d) for d in np.random.randint(0, 9, digits - len(prefix))])
#         return formatter.format(fmt, number)


# class WordCombinator(Combinator):
#
#     def _combine_slots(self, slot_abstracts, slot_probabilities):
#         slot_mask = [np.random.binomial(1, p) if p < 1.0 else 1 for p in slot_probabilities['slots']]
#         slot_values = [sl() for sl, mask in zip(slot_abstracts['slots'], slot_mask) if mask]
#         return " ".join(slot_values)


# class SlotCombinator(Combinator):
#
#     def _combine_slots(self, values):
#         formatter = SliceFormatter()
#         if 'format' not in values:
#             raise ValueError('Error: format is required by SlotCombinator object but it hasn\'t been provided')
#         fmt = values['format']
#         slot_values = values['slots']
#         s = formatter.format(fmt, *values['slots'])
#         # if 'trim' in values:
#         #     s = s[values['trim']()]
#         return s


# class SlotDictionary(Combinator):
#
#     def _combine_slots(self, slot_abstracts, slot_probabilities):
#         return {key: value() for key, value in slot_abstracts.items()}
