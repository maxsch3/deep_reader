from data_generator import entity
import re


class CollectionEntity(entity.Entity):
    """This class is just an enhanced version of Entity class that has adden functionality that will be shared by all
    collection objects. This new functionality is mainly pre-processing (ingesting) of new attributes like alignment,
     padding etc. This was implemented in a separate class to avoid clutter in Entity class which subclasses don't
     need this functionality, and on the other hand to avoid implementing these functions in many places"""

    padding = [0, 0, 0, 0]
    align = 'lt'
    halign = 'l'
    valign = 't'

    def __init__(self, kwargs):
        super().__init__(kwargs)
        self.ingest_parameters()

    def ingest_parameters(self):
        self.padding = self.ingest_padding(self.padding)
        self.halign, self.valign = self.ingest_alignment(self.align)

    @staticmethod
    def ingest_alignment(align):
        # decode lt or tl to left-top
        if type(align) == list:
            raise ValueError('\'align\' parameter must be character string, not list. '
                             'Did you mean to use \'col_align\'?')
        if type(align) != str:
            raise ValueError('\'align\' parameter must be character string like \'rt\' for right-top or'
                             ' \'l\' for left. List of characters recognised: [lcr] and [tmb]')
        halign, valign = 'l', 't'
        h = re.search('[lcr]', align)
        v = re.search('[tmb]', align)
        if h is not None:
            halign = h.group(0)
        if v is not None:
            valign = v.group(0)
        return halign, valign

    @staticmethod
    def ingest_padding(padding):
        """This method harmonizes various ways of padding input and returns harmonized representation
        in a format used in web/css [top, right, bottom, left]
        padding can be inputed in two ways:
        1. As a single number meaning same value for all sides
        2. As 4 numbers setting padding for each side separately
        """
        if type(padding) not in [list, tuple, int]:
            raise ValueError('Padding value %s is not supported. Supported types are list/tuple of'
                             ' integers [top, right, bottom, left] or a single integer' % padding)
        if type(padding) in [int]:
            padding = [padding for _ in range(4)]
        if type(padding) in [list, tuple]:
            if len(padding) != 4:
                raise ValueError('Length of padding argument is wrong. Only 4 integers are accepted')
        # maybe check for negative values?
        return padding

